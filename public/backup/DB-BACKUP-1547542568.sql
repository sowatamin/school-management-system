DROP TABLE IF EXISTS academic_branch;

CREATE TABLE `academic_branch` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `academic_id` int(10) unsigned NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `academic_branch_academic_id_foreign` (`academic_id`),
  CONSTRAINT `academic_branch_academic_id_foreign` FOREIGN KEY (`academic_id`) REFERENCES `academic_years` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO academic_branch VALUES('1','3','1','2019-01-10 12:59:16','2019-01-10 12:59:16');
INSERT INTO academic_branch VALUES('2','3','2','2019-01-10 12:59:16','2019-01-10 12:59:16');
INSERT INTO academic_branch VALUES('3','3','3','2019-01-10 12:59:16','2019-01-10 12:59:16');
INSERT INTO academic_branch VALUES('4','3','4','2019-01-10 12:59:16','2019-01-10 12:59:16');
INSERT INTO academic_branch VALUES('5','3','5','2019-01-10 12:59:16','2019-01-10 12:59:16');



DROP TABLE IF EXISTS academic_years;

CREATE TABLE `academic_years` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO academic_years VALUES('1','2018','2018-2019','2018-12-24 16:13:08','2018-12-24 16:13:08');
INSERT INTO academic_years VALUES('3','2019','2019-2020','2019-01-10 12:59:16','2019-01-10 12:59:16');



DROP TABLE IF EXISTS admission_enquiries;

CREATE TABLE `admission_enquiries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `date` date NOT NULL,
  `next_follow_up_date` date DEFAULT NULL,
  `reference` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` int(11) DEFAULT NULL,
  `number_of_child` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS assign_subjects;

CREATE TABLE `assign_subjects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `section_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO assign_subjects VALUES('1','1','3','1','2019-01-12 10:30:29','2019-01-14 16:13:04');
INSERT INTO assign_subjects VALUES('2','2','7','1','2019-01-12 10:30:29','2019-01-14 16:13:04');
INSERT INTO assign_subjects VALUES('3','3','5','1','2019-01-12 10:30:29','2019-01-14 16:13:04');
INSERT INTO assign_subjects VALUES('4','4','6','1','2019-01-12 10:30:29','2019-01-14 16:13:04');
INSERT INTO assign_subjects VALUES('5','1','4','2','2019-01-12 10:30:44','2019-01-14 16:13:14');
INSERT INTO assign_subjects VALUES('6','2','7','2','2019-01-12 10:30:44','2019-01-14 16:13:14');
INSERT INTO assign_subjects VALUES('7','3','3','2','2019-01-12 10:30:44','2019-01-14 16:13:14');
INSERT INTO assign_subjects VALUES('8','4','5','2','2019-01-12 10:30:44','2019-01-14 16:13:14');
INSERT INTO assign_subjects VALUES('9','5','3','3','2019-01-12 10:31:02','2019-01-14 16:13:26');
INSERT INTO assign_subjects VALUES('10','6','4','3','2019-01-12 10:31:02','2019-01-14 16:13:26');
INSERT INTO assign_subjects VALUES('11','7','7','3','2019-01-12 10:31:02','2019-01-14 16:13:26');
INSERT INTO assign_subjects VALUES('12','8','6','3','2019-01-12 10:31:02','2019-01-14 16:13:26');
INSERT INTO assign_subjects VALUES('13','5','3','4','2019-01-12 10:31:27','2019-01-14 16:13:36');
INSERT INTO assign_subjects VALUES('14','6','8','4','2019-01-12 10:31:27','2019-01-14 16:13:36');
INSERT INTO assign_subjects VALUES('15','7','7','4','2019-01-12 10:31:27','2019-01-14 16:13:36');
INSERT INTO assign_subjects VALUES('16','8','4','4','2019-01-12 10:31:27','2019-01-14 16:13:36');
INSERT INTO assign_subjects VALUES('17','9','3','5','2019-01-12 10:32:15','2019-01-14 16:16:40');
INSERT INTO assign_subjects VALUES('18','10','4','5','2019-01-12 10:32:15','2019-01-14 16:16:40');
INSERT INTO assign_subjects VALUES('20','12','6','5','2019-01-12 10:32:15','2019-01-14 16:16:40');
INSERT INTO assign_subjects VALUES('21','9','3','6','2019-01-12 10:32:25','2019-01-14 16:16:46');
INSERT INTO assign_subjects VALUES('22','10','8','6','2019-01-12 10:32:25','2019-01-14 16:16:46');
INSERT INTO assign_subjects VALUES('24','12','4','6','2019-01-12 10:32:25','2019-01-14 16:16:46');
INSERT INTO assign_subjects VALUES('25','13','9','7','2019-01-12 13:45:58','2019-01-12 13:45:58');
INSERT INTO assign_subjects VALUES('26','14','10','7','2019-01-12 13:45:58','2019-01-12 13:45:58');
INSERT INTO assign_subjects VALUES('27','15','11','7','2019-01-12 13:45:58','2019-01-12 13:45:58');
INSERT INTO assign_subjects VALUES('28','16','12','7','2019-01-12 13:45:58','2019-01-12 13:45:58');
INSERT INTO assign_subjects VALUES('29','13','13','8','2019-01-12 13:46:09','2019-01-12 13:46:09');
INSERT INTO assign_subjects VALUES('30','14','14','8','2019-01-12 13:46:09','2019-01-12 13:46:09');
INSERT INTO assign_subjects VALUES('31','15','9','8','2019-01-12 13:46:09','2019-01-12 13:46:09');
INSERT INTO assign_subjects VALUES('32','16','10','8','2019-01-12 13:46:09','2019-01-12 13:46:09');
INSERT INTO assign_subjects VALUES('33','17','9','9','2019-01-12 13:46:22','2019-01-12 13:46:22');
INSERT INTO assign_subjects VALUES('34','18','10','9','2019-01-12 13:46:22','2019-01-12 13:46:22');
INSERT INTO assign_subjects VALUES('35','19','11','9','2019-01-12 13:46:22','2019-01-12 13:46:22');
INSERT INTO assign_subjects VALUES('36','20','12','9','2019-01-12 13:46:22','2019-01-12 13:46:22');
INSERT INTO assign_subjects VALUES('37','17','13','10','2019-01-12 13:46:33','2019-01-12 13:46:33');
INSERT INTO assign_subjects VALUES('38','18','14','10','2019-01-12 13:46:33','2019-01-12 13:46:33');
INSERT INTO assign_subjects VALUES('39','19','9','10','2019-01-12 13:46:33','2019-01-12 13:46:33');
INSERT INTO assign_subjects VALUES('40','20','10','10','2019-01-12 13:46:33','2019-01-12 13:46:33');
INSERT INTO assign_subjects VALUES('41','21','9','11','2019-01-12 13:46:44','2019-01-12 13:46:44');
INSERT INTO assign_subjects VALUES('42','22','10','11','2019-01-12 13:46:44','2019-01-12 13:46:44');
INSERT INTO assign_subjects VALUES('43','23','11','11','2019-01-12 13:46:44','2019-01-12 13:46:44');
INSERT INTO assign_subjects VALUES('44','24','12','11','2019-01-12 13:46:44','2019-01-12 13:46:44');
INSERT INTO assign_subjects VALUES('45','21','13','12','2019-01-12 13:46:54','2019-01-12 13:47:01');
INSERT INTO assign_subjects VALUES('46','22','14','12','2019-01-12 13:46:54','2019-01-12 13:47:01');
INSERT INTO assign_subjects VALUES('47','23','11','12','2019-01-12 13:46:54','2019-01-12 13:47:01');
INSERT INTO assign_subjects VALUES('48','24','12','12','2019-01-12 13:46:54','2019-01-12 13:47:01');
INSERT INTO assign_subjects VALUES('49','37','16','1','2019-01-12 16:18:22','2019-01-14 16:13:04');
INSERT INTO assign_subjects VALUES('50','38','3','13','2019-01-13 16:34:35','2019-01-13 16:34:35');
INSERT INTO assign_subjects VALUES('51','39','4','13','2019-01-13 16:34:35','2019-01-13 16:34:35');
INSERT INTO assign_subjects VALUES('52','40','5','13','2019-01-13 16:34:35','2019-01-13 16:34:35');
INSERT INTO assign_subjects VALUES('53','41','6','13','2019-01-13 16:34:35','2019-01-13 16:34:35');
INSERT INTO assign_subjects VALUES('54','37','16','2','2019-01-14 13:58:37','2019-01-14 16:13:14');
INSERT INTO assign_subjects VALUES('55','11','7','5','2019-01-14 16:16:40','2019-01-14 16:16:40');
INSERT INTO assign_subjects VALUES('56','11','7','6','2019-01-14 16:16:46','2019-01-14 16:16:46');



DROP TABLE IF EXISTS assignments;

CREATE TABLE `assignments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `deadline` date NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO assignments VALUES('1','3','Assignment(প্রথম-বিজ্ঞান)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-15','1','1','4','assignment-2019.pdf','','','','1','2019-01-12 11:22:33','2019-01-12 11:22:33');
INSERT INTO assignments VALUES('2','3','Assignment(প্রথম-গণিত)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-15','1','2','3','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 11:23:39','2019-01-12 11:23:39');
INSERT INTO assignments VALUES('3','3','Assignment(প্রথম-ইংরেজি)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-16','1','1','2','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 11:24:44','2019-01-12 11:24:44');
INSERT INTO assignments VALUES('4','3','Assignment(প্রথম-বাংলা)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-14','1','2','1','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 11:25:33','2019-01-12 11:25:33');
INSERT INTO assignments VALUES('5','3','Assignment(দ্বিতীয়-বাংলা)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-15','6','3','8','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 11:26:28','2019-01-12 11:26:28');
INSERT INTO assignments VALUES('6','3','Assignment(দ্বিতীয়-ইংরেজি)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-14','6','4','7','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 11:27:19','2019-01-12 11:27:19');
INSERT INTO assignments VALUES('7','3','Assignment(দ্বিতীয়-গণিত)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-13','6','3','6','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 11:28:10','2019-01-12 11:28:10');
INSERT INTO assignments VALUES('8','3','Assignment(দ্বিতীয়-বিজ্ঞান)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-15','6','4','5','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 11:29:01','2019-01-12 11:29:01');
INSERT INTO assignments VALUES('11','3','Assignment(তৃতীয়-গণিত)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-15','11','5','10','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 11:31:48','2019-01-12 11:31:48');
INSERT INTO assignments VALUES('12','3','Assignment(তৃতীয়-বিজ্ঞান)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-15','11','6','9','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 11:32:54','2019-01-12 11:32:54');
INSERT INTO assignments VALUES('13','3','Assignment(তৃতীয়-বাংলা)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-14','11','5','12','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 12:00:09','2019-01-12 12:00:09');
INSERT INTO assignments VALUES('14','3','Assignment(তৃতীয়-ইংরেজি)','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','2019-01-14','11','6','11','assignment-2019.pdf','assignment-2019.pdf','','','1','2019-01-12 12:01:00','2019-01-12 12:01:00');
INSERT INTO assignments VALUES('15','1','English','<p><strong style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span><br></p>','2019-01-14','16','13','38','english-2019.docx','','','','1','2019-01-13 16:44:50','2019-01-13 16:44:50');
INSERT INTO assignments VALUES('16','1','English','<p><strong style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span><br></p>','2019-01-14','16','13','38','english-2019.docx','','','','1','2019-01-13 16:46:21','2019-01-13 16:46:21');



DROP TABLE IF EXISTS bank_cash_accounts;

CREATE TABLE `bank_cash_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_balance` decimal(8,2) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO bank_cash_accounts VALUES('1','Abdul Wahhab','100.00','','1','','2','2019-01-12 16:34:08','2019-01-12 16:34:08');
INSERT INTO bank_cash_accounts VALUES('2','Taymullah Moghadam','100.00','','1','','2','2019-01-12 16:34:39','2019-01-12 16:34:39');
INSERT INTO bank_cash_accounts VALUES('3','Dhakir Wasem','100.00','','1','','2','2019-01-12 16:42:23','2019-01-12 16:42:23');
INSERT INTO bank_cash_accounts VALUES('4','Nadhir Maloof','20000.00','','1','','1','2019-01-12 16:49:37','2019-01-12 16:49:37');
INSERT INTO bank_cash_accounts VALUES('5','Isma\'il Haddad','30000.00','','1','','1','2019-01-12 16:50:02','2019-01-12 16:50:02');
INSERT INTO bank_cash_accounts VALUES('6','Imad Ganim','15000.00','','1','','1','2019-01-12 16:50:22','2019-01-12 16:50:22');



DROP TABLE IF EXISTS book_categories;

CREATE TABLE `book_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO book_categories VALUES('1','Science','2019-01-12 16:42:20','2019-01-12 16:42:20');
INSERT INTO book_categories VALUES('2','Mathematics','2019-01-12 16:42:28','2019-01-12 16:42:28');
INSERT INTO book_categories VALUES('3','Physics','2019-01-12 16:42:36','2019-01-12 16:42:36');
INSERT INTO book_categories VALUES('4','Bangla','2019-01-12 16:42:41','2019-01-12 16:42:41');



DROP TABLE IF EXISTS book_issues;

CREATE TABLE `book_issues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `library_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `issue_date` date NOT NULL,
  `due_date` date NOT NULL,
  `return_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO book_issues VALUES('1','201966071','1','','2019-01-12','2019-01-16','','1','2019-01-12 17:05:34','2019-01-12 17:05:34');
INSERT INTO book_issues VALUES('2','201981963','1','','2019-01-12','2019-01-16','','1','2019-01-12 17:05:48','2019-01-12 17:05:48');
INSERT INTO book_issues VALUES('3','201990172','6','','2019-01-12','2019-01-23','','1','2019-01-12 17:06:00','2019-01-12 17:06:00');



DROP TABLE IF EXISTS books;

CREATE TABLE `books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `author` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publisher` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rack_no` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `publish_date` date NOT NULL,
  `photo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'book.png',
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO books VALUES('1','Ganer Sondhane','4','Haani Ali','Amin Limited','1001','10','','2019-01-09','1547290399.jpg','1','2019-01-12 16:53:19','2019-01-12 16:53:19');
INSERT INTO books VALUES('2','Ganer Sondhane','4','Haani Ali','Amin Limited','1001','10','','2019-01-09','1547290399.jpg','2','2019-01-12 16:53:19','2019-01-12 16:53:19');
INSERT INTO books VALUES('3','Ganer Sondhane','4','Haani Ali','Amin Limited','1001','10','','2019-01-09','1547290399.jpg','3','2019-01-12 16:53:19','2019-01-12 16:53:19');
INSERT INTO books VALUES('4','Ganer Sondhane','4','Haani Ali','Amin Limited','1001','10','','2019-01-09','1547290399.jpg','4','2019-01-12 16:53:19','2019-01-12 16:53:19');
INSERT INTO books VALUES('5','Ganer Sondhane','4','Haani Ali','Amin Limited','1001','10','','2019-01-09','1547290399.jpg','5','2019-01-12 16:53:19','2019-01-12 16:53:19');
INSERT INTO books VALUES('6','Mathematics','2','Uzzal kar','Amin Limited','2001','11','','2019-01-10','1547290488.jpg','1','2019-01-12 16:54:48','2019-01-12 16:54:48');
INSERT INTO books VALUES('7','Mathematics','2','Uzzal kar','Amin Limited','2001','11','','2019-01-10','1547290488.jpg','2','2019-01-12 16:54:48','2019-01-12 16:54:48');
INSERT INTO books VALUES('8','Mathematics','2','Uzzal kar','Amin Limited','2001','11','','2019-01-10','1547290488.jpg','3','2019-01-12 16:54:48','2019-01-12 16:54:48');
INSERT INTO books VALUES('9','Mathematics','2','Uzzal kar','Amin Limited','2001','11','','2019-01-10','1547290488.jpg','4','2019-01-12 16:54:48','2019-01-12 16:54:48');
INSERT INTO books VALUES('10','Mathematics','2','Uzzal kar','Amin Limited','2001','11','','2019-01-10','1547290488.jpg','5','2019-01-12 16:54:48','2019-01-12 16:54:48');



DROP TABLE IF EXISTS branches;

CREATE TABLE `branches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO branches VALUES('1','Savar','Hemayetpur,Savar,Dhaka-1340','88016000000000','Active','2019-01-10 12:51:38','2019-01-10 12:51:38');
INSERT INTO branches VALUES('2','Mirpur','House #02, Road#06, Block#A, Section#10, Mirpur, Dhaka','8801670000000','Active','2019-01-10 12:54:19','2019-01-10 12:54:19');
INSERT INTO branches VALUES('3','Uttara','Sector: 04, Uttara Model Town, Dhaka-1230','8801670000000','Active','2019-01-10 12:55:10','2019-01-10 12:55:10');
INSERT INTO branches VALUES('4','Dhanmondi','House no. 7, Road no. 8, Dhanmondi, Dhaka-120500','8801676275506','Active','2019-01-10 12:56:21','2019-01-10 12:56:39');
INSERT INTO branches VALUES('5','Gulshan','Eastern Nibash (1st Floor), 138 Gulshan Avenue, Gulshan 2, Dhaka 1212','8801676275506','Active','2019-01-10 12:57:46','2019-01-10 12:57:46');



DROP TABLE IF EXISTS chart_of_accounts;

CREATE TABLE `chart_of_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO chart_of_accounts VALUES('1','Insurance','expense','2019-01-12 16:58:48','2019-01-12 16:58:48');
INSERT INTO chart_of_accounts VALUES('2','Library Fees','income','2019-01-12 16:59:35','2019-01-12 16:59:35');



DROP TABLE IF EXISTS class_days;

CREATE TABLE `class_days` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `day` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO class_days VALUES('1','SATURDAY','1','','');
INSERT INTO class_days VALUES('2','SUNDAY','1','','');
INSERT INTO class_days VALUES('3','MONDAY','1','','');
INSERT INTO class_days VALUES('4','TUESDAY','1','','');
INSERT INTO class_days VALUES('5','WEDNESDAY','1','','');
INSERT INTO class_days VALUES('6','THURSDAY','1','','');
INSERT INTO class_days VALUES('7','FRIDAY','1','','');



DROP TABLE IF EXISTS class_routines;

CREATE TABLE `class_routines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `day` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO class_routines VALUES('1','1','1','SUNDAY','08:30:00','09:00:00','0','2019-01-12 12:54:53','2019-01-14 15:27:16');
INSERT INTO class_routines VALUES('2','1','2','SUNDAY','09:00:00','09:30:00','0','2019-01-12 12:54:53','2019-01-14 15:27:16');
INSERT INTO class_routines VALUES('3','1','3','SUNDAY','09:30:00','10:00:00','0','2019-01-12 12:54:53','2019-01-14 15:27:16');
INSERT INTO class_routines VALUES('4','1','4','SUNDAY','10:00:00','10:30:00','0','2019-01-12 12:54:53','2019-01-14 15:27:16');
INSERT INTO class_routines VALUES('5','7','13','SATURDAY','09:00:00','09:30:00','0','2019-01-12 13:52:24','2019-01-12 13:52:24');
INSERT INTO class_routines VALUES('6','7','14','SATURDAY','09:30:00','10:00:00','0','2019-01-12 13:52:24','2019-01-12 13:52:24');
INSERT INTO class_routines VALUES('7','7','15','SATURDAY','10:00:00','10:30:00','0','2019-01-12 13:52:24','2019-01-12 13:52:24');
INSERT INTO class_routines VALUES('8','7','16','SATURDAY','10:30:00','11:00:00','0','2019-01-12 13:52:24','2019-01-12 13:52:24');
INSERT INTO class_routines VALUES('9','7','13','SUNDAY','09:00:00','09:30:00','0','2019-01-12 13:55:09','2019-01-12 13:55:09');
INSERT INTO class_routines VALUES('10','7','14','SUNDAY','09:30:00','10:00:00','0','2019-01-12 13:55:09','2019-01-12 13:55:09');
INSERT INTO class_routines VALUES('11','7','15','SUNDAY','10:00:00','10:30:00','0','2019-01-12 13:55:09','2019-01-12 13:55:09');
INSERT INTO class_routines VALUES('12','7','16','SUNDAY','10:30:00','11:00:00','0','2019-01-12 13:55:09','2019-01-12 13:55:09');
INSERT INTO class_routines VALUES('13','7','13','MONDAY','09:00:00','09:30:00','0','2019-01-12 14:24:35','2019-01-12 14:24:35');
INSERT INTO class_routines VALUES('14','7','14','MONDAY','09:30:00','10:00:00','0','2019-01-12 14:24:35','2019-01-12 14:24:35');
INSERT INTO class_routines VALUES('15','7','15','MONDAY','10:00:00','10:30:00','0','2019-01-12 14:24:35','2019-01-12 14:24:35');
INSERT INTO class_routines VALUES('16','7','16','MONDAY','10:30:00','11:00:00','0','2019-01-12 14:24:35','2019-01-12 14:24:35');
INSERT INTO class_routines VALUES('17','8','13','SATURDAY','09:00:00','09:30:00','0','2019-01-12 14:26:42','2019-01-12 14:26:42');
INSERT INTO class_routines VALUES('18','8','14','SATURDAY','09:30:00','10:00:00','0','2019-01-12 14:26:42','2019-01-12 14:26:42');
INSERT INTO class_routines VALUES('19','8','15','SATURDAY','10:00:00','10:30:00','0','2019-01-12 14:26:42','2019-01-12 14:26:42');
INSERT INTO class_routines VALUES('20','8','16','SATURDAY','10:30:00','11:00:00','0','2019-01-12 14:26:42','2019-01-12 14:26:42');
INSERT INTO class_routines VALUES('21','8','13','SUNDAY','09:00:00','09:30:00','0','2019-01-12 14:27:53','2019-01-12 14:27:53');
INSERT INTO class_routines VALUES('22','8','14','SUNDAY','09:30:00','10:00:00','0','2019-01-12 14:27:53','2019-01-12 14:27:53');
INSERT INTO class_routines VALUES('23','8','15','SUNDAY','10:00:00','10:30:00','0','2019-01-12 14:27:53','2019-01-12 14:27:53');
INSERT INTO class_routines VALUES('24','8','16','SUNDAY','10:30:00','11:00:00','0','2019-01-12 14:27:53','2019-01-12 14:27:53');
INSERT INTO class_routines VALUES('25','8','13','MONDAY','09:00:00','09:30:00','0','2019-01-12 14:28:51','2019-01-12 14:28:51');
INSERT INTO class_routines VALUES('26','8','14','MONDAY','09:30:00','10:00:00','0','2019-01-12 14:28:51','2019-01-12 14:28:51');
INSERT INTO class_routines VALUES('27','8','15','MONDAY','10:00:00','10:30:00','0','2019-01-12 14:28:51','2019-01-12 14:28:51');
INSERT INTO class_routines VALUES('28','8','16','MONDAY','10:30:00','11:00:00','0','2019-01-12 14:28:51','2019-01-12 14:28:51');
INSERT INTO class_routines VALUES('29','8','13','TUESDAY','09:00:00','09:30:00','0','2019-01-12 14:30:44','2019-01-12 14:30:44');
INSERT INTO class_routines VALUES('30','8','14','TUESDAY','09:30:00','10:00:00','0','2019-01-12 14:30:44','2019-01-12 14:30:44');
INSERT INTO class_routines VALUES('31','8','15','TUESDAY','10:00:00','10:30:00','0','2019-01-12 14:30:44','2019-01-12 14:30:44');
INSERT INTO class_routines VALUES('32','8','16','TUESDAY','10:30:00','11:00:00','0','2019-01-12 14:30:44','2019-01-12 14:30:44');
INSERT INTO class_routines VALUES('33','8','13','WEDNESDAY','09:00:00','09:30:00','0','2019-01-12 14:30:49','2019-01-12 14:30:49');
INSERT INTO class_routines VALUES('34','8','14','WEDNESDAY','09:30:00','10:00:00','0','2019-01-12 14:30:49','2019-01-12 14:30:49');
INSERT INTO class_routines VALUES('35','8','15','WEDNESDAY','10:00:00','10:30:00','0','2019-01-12 14:30:49','2019-01-12 14:30:49');
INSERT INTO class_routines VALUES('36','8','16','WEDNESDAY','10:30:00','11:00:00','0','2019-01-12 14:30:49','2019-01-12 14:30:49');
INSERT INTO class_routines VALUES('37','8','13','THURSDAY','09:00:00','09:30:00','0','2019-01-12 14:30:54','2019-01-12 14:30:54');
INSERT INTO class_routines VALUES('38','8','14','THURSDAY','09:30:00','10:00:00','0','2019-01-12 14:30:54','2019-01-12 14:30:54');
INSERT INTO class_routines VALUES('39','8','15','THURSDAY','10:00:00','10:30:00','0','2019-01-12 14:30:54','2019-01-12 14:30:54');
INSERT INTO class_routines VALUES('40','8','16','THURSDAY','10:30:00','11:00:00','0','2019-01-12 14:30:54','2019-01-12 14:30:54');
INSERT INTO class_routines VALUES('41','7','13','THURSDAY','09:00:00','09:30:00','0','2019-01-12 14:32:14','2019-01-12 14:32:14');
INSERT INTO class_routines VALUES('42','7','14','THURSDAY','09:30:00','10:00:00','0','2019-01-12 14:32:14','2019-01-12 14:32:14');
INSERT INTO class_routines VALUES('43','7','15','THURSDAY','10:00:00','10:30:00','0','2019-01-12 14:32:14','2019-01-12 14:32:14');
INSERT INTO class_routines VALUES('44','7','16','THURSDAY','10:30:00','11:00:00','0','2019-01-12 14:32:14','2019-01-12 14:32:14');
INSERT INTO class_routines VALUES('45','7','13','WEDNESDAY','09:00:00','09:30:00','0','2019-01-12 14:32:17','2019-01-12 14:32:17');
INSERT INTO class_routines VALUES('46','7','14','WEDNESDAY','09:30:00','10:00:00','0','2019-01-12 14:32:17','2019-01-12 14:32:17');
INSERT INTO class_routines VALUES('47','7','15','WEDNESDAY','10:00:00','10:30:00','0','2019-01-12 14:32:17','2019-01-12 14:32:17');
INSERT INTO class_routines VALUES('48','7','16','WEDNESDAY','10:30:00','11:00:00','0','2019-01-12 14:32:17','2019-01-12 14:32:17');
INSERT INTO class_routines VALUES('49','7','13','TUESDAY','09:00:00','09:30:00','0','2019-01-12 14:32:20','2019-01-12 14:32:20');
INSERT INTO class_routines VALUES('50','7','14','TUESDAY','09:30:00','10:00:00','0','2019-01-12 14:32:20','2019-01-12 14:32:20');
INSERT INTO class_routines VALUES('51','7','15','TUESDAY','10:00:00','10:30:00','0','2019-01-12 14:32:20','2019-01-12 14:32:20');
INSERT INTO class_routines VALUES('52','7','16','TUESDAY','10:30:00','11:00:00','0','2019-01-12 14:32:20','2019-01-12 14:32:20');
INSERT INTO class_routines VALUES('53','9','17','THURSDAY','09:00:00','09:30:00','0','2019-01-12 14:41:15','2019-01-12 14:41:15');
INSERT INTO class_routines VALUES('54','9','18','THURSDAY','09:30:00','10:00:00','0','2019-01-12 14:41:15','2019-01-12 14:41:15');
INSERT INTO class_routines VALUES('55','9','19','THURSDAY','10:00:00','10:30:00','0','2019-01-12 14:41:15','2019-01-12 14:41:15');
INSERT INTO class_routines VALUES('56','9','20','THURSDAY','10:30:00','11:00:00','0','2019-01-12 14:41:15','2019-01-12 14:41:15');
INSERT INTO class_routines VALUES('57','9','17','WEDNESDAY','09:00:00','09:30:00','0','2019-01-12 14:41:18','2019-01-12 14:41:18');
INSERT INTO class_routines VALUES('58','9','18','WEDNESDAY','09:30:00','10:00:00','0','2019-01-12 14:41:18','2019-01-12 14:41:18');
INSERT INTO class_routines VALUES('59','9','19','WEDNESDAY','10:00:00','10:30:00','0','2019-01-12 14:41:18','2019-01-12 14:41:18');
INSERT INTO class_routines VALUES('60','9','20','WEDNESDAY','10:30:00','11:00:00','0','2019-01-12 14:41:18','2019-01-12 14:41:18');
INSERT INTO class_routines VALUES('61','9','17','TUESDAY','09:00:00','09:30:00','0','2019-01-12 14:41:20','2019-01-12 14:41:20');
INSERT INTO class_routines VALUES('62','9','18','TUESDAY','09:30:00','10:00:00','0','2019-01-12 14:41:20','2019-01-12 14:41:20');
INSERT INTO class_routines VALUES('63','9','19','TUESDAY','10:00:00','10:30:00','0','2019-01-12 14:41:20','2019-01-12 14:41:20');
INSERT INTO class_routines VALUES('64','9','20','TUESDAY','10:30:00','11:00:00','0','2019-01-12 14:41:20','2019-01-12 14:41:20');
INSERT INTO class_routines VALUES('65','9','17','TUESDAY','09:00:00','09:30:00','0','2019-01-12 14:41:25','2019-01-12 14:41:25');
INSERT INTO class_routines VALUES('66','9','18','TUESDAY','09:30:00','10:00:00','0','2019-01-12 14:41:25','2019-01-12 14:41:25');
INSERT INTO class_routines VALUES('67','9','19','TUESDAY','10:00:00','10:30:00','0','2019-01-12 14:41:25','2019-01-12 14:41:25');
INSERT INTO class_routines VALUES('68','9','20','TUESDAY','10:30:00','11:00:00','0','2019-01-12 14:41:25','2019-01-12 14:41:25');
INSERT INTO class_routines VALUES('69','9','17','MONDAY','09:00:00','09:30:00','0','2019-01-12 14:41:28','2019-01-12 14:41:28');
INSERT INTO class_routines VALUES('70','9','18','MONDAY','09:30:00','10:00:00','0','2019-01-12 14:41:28','2019-01-12 14:41:28');
INSERT INTO class_routines VALUES('71','9','19','MONDAY','10:00:00','10:30:00','0','2019-01-12 14:41:28','2019-01-12 14:41:28');
INSERT INTO class_routines VALUES('72','9','20','MONDAY','10:30:00','11:00:00','0','2019-01-12 14:41:28','2019-01-12 14:41:28');
INSERT INTO class_routines VALUES('73','9','17','SUNDAY','09:00:00','09:30:00','0','2019-01-12 14:41:32','2019-01-12 14:41:32');
INSERT INTO class_routines VALUES('74','9','18','SUNDAY','09:30:00','10:00:00','0','2019-01-12 14:41:32','2019-01-12 14:41:32');
INSERT INTO class_routines VALUES('75','9','19','SUNDAY','10:00:00','10:30:00','0','2019-01-12 14:41:32','2019-01-12 14:41:32');
INSERT INTO class_routines VALUES('76','9','20','SUNDAY','10:30:00','11:00:00','0','2019-01-12 14:41:32','2019-01-12 14:41:32');
INSERT INTO class_routines VALUES('77','9','17','SATURDAY','09:00:00','09:30:00','0','2019-01-12 14:41:34','2019-01-12 14:41:34');
INSERT INTO class_routines VALUES('78','9','18','SATURDAY','09:30:00','10:00:00','0','2019-01-12 14:41:34','2019-01-12 14:41:34');
INSERT INTO class_routines VALUES('79','9','19','SATURDAY','10:00:00','10:30:00','0','2019-01-12 14:41:34','2019-01-12 14:41:34');
INSERT INTO class_routines VALUES('80','9','20','SATURDAY','10:30:00','11:00:00','0','2019-01-12 14:41:34','2019-01-12 14:41:34');
INSERT INTO class_routines VALUES('81','1','37','SUNDAY','10:30:00','11:00:00','0','2019-01-14 15:27:16','2019-01-14 15:27:16');
INSERT INTO class_routines VALUES('82','2','1','SATURDAY','08:30:00','09:00:00','0','2019-01-14 15:29:05','2019-01-14 15:29:05');
INSERT INTO class_routines VALUES('83','2','2','SATURDAY','09:00:00','09:30:00','0','2019-01-14 15:29:05','2019-01-14 15:29:05');
INSERT INTO class_routines VALUES('84','2','3','SATURDAY','09:30:00','10:00:00','0','2019-01-14 15:29:05','2019-01-14 15:29:05');
INSERT INTO class_routines VALUES('85','2','4','SATURDAY','10:00:00','10:30:00','0','2019-01-14 15:29:05','2019-01-14 15:29:05');
INSERT INTO class_routines VALUES('86','2','37','SATURDAY','10:30:00','11:00:00','0','2019-01-14 15:29:05','2019-01-14 15:29:05');
INSERT INTO class_routines VALUES('87','2','1','SUNDAY','08:30:00','09:00:00','0','2019-01-14 15:29:45','2019-01-14 15:29:45');
INSERT INTO class_routines VALUES('88','2','2','SUNDAY','09:00:00','09:30:00','0','2019-01-14 15:29:45','2019-01-14 15:29:45');
INSERT INTO class_routines VALUES('89','2','3','SUNDAY','09:30:00','10:00:00','0','2019-01-14 15:29:45','2019-01-14 15:29:45');
INSERT INTO class_routines VALUES('90','2','4','SUNDAY','10:00:00','10:30:00','0','2019-01-14 15:29:45','2019-01-14 15:29:45');
INSERT INTO class_routines VALUES('91','2','37','SUNDAY','10:30:00','11:00:00','0','2019-01-14 15:29:45','2019-01-14 15:29:45');



DROP TABLE IF EXISTS classes;

CREATE TABLE `classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL,
  `academic_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO classes VALUES('1','Class 1-2019','1','1','3','2019-01-10 13:02:22','2019-01-10 13:02:22');
INSERT INTO classes VALUES('2','Class 1-2019','1','2','3','2019-01-10 13:02:22','2019-01-10 13:02:22');
INSERT INTO classes VALUES('3','Class 1-2019','1','3','3','2019-01-10 13:02:22','2019-01-10 13:02:22');
INSERT INTO classes VALUES('4','Class 1-2019','1','4','3','2019-01-10 13:02:22','2019-01-10 13:02:22');
INSERT INTO classes VALUES('5','Class 1-2019','1','5','3','2019-01-10 13:02:22','2019-01-10 13:02:22');
INSERT INTO classes VALUES('6','Class 2-2019','1','1','3','2019-01-10 13:03:01','2019-01-10 13:03:01');
INSERT INTO classes VALUES('7','Class 2-2019','1','2','3','2019-01-10 13:03:01','2019-01-10 13:03:01');
INSERT INTO classes VALUES('8','Class 2-2019','1','3','3','2019-01-10 13:03:01','2019-01-10 13:03:01');
INSERT INTO classes VALUES('9','Class 2-2019','1','4','3','2019-01-10 13:03:01','2019-01-10 13:03:01');
INSERT INTO classes VALUES('10','Class 2-2019','1','5','3','2019-01-10 13:03:01','2019-01-10 13:03:01');
INSERT INTO classes VALUES('11','Class 3-2019','1','1','3','2019-01-10 13:03:11','2019-01-10 13:03:11');
INSERT INTO classes VALUES('12','Class 3-2019','1','2','3','2019-01-10 13:03:11','2019-01-10 13:03:11');
INSERT INTO classes VALUES('13','Class 3-2019','1','3','3','2019-01-10 13:03:11','2019-01-10 13:03:11');
INSERT INTO classes VALUES('14','Class 3-2019','1','4','3','2019-01-10 13:03:11','2019-01-10 13:03:11');
INSERT INTO classes VALUES('15','Class 3-2019','1','5','3','2019-01-10 13:03:11','2019-01-10 13:03:11');
INSERT INTO classes VALUES('16','Class 1-2018','1','1','1','2019-01-12 10:08:43','2019-01-12 10:08:43');
INSERT INTO classes VALUES('17','Class 1-2018','1','2','1','2019-01-12 10:08:43','2019-01-12 10:08:43');
INSERT INTO classes VALUES('18','Class 1-2018','1','3','1','2019-01-12 10:08:43','2019-01-12 10:08:43');
INSERT INTO classes VALUES('19','Class 1-2018','1','4','1','2019-01-12 10:08:43','2019-01-12 10:08:43');
INSERT INTO classes VALUES('20','Class 1-2018','1','5','1','2019-01-12 10:08:43','2019-01-12 10:08:43');
INSERT INTO classes VALUES('21','Class 2-2018','1','1','1','2019-01-12 10:09:54','2019-01-12 10:09:54');
INSERT INTO classes VALUES('22','Class 2-2018','1','2','1','2019-01-12 10:09:54','2019-01-12 10:09:54');
INSERT INTO classes VALUES('23','Class 2-2018','1','3','1','2019-01-12 10:09:54','2019-01-12 10:09:54');
INSERT INTO classes VALUES('24','Class 2-2018','1','4','1','2019-01-12 10:09:54','2019-01-12 10:09:54');
INSERT INTO classes VALUES('25','Class 2-2018','1','5','1','2019-01-12 10:09:54','2019-01-12 10:09:54');
INSERT INTO classes VALUES('31','Class 3-2018','1','1','1','2019-01-12 10:10:41','2019-01-12 10:10:41');
INSERT INTO classes VALUES('32','Class 3-2018','1','2','1','2019-01-12 10:10:41','2019-01-12 10:10:41');
INSERT INTO classes VALUES('33','Class 3-2018','1','3','1','2019-01-12 10:10:41','2019-01-12 10:10:41');
INSERT INTO classes VALUES('34','Class 3-2018','1','4','1','2019-01-12 10:10:41','2019-01-12 10:10:41');
INSERT INTO classes VALUES('35','Class 3-2018','1','5','1','2019-01-12 10:10:41','2019-01-12 10:10:41');
INSERT INTO classes VALUES('36','Class 4-2019','1','1','3','2019-01-12 16:08:08','2019-01-12 16:08:08');
INSERT INTO classes VALUES('37','Class 4-2019','1','2','3','2019-01-12 16:08:08','2019-01-12 16:08:08');



DROP TABLE IF EXISTS complains;

CREATE TABLE `complains` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `complain_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complain_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `taken_action` text COLLATE utf8mb4_unicode_ci,
  `note` text COLLATE utf8mb4_unicode_ci,
  `attach_document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS custom_fields;

CREATE TABLE `custom_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_data` text COLLATE utf8mb4_unicode_ci,
  `field_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS email_logs;

CREATE TABLE `email_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `receiver_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` int(11) NOT NULL,
  `academic_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO email_logs VALUES('1','nomaduzzal@gmail.com','Testing','<p>Testing<br></p>','1','3','2019-01-14 16:44:13','2019-01-14 16:44:13');



DROP TABLE IF EXISTS events;

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `academic_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS exam_attendances;

CREATE TABLE `exam_attendances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `attendance` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO exam_attendances VALUES('1','1','1','1','1','1','2019-01-12','1','2019-01-12 17:21:02','2019-01-12 17:21:02');
INSERT INTO exam_attendances VALUES('2','1','1','10','1','1','2019-01-12','1','2019-01-12 17:21:02','2019-01-12 17:21:02');
INSERT INTO exam_attendances VALUES('3','1','1','3','1','2','2019-01-12','1','2019-01-12 17:21:07','2019-01-12 17:21:07');
INSERT INTO exam_attendances VALUES('4','1','1','11','1','2','2019-01-12','1','2019-01-12 17:21:07','2019-01-12 17:21:07');
INSERT INTO exam_attendances VALUES('5','1','8','6','6','3','2019-01-12','1','2019-01-12 17:21:26','2019-01-12 17:21:26');
INSERT INTO exam_attendances VALUES('6','1','8','12','6','3','2019-01-12','1','2019-01-12 17:21:26','2019-01-12 17:21:26');
INSERT INTO exam_attendances VALUES('7','1','8','2','6','4','2019-01-12','1','2019-01-12 17:21:30','2019-01-12 17:21:30');
INSERT INTO exam_attendances VALUES('8','1','8','4','6','4','2019-01-12','1','2019-01-12 17:21:30','2019-01-12 17:21:30');
INSERT INTO exam_attendances VALUES('9','1','2','1','1','1','2019-01-12','1','2019-01-12 17:21:50','2019-01-12 17:21:50');
INSERT INTO exam_attendances VALUES('10','1','2','10','1','1','2019-01-12','1','2019-01-12 17:21:50','2019-01-12 17:21:50');
INSERT INTO exam_attendances VALUES('11','1','2','3','1','2','2019-01-12','1','2019-01-12 17:21:55','2019-01-12 17:21:55');
INSERT INTO exam_attendances VALUES('12','1','2','11','1','2','2019-01-12','1','2019-01-12 17:21:55','2019-01-12 17:21:55');
INSERT INTO exam_attendances VALUES('13','1','3','1','1','1','2019-01-12','1','2019-01-12 17:22:07','2019-01-12 17:22:07');
INSERT INTO exam_attendances VALUES('14','1','3','10','1','1','2019-01-12','1','2019-01-12 17:22:07','2019-01-12 17:22:07');
INSERT INTO exam_attendances VALUES('15','1','3','3','1','2','2019-01-12','1','2019-01-12 17:22:16','2019-01-12 17:22:16');
INSERT INTO exam_attendances VALUES('16','1','3','11','1','2','2019-01-12','1','2019-01-12 17:22:16','2019-01-12 17:22:16');
INSERT INTO exam_attendances VALUES('17','1','4','1','1','1','2019-01-12','1','2019-01-12 17:22:29','2019-01-12 17:22:29');
INSERT INTO exam_attendances VALUES('18','1','4','10','1','1','2019-01-12','1','2019-01-12 17:22:29','2019-01-12 17:22:29');
INSERT INTO exam_attendances VALUES('19','1','4','3','1','2','2019-01-12','1','2019-01-12 17:22:34','2019-01-12 17:22:34');
INSERT INTO exam_attendances VALUES('20','1','4','11','1','2','2019-01-12','1','2019-01-12 17:22:34','2019-01-12 17:22:34');
INSERT INTO exam_attendances VALUES('21','1','5','6','6','3','2019-01-12','1','2019-01-12 17:22:54','2019-01-12 17:22:54');
INSERT INTO exam_attendances VALUES('22','1','5','12','6','3','2019-01-12','1','2019-01-12 17:22:54','2019-01-12 17:22:54');
INSERT INTO exam_attendances VALUES('23','1','5','2','6','4','2019-01-12','1','2019-01-12 17:23:00','2019-01-12 17:23:00');
INSERT INTO exam_attendances VALUES('24','1','5','4','6','4','2019-01-12','1','2019-01-12 17:23:00','2019-01-12 17:23:00');
INSERT INTO exam_attendances VALUES('25','1','6','6','6','3','2019-01-12','1','2019-01-12 17:23:10','2019-01-12 17:23:10');
INSERT INTO exam_attendances VALUES('26','1','6','12','6','3','2019-01-12','1','2019-01-12 17:23:10','2019-01-12 17:23:10');
INSERT INTO exam_attendances VALUES('27','1','6','2','6','4','2019-01-12','1','2019-01-12 17:23:16','2019-01-12 17:23:16');
INSERT INTO exam_attendances VALUES('28','1','6','4','6','4','2019-01-12','1','2019-01-12 17:23:16','2019-01-12 17:23:16');
INSERT INTO exam_attendances VALUES('29','1','7','6','6','3','2019-01-12','1','2019-01-12 17:23:25','2019-01-12 17:23:25');
INSERT INTO exam_attendances VALUES('30','1','7','12','6','3','2019-01-12','1','2019-01-12 17:23:25','2019-01-12 17:23:25');
INSERT INTO exam_attendances VALUES('31','1','7','2','6','4','2019-01-12','1','2019-01-12 17:23:30','2019-01-12 17:23:30');
INSERT INTO exam_attendances VALUES('32','1','7','4','6','4','2019-01-12','1','2019-01-12 17:23:30','2019-01-12 17:23:30');
INSERT INTO exam_attendances VALUES('33','2','8','2','6','4','2019-01-12','1','2019-01-12 17:23:38','2019-01-12 17:23:38');
INSERT INTO exam_attendances VALUES('34','2','8','4','6','4','2019-01-12','1','2019-01-12 17:23:38','2019-01-12 17:23:38');
INSERT INTO exam_attendances VALUES('35','2','8','6','6','3','2019-01-12','1','2019-01-12 17:23:44','2019-01-12 17:23:44');
INSERT INTO exam_attendances VALUES('36','2','8','12','6','3','2019-01-12','1','2019-01-12 17:23:44','2019-01-12 17:23:44');
INSERT INTO exam_attendances VALUES('37','2','1','1','1','1','2019-01-12','1','2019-01-12 17:24:23','2019-01-12 17:24:23');
INSERT INTO exam_attendances VALUES('38','2','1','10','1','1','2019-01-12','1','2019-01-12 17:24:23','2019-01-12 17:24:23');
INSERT INTO exam_attendances VALUES('39','2','1','3','1','2','2019-01-12','1','2019-01-12 17:24:28','2019-01-12 17:24:28');
INSERT INTO exam_attendances VALUES('40','2','1','11','1','2','2019-01-12','1','2019-01-12 17:24:28','2019-01-12 17:24:28');
INSERT INTO exam_attendances VALUES('41','2','2','1','1','1','2019-01-12','1','2019-01-12 17:24:40','2019-01-12 17:24:40');
INSERT INTO exam_attendances VALUES('42','2','2','10','1','1','2019-01-12','1','2019-01-12 17:24:40','2019-01-12 17:24:40');
INSERT INTO exam_attendances VALUES('43','2','2','3','1','2','2019-01-12','1','2019-01-12 17:24:45','2019-01-12 17:24:45');
INSERT INTO exam_attendances VALUES('44','2','2','11','1','2','2019-01-12','1','2019-01-12 17:24:45','2019-01-12 17:24:45');
INSERT INTO exam_attendances VALUES('45','2','3','1','1','1','2019-01-12','1','2019-01-12 17:24:53','2019-01-12 17:24:53');
INSERT INTO exam_attendances VALUES('46','2','3','10','1','1','2019-01-12','1','2019-01-12 17:24:53','2019-01-12 17:24:53');
INSERT INTO exam_attendances VALUES('47','2','3','3','1','2','2019-01-12','1','2019-01-12 17:24:58','2019-01-12 17:24:58');
INSERT INTO exam_attendances VALUES('48','2','3','11','1','2','2019-01-12','1','2019-01-12 17:24:58','2019-01-12 17:24:58');
INSERT INTO exam_attendances VALUES('49','2','4','1','1','1','2019-01-12','1','2019-01-12 17:25:04','2019-01-12 17:25:04');
INSERT INTO exam_attendances VALUES('50','2','4','10','1','1','2019-01-12','1','2019-01-12 17:25:04','2019-01-12 17:25:04');
INSERT INTO exam_attendances VALUES('51','2','4','3','1','2','2019-01-12','1','2019-01-12 17:25:09','2019-01-12 17:25:09');
INSERT INTO exam_attendances VALUES('52','2','4','11','1','2','2019-01-12','1','2019-01-12 17:25:09','2019-01-12 17:25:09');
INSERT INTO exam_attendances VALUES('53','2','5','6','6','3','2019-01-12','1','2019-01-12 17:25:19','2019-01-12 17:25:19');
INSERT INTO exam_attendances VALUES('54','2','5','12','6','3','2019-01-12','1','2019-01-12 17:25:19','2019-01-12 17:25:19');
INSERT INTO exam_attendances VALUES('55','2','5','2','6','4','2019-01-12','1','2019-01-12 17:25:25','2019-01-12 17:25:25');
INSERT INTO exam_attendances VALUES('56','2','5','4','6','4','2019-01-12','1','2019-01-12 17:25:25','2019-01-12 17:25:25');
INSERT INTO exam_attendances VALUES('57','2','6','6','6','3','2019-01-12','1','2019-01-12 17:25:32','2019-01-12 17:25:32');
INSERT INTO exam_attendances VALUES('58','2','6','12','6','3','2019-01-12','1','2019-01-12 17:25:32','2019-01-12 17:25:32');
INSERT INTO exam_attendances VALUES('59','2','6','2','6','4','2019-01-12','1','2019-01-12 17:25:36','2019-01-12 17:25:36');
INSERT INTO exam_attendances VALUES('60','2','6','4','6','4','2019-01-12','1','2019-01-12 17:25:36','2019-01-12 17:25:36');
INSERT INTO exam_attendances VALUES('61','2','7','6','6','3','2019-01-12','1','2019-01-12 17:25:44','2019-01-12 17:25:44');
INSERT INTO exam_attendances VALUES('62','2','7','12','6','3','2019-01-12','1','2019-01-12 17:25:44','2019-01-12 17:25:44');
INSERT INTO exam_attendances VALUES('63','2','7','2','6','4','2019-01-12','1','2019-01-12 17:25:49','2019-01-12 17:25:49');
INSERT INTO exam_attendances VALUES('64','2','7','4','6','4','2019-01-12','1','2019-01-12 17:25:49','2019-01-12 17:25:49');



DROP TABLE IF EXISTS exam_schedules;

CREATE TABLE `exam_schedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `room` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO exam_schedules VALUES('1','1','1','1','2019-01-13','09:00:00','10:00:00','101','1','2019-01-12 17:08:58','2019-01-13 12:16:02');
INSERT INTO exam_schedules VALUES('2','1','1','2','2019-01-14','09:00:00','10:00:00','101','1','2019-01-12 17:08:58','2019-01-13 12:16:02');
INSERT INTO exam_schedules VALUES('3','1','1','3','2019-01-15','09:00:00','10:00:00','101','1','2019-01-12 17:08:58','2019-01-13 12:16:02');
INSERT INTO exam_schedules VALUES('4','1','1','4','2019-01-16','09:00:00','10:00:00','101','1','2019-01-12 17:08:58','2019-01-13 12:16:02');
INSERT INTO exam_schedules VALUES('5','1','1','37','2019-01-17','09:00:00','10:00:00','101','1','2019-01-12 17:08:58','2019-01-13 12:16:02');
INSERT INTO exam_schedules VALUES('6','1','6','5','2019-01-13','09:00:00','10:00:00','201','1','2019-01-12 17:10:11','2019-01-12 17:10:11');
INSERT INTO exam_schedules VALUES('7','1','6','6','2019-01-14','09:00:00','10:00:00','201','1','2019-01-12 17:10:11','2019-01-12 17:10:11');
INSERT INTO exam_schedules VALUES('8','1','6','7','2019-01-15','09:00:00','10:00:00','201','1','2019-01-12 17:10:11','2019-01-12 17:10:11');
INSERT INTO exam_schedules VALUES('9','1','6','8','2019-01-16','09:00:00','10:00:00','201','1','2019-01-12 17:10:11','2019-01-12 17:10:11');
INSERT INTO exam_schedules VALUES('10','1','11','9','2019-01-13','09:00:00','10:00:00','301','1','2019-01-12 17:10:58','2019-01-12 17:10:58');
INSERT INTO exam_schedules VALUES('11','1','11','10','2019-01-14','09:00:00','10:00:00','301','1','2019-01-12 17:10:58','2019-01-12 17:10:58');
INSERT INTO exam_schedules VALUES('12','1','11','11','2019-01-12','09:00:00','10:00:00','301','1','2019-01-12 17:10:58','2019-01-12 17:10:58');
INSERT INTO exam_schedules VALUES('13','1','11','12','2019-01-16','09:00:00','10:00:00','301','1','2019-01-12 17:10:58','2019-01-12 17:10:58');
INSERT INTO exam_schedules VALUES('19','2','6','5','2019-06-01','09:00:00','10:00:00','201','1','2019-01-12 17:14:44','2019-01-12 17:14:44');
INSERT INTO exam_schedules VALUES('20','2','6','6','2019-06-02','09:00:00','10:00:00','201','1','2019-01-12 17:14:44','2019-01-12 17:14:44');
INSERT INTO exam_schedules VALUES('21','2','6','7','2019-06-03','09:00:00','10:00:00','201','1','2019-01-12 17:14:44','2019-01-12 17:14:44');
INSERT INTO exam_schedules VALUES('22','2','6','8','2019-06-04','09:00:00','10:00:00','201','1','2019-01-12 17:14:44','2019-01-12 17:14:44');
INSERT INTO exam_schedules VALUES('23','2','1','1','2019-01-12','05:19:00','05:19:00','111','1','2019-01-12 17:14:52','2019-01-12 17:19:47');
INSERT INTO exam_schedules VALUES('24','2','1','2','2019-01-12','05:19:00','05:19:00','111','1','2019-01-12 17:14:52','2019-01-12 17:19:47');
INSERT INTO exam_schedules VALUES('25','2','1','3','2019-01-12','05:19:00','05:19:00','111','1','2019-01-12 17:14:52','2019-01-12 17:19:47');
INSERT INTO exam_schedules VALUES('26','2','1','4','2019-01-12','05:19:00','05:19:00','111','1','2019-01-12 17:14:52','2019-01-12 17:19:47');
INSERT INTO exam_schedules VALUES('27','2','1','37','2019-01-12','05:19:00','05:19:00','111','1','2019-01-12 17:14:52','2019-01-12 17:19:47');
INSERT INTO exam_schedules VALUES('28','2','11','9','2019-06-01','09:00:00','10:00:00','301','1','2019-01-12 17:15:50','2019-01-12 17:15:50');
INSERT INTO exam_schedules VALUES('29','2','11','10','2019-06-02','09:00:00','10:00:00','301','1','2019-01-12 17:15:50','2019-01-12 17:15:50');
INSERT INTO exam_schedules VALUES('30','2','11','11','2019-06-03','09:00:00','10:00:00','301','1','2019-01-12 17:15:50','2019-01-12 17:15:50');
INSERT INTO exam_schedules VALUES('31','2','11','12','2019-06-04','09:00:00','10:00:00','301','1','2019-01-12 17:15:50','2019-01-12 17:15:50');



DROP TABLE IF EXISTS exams;

CREATE TABLE `exams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `session_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO exams VALUES('1','First Temp. Examination','','3','2019-01-10 17:36:24','2019-01-14 17:01:06');
INSERT INTO exams VALUES('2','Second Temp. Examination','','3','2019-01-10 17:36:54','2019-01-14 17:00:20');
INSERT INTO exams VALUES('3','Annual Examination','','3','2019-01-10 17:37:55','2019-01-14 17:00:08');



DROP TABLE IF EXISTS fee_types;

CREATE TABLE `fee_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fee_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO fee_types VALUES('1','Tuition','001','','2019-01-12 17:08:32','2019-01-12 17:10:43');
INSERT INTO fee_types VALUES('2','Health','002','','2019-01-12 17:10:15','2019-01-12 17:10:15');
INSERT INTO fee_types VALUES('3','Exam','003','','2019-01-12 17:11:15','2019-01-12 17:11:15');



DROP TABLE IF EXISTS grades;

CREATE TABLE `grades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grade_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marks_from` decimal(8,2) NOT NULL,
  `marks_to` decimal(8,2) NOT NULL,
  `point` decimal(8,2) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS hostel_categories;

CREATE TABLE `hostel_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostel_id` int(11) NOT NULL,
  `standard` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hostel_fee` decimal(8,2) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS hostel_members;

CREATE TABLE `hostel_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `hostel_id` int(11) NOT NULL,
  `hostel_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS hostels;

CREATE TABLE `hostels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostel_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS invoice_items;

CREATE TABLE `invoice_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `fee_id` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `discount` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO invoice_items VALUES('1','1','3','200.00','0.00','2019-01-12 17:55:19','2019-01-12 17:55:19');



DROP TABLE IF EXISTS invoices;

CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `total` decimal(8,2) NOT NULL,
  `paid` decimal(8,2) DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO invoices VALUES('1','1','1','1','3','2019-01-11','Tuition fee','','200.00','','Paid','1','2019-01-12 17:55:19','2019-01-12 17:55:19');



DROP TABLE IF EXISTS library_members;

CREATE TABLE `library_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `library_id` int(11) NOT NULL,
  `member_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO library_members VALUES('1','71','201966071','Student','2019-01-12 16:56:18','2019-01-12 16:56:18');
INSERT INTO library_members VALUES('2','70','201954770','Student','2019-01-12 16:56:32','2019-01-12 16:56:32');
INSERT INTO library_members VALUES('3','63','201981963','Student','2019-01-12 16:56:50','2019-01-12 16:56:50');
INSERT INTO library_members VALUES('4','72','201990172','Student','2019-01-12 16:58:37','2019-01-12 16:58:37');
INSERT INTO library_members VALUES('5','48','201925248','Teacher','2019-01-12 16:58:50','2019-01-12 16:58:50');
INSERT INTO library_members VALUES('6','46','201936046','Teacher','2019-01-12 16:59:03','2019-01-12 16:59:03');



DROP TABLE IF EXISTS mark_details;

CREATE TABLE `mark_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mark_id` int(11) NOT NULL,
  `mark_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mark_value` decimal(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO mark_details VALUES('1','1','100','90.00','2019-01-12 17:49:56','2019-01-12 17:49:56');
INSERT INTO mark_details VALUES('2','2','100','80.00','2019-01-12 17:49:56','2019-01-12 17:49:56');
INSERT INTO mark_details VALUES('3','3','100','95.00','2019-01-12 17:50:11','2019-01-12 17:50:11');
INSERT INTO mark_details VALUES('4','4','100','90.00','2019-01-12 17:50:11','2019-01-12 17:50:11');
INSERT INTO mark_details VALUES('5','5','100','88.00','2019-01-12 17:50:29','2019-01-12 17:50:29');
INSERT INTO mark_details VALUES('6','6','100','85.00','2019-01-12 17:50:29','2019-01-12 17:50:29');
INSERT INTO mark_details VALUES('7','7','100','87.00','2019-01-12 17:50:40','2019-01-12 17:50:40');
INSERT INTO mark_details VALUES('8','8','100','89.00','2019-01-12 17:50:40','2019-01-12 17:50:40');
INSERT INTO mark_details VALUES('9','9','100','100.00','2019-01-12 17:50:58','2019-01-12 17:50:58');
INSERT INTO mark_details VALUES('10','10','100','95.00','2019-01-12 17:50:58','2019-01-12 17:50:58');
INSERT INTO mark_details VALUES('11','11','100','60.00','2019-01-12 17:51:23','2019-01-12 17:51:23');
INSERT INTO mark_details VALUES('12','12','100','55.00','2019-01-12 17:51:23','2019-01-12 17:51:23');
INSERT INTO mark_details VALUES('13','13','100','80.00','2019-01-12 17:51:38','2019-01-12 17:51:38');
INSERT INTO mark_details VALUES('14','14','100','85.00','2019-01-12 17:51:38','2019-01-12 17:51:38');
INSERT INTO mark_details VALUES('15','15','100','78.00','2019-01-12 17:51:47','2019-01-12 17:51:47');
INSERT INTO mark_details VALUES('16','16','100','80.00','2019-01-12 17:51:47','2019-01-12 17:51:47');
INSERT INTO mark_details VALUES('17','17','100','78.00','2019-01-12 17:52:15','2019-01-12 17:52:15');
INSERT INTO mark_details VALUES('18','18','100','89.00','2019-01-12 17:52:15','2019-01-12 17:52:15');
INSERT INTO mark_details VALUES('19','19','100','56.00','2019-01-12 17:52:23','2019-01-12 17:52:23');
INSERT INTO mark_details VALUES('20','20','100','55.00','2019-01-12 17:52:23','2019-01-12 17:52:23');
INSERT INTO mark_details VALUES('21','21','100','89.00','2019-01-12 17:52:34','2019-01-12 17:52:34');
INSERT INTO mark_details VALUES('22','22','100','90.00','2019-01-12 17:52:34','2019-01-12 17:52:34');
INSERT INTO mark_details VALUES('23','23','100','78.00','2019-01-12 17:52:42','2019-01-12 17:52:42');
INSERT INTO mark_details VALUES('24','24','100','80.00','2019-01-12 17:52:42','2019-01-12 17:52:42');
INSERT INTO mark_details VALUES('25','25','100','90.00','2019-01-12 17:52:54','2019-01-12 17:52:54');
INSERT INTO mark_details VALUES('26','26','100','95.00','2019-01-12 17:52:54','2019-01-12 17:52:54');
INSERT INTO mark_details VALUES('27','27','100','90.00','2019-01-12 17:55:26','2019-01-12 17:55:26');
INSERT INTO mark_details VALUES('28','28','100','95.00','2019-01-12 17:55:26','2019-01-12 17:55:26');
INSERT INTO mark_details VALUES('29','29','100','65.00','2019-01-12 17:55:37','2019-01-12 17:55:37');
INSERT INTO mark_details VALUES('30','30','100','64.00','2019-01-12 17:55:37','2019-01-12 17:55:37');
INSERT INTO mark_details VALUES('31','31','100','78.00','2019-01-12 17:55:46','2019-01-12 17:55:46');
INSERT INTO mark_details VALUES('32','32','100','80.00','2019-01-12 17:55:46','2019-01-12 17:55:46');



DROP TABLE IF EXISTS mark_distributions;

CREATE TABLE `mark_distributions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mark_distribution_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mark_percentage` decimal(8,2) NOT NULL,
  `is_exam` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `is_active` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO mark_distributions VALUES('1','100','33.00','no','yes','2019-01-12 17:26:45','2019-01-12 17:26:45');



DROP TABLE IF EXISTS marks;

CREATE TABLE `marks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO marks VALUES('1','1','1','1','1','1','2019-01-12 17:49:56','2019-01-12 17:49:56');
INSERT INTO marks VALUES('2','1','10','1','1','1','2019-01-12 17:49:56','2019-01-12 17:49:56');
INSERT INTO marks VALUES('3','1','3','1','2','1','2019-01-12 17:50:11','2019-01-12 17:50:11');
INSERT INTO marks VALUES('4','1','11','1','2','1','2019-01-12 17:50:11','2019-01-12 17:50:11');
INSERT INTO marks VALUES('5','1','1','1','1','2','2019-01-12 17:50:29','2019-01-12 17:50:29');
INSERT INTO marks VALUES('6','1','10','1','1','2','2019-01-12 17:50:29','2019-01-12 17:50:29');
INSERT INTO marks VALUES('7','1','3','1','2','2','2019-01-12 17:50:40','2019-01-12 17:50:40');
INSERT INTO marks VALUES('8','1','11','1','2','2','2019-01-12 17:50:40','2019-01-12 17:50:40');
INSERT INTO marks VALUES('9','1','1','1','1','3','2019-01-12 17:50:58','2019-01-12 17:50:58');
INSERT INTO marks VALUES('10','1','10','1','1','3','2019-01-12 17:50:58','2019-01-12 17:50:58');
INSERT INTO marks VALUES('11','1','3','1','2','3','2019-01-12 17:51:23','2019-01-12 17:51:23');
INSERT INTO marks VALUES('12','1','11','1','2','3','2019-01-12 17:51:23','2019-01-12 17:51:23');
INSERT INTO marks VALUES('13','1','1','1','1','4','2019-01-12 17:51:38','2019-01-12 17:51:38');
INSERT INTO marks VALUES('14','1','10','1','1','4','2019-01-12 17:51:38','2019-01-12 17:51:38');
INSERT INTO marks VALUES('15','1','3','1','2','4','2019-01-12 17:51:47','2019-01-12 17:51:47');
INSERT INTO marks VALUES('16','1','11','1','2','4','2019-01-12 17:51:47','2019-01-12 17:51:47');
INSERT INTO marks VALUES('17','1','6','6','3','5','2019-01-12 17:52:15','2019-01-12 17:52:15');
INSERT INTO marks VALUES('18','1','12','6','3','5','2019-01-12 17:52:15','2019-01-12 17:52:15');
INSERT INTO marks VALUES('19','1','2','6','4','5','2019-01-12 17:52:23','2019-01-12 17:52:23');
INSERT INTO marks VALUES('20','1','4','6','4','5','2019-01-12 17:52:23','2019-01-12 17:52:23');
INSERT INTO marks VALUES('21','1','2','6','4','6','2019-01-12 17:52:34','2019-01-12 17:52:34');
INSERT INTO marks VALUES('22','1','4','6','4','6','2019-01-12 17:52:34','2019-01-12 17:52:34');
INSERT INTO marks VALUES('23','1','6','6','3','6','2019-01-12 17:52:42','2019-01-12 17:52:42');
INSERT INTO marks VALUES('24','1','12','6','3','6','2019-01-12 17:52:42','2019-01-12 17:52:42');
INSERT INTO marks VALUES('25','1','2','6','4','7','2019-01-12 17:52:54','2019-01-12 17:52:54');
INSERT INTO marks VALUES('26','1','4','6','4','7','2019-01-12 17:52:54','2019-01-12 17:52:54');
INSERT INTO marks VALUES('27','1','6','6','3','7','2019-01-12 17:55:26','2019-01-12 17:55:26');
INSERT INTO marks VALUES('28','1','12','6','3','7','2019-01-12 17:55:26','2019-01-12 17:55:26');
INSERT INTO marks VALUES('29','1','6','6','3','8','2019-01-12 17:55:37','2019-01-12 17:55:37');
INSERT INTO marks VALUES('30','1','12','6','3','8','2019-01-12 17:55:37','2019-01-12 17:55:37');
INSERT INTO marks VALUES('31','1','2','6','4','8','2019-01-12 17:55:46','2019-01-12 17:55:46');
INSERT INTO marks VALUES('32','1','4','6','4','8','2019-01-12 17:55:46','2019-01-12 17:55:46');



DROP TABLE IF EXISTS messages;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO messages VALUES('1','2019-01-14 16:01:02','Testing','<p>Testing<br></p>','1','2019-01-14 16:30:02','2019-01-14 16:30:02');
INSERT INTO messages VALUES('2','2019-01-14 16:01:04','Testing','<p>Testing<br></p>','1','2019-01-14 16:37:04','2019-01-14 16:37:04');
INSERT INTO messages VALUES('3','2019-01-14 16:01:18','Testing','<p>Testing<br></p>','1','2019-01-14 16:37:18','2019-01-14 16:37:18');
INSERT INTO messages VALUES('4','2019-01-14 16:01:14','Testing','<p>Testing<br></p>','1','2019-01-14 16:38:14','2019-01-14 16:38:14');
INSERT INTO messages VALUES('5','2019-01-14 16:01:45','Testing','<p>Testing<br></p>','1','2019-01-14 16:43:45','2019-01-14 16:43:45');



DROP TABLE IF EXISTS migrations;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS notices;

CREATE TABLE `notices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `heading` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS page_contents;

CREATE TABLE `page_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `page_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_content` longtext COLLATE utf8mb4_unicode_ci,
  `meta_data` longtext COLLATE utf8mb4_unicode_ci,
  `seo_meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `seo_meta_description` text COLLATE utf8mb4_unicode_ci,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'english',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS pages;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS parents;

CREATE TABLE `parents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parent_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `m_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_profession` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_profession` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO parents VALUES('1','51','Rifat Shikdar','Kobir Shikdar','Tania Shikdar','Job','House Wife','+৮৮০৭৮৮৩১১৫','৮৯ বরকত ক্যাম্প, পশ্চিমতলা কুমিল্লা','2019-01-10 14:51:17','2019-01-10 14:51:17');
INSERT INTO parents VALUES('2','52','Abdullah Khan','Kobir Khan','sonia Khan','Job','House wife','+৮৮০৭৮৮৩১১৫','৭০ হাজী , দক্ষিনতলা খুলনা','2019-01-10 14:53:01','2019-01-14 17:03:50');
INSERT INTO parents VALUES('3','54','Lalima Sayeed','Kobir Shikdar','sonia Khan','Job','House wife','+৮৮০৭৮৮৩১১৫','৭১ হাজী গলি, নতুনহাট কুমিল্লা','2019-01-10 14:54:10','2019-01-14 17:03:30');
INSERT INTO parents VALUES('4','56','Kobir Shikdar','Kobir Shikdar','sonia Khan','Job','House wife','+৮৮০৭৮৮৩১১৫','৫৮ হাজী চিপা, নতুনটাউন ঢাকা','2019-01-10 14:55:55','2019-01-14 17:02:20');
INSERT INTO parents VALUES('5','57','Kobir Shikdar','Kobir Shikdar','sonia Khan','Job','House wife','+৮৮০৭৮৮৩১১৫','২৫ করিমউদ্দিন গলি, লেইকতলা কুমিল্লা','2019-01-10 14:57:53','2019-01-14 17:02:08');
INSERT INTO parents VALUES('6','59','Aladdin Khalil','Kobir Shikdar','sonia Khan','Job','House wife','+৮৮০৭৮৮৩১১৫','২৪ করিমউদ্দিন ক্যাম্প, নতুনহাট ঢাকা','2019-01-10 15:01:02','2019-01-10 15:01:02');
INSERT INTO parents VALUES('7','73','Devidas  Mitra','Devidas Mitra','Devi Mitra','Businessman','Housewife','0152140000','Lumbyholmvej 1854 2374 Hovborg, Denmark','2019-01-12 13:04:09','2019-01-12 13:04:09');
INSERT INTO parents VALUES('8','75','Shaahir Salah','Shaahir Salah','Mrs. Salah','Businessman','Housewife','01521450000','3144 Feked Kossuth Lajos u. 36., Hungary','2019-01-12 13:07:52','2019-01-12 13:07:52');
INSERT INTO parents VALUES('9','76','Fareed Abid','Fareed Abid','Mrs. Abid','Businessman','Housewife','01521434325','','2019-01-12 13:08:45','2019-01-12 13:08:45');
INSERT INTO parents VALUES('10','77','Mudrik Shahin','Mudrik Shahin','Mrs. Shahin','Businessman','Housewife','01521456556','','2019-01-12 13:10:08','2019-01-12 13:10:08');
INSERT INTO parents VALUES('11','78','Muntasir Farag','Muntasir Farag','Mrs.  Farag','Businessman','Housewife','01521434365','','2019-01-12 13:11:05','2019-01-12 13:11:05');
INSERT INTO parents VALUES('12','79','Haani Ali','Haani Ali','Mrs. Ali','Businessman','Housewife','0152145626','','2019-01-12 13:14:00','2019-01-12 13:14:00');



DROP TABLE IF EXISTS password_resets;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS payee_payers;

CREATE TABLE `payee_payers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO payee_payers VALUES('1','Student','payer','','2019-01-12 16:41:43','2019-01-12 16:44:12');
INSERT INTO payee_payers VALUES('2','Teacher','payee','','2019-01-12 16:41:56','2019-01-12 16:44:02');



DROP TABLE IF EXISTS payment_methods;

CREATE TABLE `payment_methods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO payment_methods VALUES('1','Cash','2019-01-12 16:46:56','2019-01-12 16:46:56');
INSERT INTO payment_methods VALUES('2','Check','2019-01-12 16:47:05','2019-01-12 16:47:54');



DROP TABLE IF EXISTS permission_roles;

CREATE TABLE `permission_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS permissions;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS phone_call_logs;

CREATE TABLE `phone_call_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `call_type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS picklists;

CREATE TABLE `picklists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO picklists VALUES('1','Designation','Vice Principal','2019-01-10 13:59:52','2019-01-10 13:59:52');
INSERT INTO picklists VALUES('2','Religion','Christian','2019-01-10 14:01:09','2019-01-10 14:01:09');
INSERT INTO picklists VALUES('3','Religion','Islam','2019-01-10 14:01:37','2019-01-10 14:01:37');
INSERT INTO picklists VALUES('4','Religion','Hindu','2019-01-10 14:01:54','2019-01-10 14:01:54');
INSERT INTO picklists VALUES('5','Designation','Science Teacher','2019-01-10 14:02:22','2019-01-10 14:02:22');
INSERT INTO picklists VALUES('6','Designation','Math Teacher','2019-01-10 14:02:57','2019-01-10 14:02:57');
INSERT INTO picklists VALUES('7','Designation','English Teacher','2019-01-10 14:03:34','2019-01-10 14:03:34');
INSERT INTO picklists VALUES('8','Designation','Computer Teacher','2019-01-10 14:28:17','2019-01-10 14:30:21');
INSERT INTO picklists VALUES('9','Designation','Health Teacher','2019-01-10 14:28:53','2019-01-10 14:30:02');
INSERT INTO picklists VALUES('11','Designation','Dance Teacher','2019-01-10 14:31:56','2019-01-10 14:31:56');



DROP TABLE IF EXISTS post_categories;

CREATE TABLE `post_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trans_category` text COLLATE utf8mb4_unicode_ci,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS post_contents;

CREATE TABLE `post_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_content` longtext COLLATE utf8mb4_unicode_ci,
  `meta_data` longtext COLLATE utf8mb4_unicode_ci,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'english',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS posts;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS sections;

CREATE TABLE `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `section_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_teacher_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `rank` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO sections VALUES('1','Class 1-A-2019','101','1','5','1','1','40','2019-01-10 14:40:28','2019-01-10 14:43:50');
INSERT INTO sections VALUES('2','Class 1-B-2019','102','1','3','1','2','40','2019-01-10 14:41:53','2019-01-10 14:44:04');
INSERT INTO sections VALUES('3','Class 2-A-2019','201','6','4','1','1','40','2019-01-10 14:44:56','2019-01-10 14:44:56');
INSERT INTO sections VALUES('4','Class 2-B-2019','202','6','6','1','2','40','2019-01-10 14:45:26','2019-01-12 17:53:45');
INSERT INTO sections VALUES('5','Class 3-A-2019','301','11','8','1','1','40','2019-01-10 14:46:04','2019-01-10 14:46:04');
INSERT INTO sections VALUES('6','Class 3-B-2019','302','11','7','1','2','40','2019-01-10 14:47:24','2019-01-10 14:47:24');
INSERT INTO sections VALUES('7','Class 1-A-2019','101','2','9','1','1','40','2019-01-12 12:54:34','2019-01-12 12:54:34');
INSERT INTO sections VALUES('8','Class 1-B-2019','102','2','10','1','2','40','2019-01-12 12:55:24','2019-01-12 12:55:24');
INSERT INTO sections VALUES('9','Class 2-A-2019','201','7','11','1','1','40','2019-01-12 12:56:27','2019-01-12 12:56:27');
INSERT INTO sections VALUES('10','Class 2-B-2019','202','7','12','1','2','40','2019-01-12 12:57:25','2019-01-12 12:57:25');
INSERT INTO sections VALUES('11','Class 3-A-2019','301','12','13','1','1','40','2019-01-12 12:57:58','2019-01-12 12:57:58');
INSERT INTO sections VALUES('12','Class 3-B-2019','302','12','14','1','2','40','2019-01-12 12:58:27','2019-01-12 12:58:27');
INSERT INTO sections VALUES('13','Class 1-A-2018','101','16','15','1','1','40','2019-01-13 16:29:49','2019-01-13 16:29:49');
INSERT INTO sections VALUES('14','Class 1-B-2018','102','16','16','1','2','40','2019-01-13 16:30:35','2019-01-13 16:30:35');



DROP TABLE IF EXISTS settings;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO settings VALUES('1','academic_year','3','','2019-01-15 13:01:06');
INSERT INTO settings VALUES('2','timezone','Asia/Omsk','','2018-12-24 16:15:10');
INSERT INTO settings VALUES('3','currency_symbol','৳','','2018-12-24 16:15:10');
INSERT INTO settings VALUES('4','mail_type','smtp','','2019-01-09 13:09:10');
INSERT INTO settings VALUES('5','logo','logo.png','','2019-01-02 17:20:11');
INSERT INTO settings VALUES('6','paypal_currency','USD','','');
INSERT INTO settings VALUES('7','stripe_currency','USD','','');
INSERT INTO settings VALUES('8','backend_direction','ltr','','');
INSERT INTO settings VALUES('9','active_theme','default','','');
INSERT INTO settings VALUES('10','disabled_website','yes','','');
INSERT INTO settings VALUES('11','copyright_text','&copy; Copyright 2018. All Rights Reserved. | This template is made with <i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i> by <a href=\"https://colorlib.com\">Colorlib</a>','','');
INSERT INTO settings VALUES('12','school_name','Nomad School','2018-12-24 16:15:10','2018-12-24 16:15:10');
INSERT INTO settings VALUES('13','site_title','Nomad School','2018-12-24 16:15:10','2018-12-24 16:15:10');
INSERT INTO settings VALUES('14','phone','01521434310','2018-12-24 16:15:10','2018-12-24 16:15:10');
INSERT INTO settings VALUES('15','email','uzzal@nomadtech.com.bd','2018-12-24 16:15:10','2018-12-24 16:15:10');
INSERT INTO settings VALUES('18','from_email','nomadtechbd@gmail.com','2019-01-09 13:09:10','2019-01-09 13:09:10');
INSERT INTO settings VALUES('19','from_name','Nomad School','2019-01-09 13:09:10','2019-01-09 13:09:10');
INSERT INTO settings VALUES('20','smtp_host','smtp.gmail.com','2019-01-09 13:09:10','2019-01-09 13:09:10');
INSERT INTO settings VALUES('21','smtp_port','587','2019-01-09 13:09:10','2019-01-09 13:09:10');
INSERT INTO settings VALUES('22','smtp_username','nomadtechbd@gmail.com','2019-01-09 13:09:10','2019-01-09 13:09:10');
INSERT INTO settings VALUES('23','smtp_password','Bambi_0110','2019-01-09 13:09:10','2019-01-09 13:09:10');
INSERT INTO settings VALUES('24','smtp_encryption','tls','2019-01-09 13:09:10','2019-01-09 13:09:10');
INSERT INTO settings VALUES('25','branch','1','2019-01-10 12:51:38','2019-01-15 13:01:45');



DROP TABLE IF EXISTS site_navigation_items;

CREATE TABLE `site_navigation_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `navigation_id` int(11) NOT NULL,
  `menu_label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci,
  `page_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `css_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_order` int(11) NOT NULL DEFAULT '100',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS site_navigations;

CREATE TABLE `site_navigations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS sms_logs;

CREATE TABLE `sms_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `receiver` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` int(11) NOT NULL,
  `academic_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS staff_attendances;

CREATE TABLE `staff_attendances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `attendance` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO staff_attendances VALUES('1','48','2019-01-13','1','1','2019-01-13 12:12:35','2019-01-13 12:12:35');
INSERT INTO staff_attendances VALUES('2','47','2019-01-13','1','1','2019-01-13 12:12:35','2019-01-13 12:12:35');
INSERT INTO staff_attendances VALUES('3','46','2019-01-13','2','1','2019-01-13 12:12:35','2019-01-13 12:12:35');
INSERT INTO staff_attendances VALUES('4','45','2019-01-13','2','1','2019-01-13 12:12:35','2019-01-13 12:12:35');
INSERT INTO staff_attendances VALUES('5','44','2019-01-13','3','1','2019-01-13 12:12:35','2019-01-13 12:12:35');
INSERT INTO staff_attendances VALUES('6','43','2019-01-13','3','1','2019-01-13 12:12:35','2019-01-13 12:12:35');



DROP TABLE IF EXISTS student_attendances;

CREATE TABLE `student_attendances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `attendance` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO student_attendances VALUES('1','25','16','13','2019-01-12','1','2019-01-14 13:33:21','2019-01-14 13:33:21');
INSERT INTO student_attendances VALUES('2','26','16','13','2019-01-12','1','2019-01-14 13:33:21','2019-01-14 13:33:21');
INSERT INTO student_attendances VALUES('3','25','16','13','2019-01-13','1','2019-01-14 13:33:27','2019-01-14 13:33:27');
INSERT INTO student_attendances VALUES('4','26','16','13','2019-01-13','1','2019-01-14 13:33:27','2019-01-14 13:33:27');
INSERT INTO student_attendances VALUES('5','25','16','13','2019-01-14','1','2019-01-14 13:33:32','2019-01-14 13:33:32');
INSERT INTO student_attendances VALUES('6','26','16','13','2019-01-14','1','2019-01-14 13:33:32','2019-01-14 13:33:32');
INSERT INTO student_attendances VALUES('7','27','16','14','2019-01-14','1','2019-01-14 13:33:50','2019-01-14 13:33:50');
INSERT INTO student_attendances VALUES('8','28','16','14','2019-01-14','1','2019-01-14 13:33:50','2019-01-14 13:33:50');
INSERT INTO student_attendances VALUES('9','27','16','14','2019-01-13','2','2019-01-14 13:33:55','2019-01-14 13:33:55');
INSERT INTO student_attendances VALUES('10','28','16','14','2019-01-13','2','2019-01-14 13:33:55','2019-01-14 13:33:55');
INSERT INTO student_attendances VALUES('11','1','1','1','2019-01-14','1','2019-01-14 17:14:19','2019-01-14 17:14:19');
INSERT INTO student_attendances VALUES('12','10','1','1','2019-01-14','1','2019-01-14 17:14:19','2019-01-14 17:14:19');
INSERT INTO student_attendances VALUES('13','1','1','1','2019-01-15','1','2019-01-15 11:32:26','2019-01-15 11:32:26');
INSERT INTO student_attendances VALUES('14','10','1','1','2019-01-15','1','2019-01-15 11:32:26','2019-01-15 11:32:26');



DROP TABLE IF EXISTS student_groups;

CREATE TABLE `student_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS student_payments;

CREATE TABLE `student_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS student_sessions;

CREATE TABLE `student_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `roll` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `optional_subject` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO student_sessions VALUES('1','3','1','1','1','001','','2019-01-10 15:09:53','2019-01-10 15:09:53');
INSERT INTO student_sessions VALUES('2','3','2','6','4','003','','2019-01-10 15:25:23','2019-01-10 15:25:23');
INSERT INTO student_sessions VALUES('3','3','3','1','2','002','','2019-01-10 15:25:49','2019-01-10 15:25:49');
INSERT INTO student_sessions VALUES('4','3','4','6','4','004','','2019-01-10 15:30:54','2019-01-10 15:30:54');
INSERT INTO student_sessions VALUES('5','3','5','11','5','001','','2019-01-10 15:33:21','2019-01-10 15:33:21');
INSERT INTO student_sessions VALUES('6','3','6','6','3','001','','2019-01-10 15:35:29','2019-01-10 15:35:29');
INSERT INTO student_sessions VALUES('7','3','7','11','5','002','','2019-01-10 15:35:33','2019-01-10 15:35:33');
INSERT INTO student_sessions VALUES('8','3','8','11','6','003','','2019-01-10 15:57:20','2019-01-10 15:57:20');
INSERT INTO student_sessions VALUES('9','3','9','11','6','004','','2019-01-10 17:34:26','2019-01-10 17:34:26');
INSERT INTO student_sessions VALUES('10','3','10','1','1','003','','2019-01-12 10:34:17','2019-01-12 10:34:17');
INSERT INTO student_sessions VALUES('11','3','11','1','2','004','','2019-01-12 10:37:13','2019-01-12 10:37:13');
INSERT INTO student_sessions VALUES('12','3','12','6','3','002','','2019-01-12 10:42:26','2019-01-12 10:42:26');
INSERT INTO student_sessions VALUES('13','3','13','2','7','001','','2019-01-12 13:04:21','2019-01-12 13:04:21');
INSERT INTO student_sessions VALUES('14','3','14','2','7','002','','2019-01-12 13:46:22','2019-01-12 13:46:22');
INSERT INTO student_sessions VALUES('15','3','15','7','9','001','','2019-01-12 13:50:24','2019-01-12 13:50:24');
INSERT INTO student_sessions VALUES('16','3','16','7','10','002','','2019-01-12 14:27:03','2019-01-12 14:27:03');
INSERT INTO student_sessions VALUES('17','3','17','7','9','003','','2019-01-12 14:31:58','2019-01-12 14:31:58');
INSERT INTO student_sessions VALUES('18','3','18','7','10','004','','2019-01-12 14:35:53','2019-01-12 14:35:53');
INSERT INTO student_sessions VALUES('19','3','19','2','8','003','','2019-01-12 14:40:09','2019-01-12 14:43:28');
INSERT INTO student_sessions VALUES('20','3','20','2','8','004','','2019-01-12 14:42:52','2019-01-12 14:42:52');
INSERT INTO student_sessions VALUES('21','3','21','12','11','001','','2019-01-12 14:46:42','2019-01-12 14:46:42');
INSERT INTO student_sessions VALUES('22','3','22','12','11','002','','2019-01-12 14:52:41','2019-01-12 14:52:41');
INSERT INTO student_sessions VALUES('23','3','23','12','12','003','','2019-01-12 14:59:29','2019-01-12 14:59:29');
INSERT INTO student_sessions VALUES('24','3','24','12','12','004','','2019-01-12 15:01:33','2019-01-12 15:01:33');
INSERT INTO student_sessions VALUES('25','1','25','16','13','001','','2019-01-13 17:15:46','2019-01-13 17:15:46');
INSERT INTO student_sessions VALUES('26','1','26','16','13','002','','2019-01-13 17:19:05','2019-01-13 17:19:05');
INSERT INTO student_sessions VALUES('27','1','27','16','14','003','','2019-01-13 17:22:58','2019-01-13 17:22:58');
INSERT INTO student_sessions VALUES('28','1','28','16','14','004','','2019-01-13 17:26:08','2019-01-13 17:26:08');



DROP TABLE IF EXISTS students;

CREATE TABLE `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blood_group` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `register_no` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activities` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO students VALUES('1','61','6','Asad','Tannous','2011-01-10','Male','','Islam','+৮৮০৯৯০৩১৫৪','২৪ করিমউদ্দিন ক্যাম্প, নতুনহাট ঢাকা','Dhaka','Bangladesh','Sa-19-1-001','','','','2019-01-10 15:09:53','2019-01-14 16:37:54');
INSERT INTO students VALUES('2','62','3','Jarin','Tasneem','2008-12-31','Female','','Islam','','৫৩ করিমউদ্দিন বাইপাস, পূর্বহাট কুমিল্লা','Dhaka','Bangladesh','Sa-19-2-003','','','','2019-01-10 15:25:23','2019-01-14 17:05:30');
INSERT INTO students VALUES('3','63','5','Ahsan','Khan','2011-01-10','Male','','Islam','+৮৮০৬৪৬৪৬৬১','৪৫ করিমউদ্দিন ক্যাম্প, পূর্বটাউন রাজশাহী','Rajshi','Bangladesh','Sa-19-01-002','','','','2019-01-10 15:25:49','2019-01-14 17:04:24');
INSERT INTO students VALUES('4','64','2','Imrul','Tabassum','2009-01-01','Female','A+','Islam','','৩২ বরকত বাইপাস, পোর্টখানা কুমিল্লা','Dhaka','Bangladesh','Sa-19-2-004','','','','2019-01-10 15:30:54','2019-01-14 17:05:49');
INSERT INTO students VALUES('5','65','3','Ahsan','Shikdar','2011-01-01','Male','B+','Islam','','৪৩ করিমউদ্দিন ব্রীজ, নতুনটাউন ঢাকা','Dhaka','Bangladesh','Sa-19-3-001','','','','2019-01-10 15:33:21','2019-01-14 17:06:11');
INSERT INTO students VALUES('6','66','4','Barirah','Talibah','2012-01-10','Female','AB+','Islam','0168000000','৪৫ করিমউদ্দিন ক্যাম্প, পূর্বটাউন রাজশাহী','Rajshi','Bangladesh','Sa-19-01-001','','','','2019-01-10 15:35:29','2019-01-10 15:35:29');
INSERT INTO students VALUES('7','67','1','Zerin','Sheikh','2010-01-01','Female','','Islam','','২০ বরকত চিপা, লেইকহাট বরিশাল','Dhaka','Bangladesh','Sa-19-03-002','','','','2019-01-10 15:35:33','2019-01-14 17:06:25');
INSERT INTO students VALUES('8','68','2','Hasnat','Khan','2009-01-01','Female','B+','Islam','','৭৮ বরকত চিপা, পশ্চিমটাউন ঢাকা','Dhaka','Bangladesh','Sa-19-3-003','','','','2019-01-10 15:57:20','2019-01-14 17:06:40');
INSERT INTO students VALUES('9','69','1','Jarin','Ali','2011-12-31','Female','A-','Islam','','৭২ হাজী ব্রীজ, উত্তরটাউন বরিশাল','Dhaka','Bangladesh','Sa-19-3-004','','','','2019-01-10 17:34:26','2019-01-14 17:06:57');
INSERT INTO students VALUES('10','70','4','Alima','Buhaisah','2012-01-11','Female','','Islam','+৮৮০৪৭৫৪৮৫৯','৮৪ করিমউদ্দিন ক্যাম্প, পোর্টটাউন ঢাকা','Dhaka','Bangladesh','Sa-19-01-003','','','','2019-01-12 10:34:17','2019-01-12 10:34:17');
INSERT INTO students VALUES('11','71','6','Laboni','Tabasasum','2013-01-11','Female','A+','Islam','+৮৮০৬৪৪৩০২৪','৪ বরকত ব্রীজ, পূর্বতলা খুলনা','Khulna','Bangladesh','Sa-19-01-004','','','','2019-01-12 10:37:13','2019-01-14 17:04:42');
INSERT INTO students VALUES('12','72','4','Karim','Sikder','2011-01-10','Male','A+','Islam','+৮৮০৪২৭৫৪৭৬','৬৬ হাজী  , নতুনখানা রাজশাহী','Rajshi','Bangladesh','Sa-19-02-002','','','','2019-01-12 10:42:26','2019-01-14 17:05:07');
INSERT INTO students VALUES('13','74','7','Rafah','Nihad','2011-01-12','Female','A+','Islam','','','Dhaka','Bangladesh','Mir-19-01-001','','','','2019-01-12 13:04:21','2019-01-12 15:19:41');
INSERT INTO students VALUES('14','80','12','Hudhaifah','Nawwaf','2011-01-12','Male','A+','Islam','','','Dhaka','Bangladesh','Mir-19-01-002','','','','2019-01-12 13:46:22','2019-01-12 13:46:22');
INSERT INTO students VALUES('15','81','9','Warda','Mustafa','2011-01-12','Female','B+','Islam','','','Dhaka','Bangladesh','Mir-19-02-001','','','','2019-01-12 13:50:24','2019-01-12 13:50:24');
INSERT INTO students VALUES('16','82','9','Hur','Tahan','2011-01-12','Female','B-','Islam','','','Dhaka','Bangladesh','Mir-19-02-002','','','','2019-01-12 14:27:03','2019-01-12 14:27:03');
INSERT INTO students VALUES('17','83','12','Basma','Morcos','2011-01-12','Female','','Islam','','','Dhaka','Bangladesh','Mir-19-02-003','','','','2019-01-12 14:31:58','2019-01-12 14:31:58');
INSERT INTO students VALUES('18','84','11','Murshid','Quraishi','2011-01-12','Male','','Hindu','','','Dhaka','Bangladesh','Mir-19-02-004','','','','2019-01-12 14:35:53','2019-01-12 14:35:53');
INSERT INTO students VALUES('19','85','9','Rajih','Ayyub','2011-01-08','Male','','Islam','','','Dhaka','Bangladesh','Mir-19-01-003','','','','2019-01-12 14:40:09','2019-01-12 14:40:09');
INSERT INTO students VALUES('20','86','7','Falak','Maalouf','2011-01-03','Female','','Islam','','','Dhaka','Bangladesh','Mir-19-01-004','','','','2019-01-12 14:42:52','2019-01-12 14:42:52');
INSERT INTO students VALUES('21','87','7','Wahbiyah','Shamoun','2010-01-09','Female','','Islam','','','Dhaka','Bangladesh','Mir-19-03-001','','','','2019-01-12 14:46:42','2019-01-12 14:46:42');
INSERT INTO students VALUES('22','88','8','Iffah','Guirguis','2010-01-11','Male','B+','Islam','','','Dhaka','Bangladesh','Mir-19-03-002','','','','2019-01-12 14:52:41','2019-01-12 15:22:54');
INSERT INTO students VALUES('23','89','9','Ziad','Deeb','2011-01-09','Male','','Hindu','','','Dhaka','Bangladesh','Mir-19-03-003','','','','2019-01-12 14:59:29','2019-01-12 15:23:04');
INSERT INTO students VALUES('24','90','12','Aziz','Naser','2010-01-12','Male','','Islam','','','Dhaka','Bangladesh','Mir-19-03-004','','','','2019-01-12 15:01:33','2019-01-12 15:23:11');
INSERT INTO students VALUES('25','93','6','Alim','Ganem','2011-01-13','Male','A+','Islam','','','Dhaka','Bangladesh','Sav-18-01-001','','','','2019-01-13 17:15:46','2019-01-13 17:15:46');
INSERT INTO students VALUES('26','94','1','Zahir','Shamoun','2010-01-13','Male','B-','Islam','','','Dhaka','Bangladesh','Sav-18-01-002','','','','2019-01-13 17:19:05','2019-01-13 17:19:05');
INSERT INTO students VALUES('27','95','4','Dhakiy','Halabi','2011-01-13','Male','B-','Islam','','','Dhaka','Bangladesh','Sav-18-01-003','','','','2019-01-13 17:22:58','2019-01-13 17:22:58');
INSERT INTO students VALUES('28','96','2','Suhaimah','Hakimi','2011-01-13','Female','','Islam','','','Dhaka','Bangladesh','Sav-18-01-004','','','','2019-01-13 17:26:08','2019-01-13 17:26:08');



DROP TABLE IF EXISTS subjects;

CREATE TABLE `subjects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_id` int(11) NOT NULL,
  `full_mark` int(11) NOT NULL,
  `pass_mark` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO subjects VALUES('1','First Bengali','101','Theory','1','100','33','2019-01-10 17:39:42','2019-01-14 17:09:09');
INSERT INTO subjects VALUES('2','First-english','102','Theory','1','100','33','2019-01-10 17:40:08','2019-01-14 17:09:00');
INSERT INTO subjects VALUES('3','First-math','103','Theory','1','100','33','2019-01-10 17:40:40','2019-01-14 17:08:52');
INSERT INTO subjects VALUES('4','First-science','104','Theory','1','100','33','2019-01-10 17:41:38','2019-01-14 17:08:34');
INSERT INTO subjects VALUES('5','Second-science','201','Theory','6','100','33','2019-01-10 17:41:59','2019-01-14 17:09:53');
INSERT INTO subjects VALUES('6','Second-math','203','Theory','6','100','33','2019-01-10 17:42:23','2019-01-14 17:09:43');
INSERT INTO subjects VALUES('7','Second-english','202','Theory','6','100','33','2019-01-10 17:42:36','2019-01-14 17:09:34');
INSERT INTO subjects VALUES('8','Second-Bengali','204','Theory','6','100','33','2019-01-10 17:42:49','2019-01-14 17:09:24');
INSERT INTO subjects VALUES('9','Third-science','301','Theory','11','100','33','2019-01-10 17:44:03','2019-01-14 17:10:34');
INSERT INTO subjects VALUES('10','Third-math','303','Theory','11','100','33','2019-01-10 17:44:23','2019-01-14 17:10:27');
INSERT INTO subjects VALUES('11','Third-English','302','Theory','11','100','33','2019-01-10 17:44:39','2019-01-14 17:10:19');
INSERT INTO subjects VALUES('12','Third-Bengali','304','Theory','11','100','33','2019-01-10 17:44:54','2019-01-14 17:10:10');
INSERT INTO subjects VALUES('13','প্রথম-বাংলা','101','Theory','2','100','33','2019-01-12 13:29:50','2019-01-12 13:29:50');
INSERT INTO subjects VALUES('14','প্রথম-ইংরেজি','102','Theory','2','100','33','2019-01-12 13:30:46','2019-01-12 13:30:46');
INSERT INTO subjects VALUES('15','প্রথম-গণিত','103','Theory','2','100','33','2019-01-12 13:31:06','2019-01-12 13:31:06');
INSERT INTO subjects VALUES('16','প্রথম-বিজ্ঞান','104','Theory','2','100','33','2019-01-12 13:31:37','2019-01-12 13:31:37');
INSERT INTO subjects VALUES('17','দ্বিতীয়-বিজ্ঞান','201','Theory','7','100','33','2019-01-12 13:31:58','2019-01-12 13:31:58');
INSERT INTO subjects VALUES('18','দ্বিতীয়-গণিত','202','Theory','7','100','33','2019-01-12 13:32:21','2019-01-12 13:32:21');
INSERT INTO subjects VALUES('19','দ্বিতীয়-ইংরেজি','203','Theory','7','100','33','2019-01-12 13:32:47','2019-01-12 13:32:47');
INSERT INTO subjects VALUES('20','দ্বিতীয়-বাংলা','204','Theory','7','100','33','2019-01-12 13:33:07','2019-01-12 13:33:07');
INSERT INTO subjects VALUES('21','তৃতীয়-বিজ্ঞান','301','Theory','12','100','33','2019-01-12 13:33:24','2019-01-12 13:33:24');
INSERT INTO subjects VALUES('22','তৃতীয়-গণিত','302','Theory','12','100','33','2019-01-12 13:33:40','2019-01-12 13:33:40');
INSERT INTO subjects VALUES('23','তৃতীয়-ইংরেজি','303','Theory','12','100','33','2019-01-12 13:33:59','2019-01-12 13:33:59');
INSERT INTO subjects VALUES('24','তৃতীয়-বাংলা','304','Theory','12','100','33','2019-01-12 13:34:15','2019-01-12 13:34:15');
INSERT INTO subjects VALUES('25','প্রথম-বাংলা','101','Theory','3','100','33','2019-01-12 13:35:29','2019-01-12 13:35:29');
INSERT INTO subjects VALUES('26','প্রথম-ইংরেজি','102','Theory','3','100','33','2019-01-12 13:35:44','2019-01-12 13:35:44');
INSERT INTO subjects VALUES('27','প্রথম-গণিত','103','Theory','3','100','33','2019-01-12 13:36:13','2019-01-12 13:36:13');
INSERT INTO subjects VALUES('28','প্রথম-বিজ্ঞান','104','Theory','3','100','33','2019-01-12 13:36:34','2019-01-12 13:36:34');
INSERT INTO subjects VALUES('29','দ্বিতীয়-বিজ্ঞান','201','Theory','8','100','33','2019-01-12 13:38:55','2019-01-12 13:38:55');
INSERT INTO subjects VALUES('30','দ্বিতীয়-গণিত','202','Theory','8','100','33','2019-01-12 13:39:08','2019-01-12 13:39:08');
INSERT INTO subjects VALUES('31','দ্বিতীয়-ইংরেজি','203','Theory','8','100','33','2019-01-12 13:39:24','2019-01-12 13:39:24');
INSERT INTO subjects VALUES('32','দ্বিতীয়-বাংলা','104','Theory','8','100','33','2019-01-12 13:39:36','2019-01-12 13:39:36');
INSERT INTO subjects VALUES('33','তৃতীয়-বিজ্ঞান','301','Theory','13','100','33','2019-01-12 13:39:49','2019-01-12 13:39:49');
INSERT INTO subjects VALUES('34','তৃতীয়-গণিত','302','Theory','13','100','33','2019-01-12 13:40:04','2019-01-12 13:40:04');
INSERT INTO subjects VALUES('35','তৃতীয়-ইংরেজি','303','Theory','13','100','33','2019-01-12 13:40:19','2019-01-12 13:40:19');
INSERT INTO subjects VALUES('36','তৃতীয়-বাংলা','304','Theory','13','100','33','2019-01-12 13:40:32','2019-01-12 13:40:32');
INSERT INTO subjects VALUES('37','Computer Studies','105','Theory','1','100','33','2019-01-12 16:17:17','2019-01-12 16:17:17');
INSERT INTO subjects VALUES('38','English','101','Theory','16','100','33','2019-01-13 16:31:57','2019-01-13 16:31:57');
INSERT INTO subjects VALUES('39','Mathematics','102','Theory','16','100','33','2019-01-13 16:32:39','2019-01-13 16:32:39');
INSERT INTO subjects VALUES('40','Information Technology','103','Theory','16','100','33','2019-01-13 16:33:22','2019-01-13 16:33:22');
INSERT INTO subjects VALUES('41','Geography','104','Theory','16','100','33','2019-01-13 16:34:00','2019-01-13 16:34:00');



DROP TABLE IF EXISTS syllabus;

CREATE TABLE `syllabus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `class_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO syllabus VALUES('1','3','First-science','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','1','-2019.pdf','1','2019-01-12 10:37:05','2019-01-14 17:11:39');
INSERT INTO syllabus VALUES('2','3','First-math','<p><strong style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','1','-2019.pdf','1','2019-01-12 10:37:35','2019-01-14 17:11:30');
INSERT INTO syllabus VALUES('3','3','First-english','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','1','-2019.pdf','1','2019-01-12 10:38:53','2019-01-14 17:11:21');
INSERT INTO syllabus VALUES('4','3','First Bengali','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','1','-2019.pdf','1','2019-01-12 10:39:33','2019-01-14 17:11:12');
INSERT INTO syllabus VALUES('5','3','Second-science','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','6','-2019.pdf','1','2019-01-12 10:44:27','2019-01-14 17:12:28');
INSERT INTO syllabus VALUES('6','3','Second-math','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','6','-2019.pdf','1','2019-01-12 10:45:14','2019-01-14 17:12:19');
INSERT INTO syllabus VALUES('7','3','Second-english','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','6','-2019.pdf','1','2019-01-12 10:45:47','2019-01-14 17:12:06');
INSERT INTO syllabus VALUES('8','3','Second-Bengali','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','6','-2019.pdf','1','2019-01-12 10:46:21','2019-01-14 17:11:55');
INSERT INTO syllabus VALUES('9','3','Third-Bengali','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','11','-2019.pdf','1','2019-01-12 10:58:34','2019-01-14 17:13:08');
INSERT INTO syllabus VALUES('10','3','Third-English','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','11','-2019.pdf','1','2019-01-12 10:59:13','2019-01-14 17:13:01');
INSERT INTO syllabus VALUES('11','3','Third-math','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','11','-2019.pdf','1','2019-01-12 10:59:50','2019-01-14 17:12:53');
INSERT INTO syllabus VALUES('12','3','Third-science','<p><span style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span><br></p>','11','-2019.pdf','1','2019-01-12 11:00:16','2019-01-14 17:12:44');
INSERT INTO syllabus VALUES('13','1','Banlgla','<p><strong style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span><br></p>','16','lorem-ipsum-2019.docx','1','2019-01-13 16:38:50','2019-01-13 16:43:26');
INSERT INTO syllabus VALUES('14','1','Math Hw','<p><strong style=\"margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span><br></p>','16','math-hw-2019.docx','1','2019-01-13 16:42:18','2019-01-13 16:42:18');



DROP TABLE IF EXISTS teachers;

CREATE TABLE `teachers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `gender` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `religion` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `joining_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO teachers VALUES('3','43','Karim Shikdar','Math Teacher','1992-01-10','Male','Islam','+৮৮০৪৫৪৪৪৮৩','৮০ বরকত তলী, পোর্টটাউন খুলনা','2018-01-10','2019-01-10 14:19:17','2019-01-10 14:38:22');
INSERT INTO teachers VALUES('4','44','Masnoon Sikder','Computer Teacher','1990-12-30','Male','Islam','01521434311','১১ হাজী ক্যাম্প, উত্তরখানা কুমিল্লা','2019-01-01','2019-01-10 14:20:13','2019-01-14 17:07:59');
INSERT INTO teachers VALUES('5','45','Hasnat Khan','Science Teacher','1992-01-10','Male','Christian','+৮৮০২৩২৮২২২','২৯ করিমউদ্দিন , পশ্চিমতলা রাজশাহী','2018-01-10','2019-01-10 14:21:39','2019-01-10 14:21:39');
INSERT INTO teachers VALUES('6','46','Mehnaz Sheikh','Health Teacher','1993-01-10','Female','Islam','+৮৮০৪৫৩০৯৪১','৭৯ বরকত বাইপাস, পশ্চিমতলা ঢাকা','2018-01-10','2019-01-10 14:24:07','2019-01-10 14:37:24');
INSERT INTO teachers VALUES('7','47','Mahajabin Tabassum','English Teacher','1994-01-09','Female','Hindu','+৮৮০৫৯৭৭৫','১২ বরকত ক্যাম্প, পশ্চিমহাট সিলেট','2017-01-10','2019-01-10 14:26:20','2019-01-10 14:26:20');
INSERT INTO teachers VALUES('8','48','Rahmat Tabasasum','Dance Teacher','2018-12-30','Female','Islam','01855555555','৫৯ বরকত তলী, দক্ষিনটাউন চিটাগং','2019-01-01','2019-01-10 14:31:21','2019-01-14 17:07:39');
INSERT INTO teachers VALUES('9','49','Miss Sabrina Khan','Dance Teacher','2008-12-29','Female','Islam','01555555555','৩১ বরকত চিপা, নতুনখানা রাজশাহী','2018-12-30','2019-01-10 14:47:09','2019-01-14 17:16:12');
INSERT INTO teachers VALUES('10','50','Rahmat Khan','English Teacher','2009-12-31','Male','Islam','01666666666','২৮ হাজী চিপা, পূর্বখানা সিলেট','2018-12-30','2019-01-10 14:50:24','2019-01-14 17:16:02');
INSERT INTO teachers VALUES('11','53','Mr. Karim Ali','Health Teacher','2010-12-31','Male','Christian','01888888888','২ বরকত তলী, উত্তরটাউন ঢাকা','2019-01-03','2019-01-10 14:53:35','2019-01-14 17:15:45');
INSERT INTO teachers VALUES('12','55','Rahim Sheikh','Science Teacher','1998-12-30','Male','Islam','0144444444','৬৬ হাজী সড়ক, দক্ষিনটাউন চিটাগং','2019-01-08','2019-01-10 14:55:12','2019-01-14 17:15:37');
INSERT INTO teachers VALUES('13','58','Mr. Abdullah Sheikh','Computer Teacher','1989-01-01','Male','Islam','019999999999','৭৫ বরকত তলী, পূর্বহাট বরিশাল','2019-01-01','2019-01-10 14:57:57','2019-01-14 17:15:27');
INSERT INTO teachers VALUES('14','60','Jarin Tasneem','Math Teacher','1991-01-01','Female','Islam','01666666666','৫৩ করিমউদ্দিন বাইপাস, পূর্বহাট কুমিল্লা','2019-01-01','2019-01-10 15:01:03','2019-01-14 17:15:16');
INSERT INTO teachers VALUES('15','91','Shamal','English Teacher','2011-01-13','Male','Islam','972-846-6543','৩১ হাজী ব্রীজ, পশ্চিমহাট খুলনা','2018-01-01','2019-01-13 16:22:43','2019-01-13 16:22:43');
INSERT INTO teachers VALUES('16','92','Mulhim Mustafa','Computer Teacher','1992-01-13','Male','Islam','+৮৮০৫৫০৮৩৮৪','৩১ হাজী ব্রীজ, পশ্চিমহাট খুলনা','2017-01-13','2019-01-13 16:28:19','2019-01-13 16:28:19');



DROP TABLE IF EXISTS transactions;

CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trans_date` date NOT NULL,
  `account_id` int(11) NOT NULL,
  `trans_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `dr_cr` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chart_id` int(11) NOT NULL,
  `payee_payer_id` int(11) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `reference` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci,
  `note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO transactions VALUES('1','2019-01-11','4','income','1000.00','cr','2','1','1','1','','','1547290902assignment-2019.pdf','','1','2019-01-12 17:01:42','2019-01-12 17:01:42');
INSERT INTO transactions VALUES('2','2019-01-11','5','income','500.00','cr','2','1','1','1','1','','1547290988assignment-2019.pdf','','1','2019-01-12 17:03:08','2019-01-12 17:04:33');
INSERT INTO transactions VALUES('3','2019-01-11','6','expense','300.00','dr','1','2','1','1','','','','','1','2019-01-12 17:05:38','2019-01-12 17:05:38');
INSERT INTO transactions VALUES('4','2019-01-14','3','expense','900.00','dr','1','2','1','1','','','','','2','2019-01-13 15:01:45','2019-01-13 15:01:45');
INSERT INTO transactions VALUES('5','2019-01-14','1','income','500.00','cr','2','1','1','1','','','','','2','2019-01-13 15:02:28','2019-01-13 15:02:28');



DROP TABLE IF EXISTS transport_members;

CREATE TABLE `transport_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `member_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transport_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO transport_members VALUES('1','70','Student','1','2019-01-13 12:00:30','2019-01-13 12:00:30');
INSERT INTO transport_members VALUES('2','47','Teacher','1','2019-01-13 12:01:35','2019-01-13 12:01:35');
INSERT INTO transport_members VALUES('3','71','Student','1','2019-01-13 12:02:32','2019-01-13 12:02:32');
INSERT INTO transport_members VALUES('4','46','Teacher','1','2019-01-13 12:02:56','2019-01-13 12:02:56');
INSERT INTO transport_members VALUES('5','62','Student','1','2019-01-13 12:03:41','2019-01-13 12:03:41');



DROP TABLE IF EXISTS transport_vehicles;

CREATE TABLE `transport_vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_number` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO transport_vehicles VALUES('1','Mini Bus','Dhaka Metro 11','1','2019-01-13 11:54:44','2019-01-13 11:54:44');
INSERT INTO transport_vehicles VALUES('2','Private Car','Dhaka Metro 12','1','2019-01-13 11:56:52','2019-01-13 11:56:52');



DROP TABLE IF EXISTS transports;

CREATE TABLE `transports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `road_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `road_fare` decimal(8,2) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO transports VALUES('1','Badd to Gulshan 2','1','50.00','','1','2019-01-13 11:59:05','2019-01-13 11:59:05');



DROP TABLE IF EXISTS user_messages;

CREATE TABLE `user_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `read` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'n',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO user_messages VALUES('1','1','61','y','2019-01-14 16:30:02','2019-01-14 16:48:55');
INSERT INTO user_messages VALUES('2','3','61','y','2019-01-14 16:37:18','2019-01-14 16:48:52');
INSERT INTO user_messages VALUES('3','4','61','y','2019-01-14 16:38:14','2019-01-14 16:48:50');
INSERT INTO user_messages VALUES('4','5','61','y','2019-01-14 16:43:45','2019-01-14 16:48:47');



DROP TABLE IF EXISTS user_notices;

CREATE TABLE `user_notices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `notice_id` int(11) NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `academic_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS users;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO users VALUES('1','0','Admin','uzzal@nomadtech.com.bd','$2y$10$9GyxSVdj3FEZUweGPS6ISOFs2vRXWdiFOYjF0tAn9zXJ5GmpTEd/y','Admin','01521434310','Active','profile.png','#','#','#','#','1','jMRGFA8QbglVkHHpwX24AIWBa0SNuLL0pay5UmuapYK5VvWAX2u9yGUAvaD6','2018-12-24 16:13:23','2019-01-09 16:11:58');
INSERT INTO users VALUES('43','0','Karim Shikdar','karimshikdar@gmail.com','$2y$10$adfuzTCrAEPQNuZE1UJ2Te4FG4yaAH/ja1KqAMMr71GcizNogtkLq','Teacher','+৮৮০৪৫৪৪৪৮৩','Active','teachers/1547108357.jpg','','','','','1','','2019-01-10 14:19:17','2019-01-10 14:19:17');
INSERT INTO users VALUES('44','0','Masnoon Sikder','mamun@gmail.com','$2y$10$6iIZW9P7MWv/zGazKbNJUOs2UxLh/Xdt7cS.MtvH6oPlmbYb/mmvy','Teacher','01521434311','Active','teachers/1547108986.png','','','','','1','','2019-01-10 14:20:13','2019-01-14 17:07:59');
INSERT INTO users VALUES('45','0','Hasnat Khan','hasnatkhan@gmail.com','$2y$10$jl3SNoy6Sa9CjI.7tci2/un2PNNdaMVtGaDpT4SRCuWafOfASbjNq','Teacher','+৮৮০২৩২৮২২২','Active','teachers/1547108499.jpg','','','','','1','','2019-01-10 14:21:39','2019-01-10 14:21:39');
INSERT INTO users VALUES('46','0','Mehnaz Sheikh','mehnazsheikh@gmail.com','$2y$10$9Z3qtrgU4uwJ.cFeySQIrOuqDbhP7SHFTT33455UtDQdrEyU.iFeW','Teacher','+৮৮০৪৫৩০৯৪১','Active','teachers/1547108647.png','','','','','1','','2019-01-10 14:24:07','2019-01-10 14:24:07');
INSERT INTO users VALUES('47','0','Mahajabin Tabassum','mahajabintabassum@gmail.com','$2y$10$bNuhjPlwp1su9EBEpN9jcOhcztSSA.EoQA77RL1puzES9WgQKwewG','Teacher','+৮৮০৫৯৭৭৫','Active','teachers/1547108780.jpg','','','','','1','6PDazTkjA6vPoukiQgrYR1e0ADDeiJ7pH3ygLByHWXylZXkoJT0T7CvMu2YO','2019-01-10 14:26:20','2019-01-10 14:26:20');
INSERT INTO users VALUES('48','0','Rahmat Tabasasum','rohomot@gmail.com','$2y$10$pfDAic04qk4P7peInyC3LOt2UwKk7PPaIzSQxXrdWGjiq8CFcGTaG','Teacher','01855555555','Active','teachers/1547109420.jpg','','','','','1','','2019-01-10 14:31:21','2019-01-14 17:07:39');
INSERT INTO users VALUES('49','0','Miss Sabrina Khan','mehanaja.ali@tabasasuma.com','$2y$10$dae133cjdfFmE2glInOO1OvNYVLC/1qCnlUzHXZyJN1wHxogMRHya','Teacher','01555555555','Active','teachers/1547110066.jpeg','','','','','2','','2019-01-10 14:47:09','2019-01-14 17:16:12');
INSERT INTO users VALUES('50','0','Rahmat Khan','asikadara@ali.com','$2y$10$3sJa2nVEc1Ge90q0GbIe1OWYLS6WXbYLX0BA90cKHccpY4n9Bgfr2','Teacher','01666666666','Active','teachers/1547110314.jpg','','','','','2','','2019-01-10 14:50:24','2019-01-14 17:16:02');
INSERT INTO users VALUES('51','0','Rifat Shikdar','rifatshikdar@gmail.com','$2y$10$1e3gMtHpKlBP/NYpmEuaJus2dNvf.KaDxY3yZaAwQMV3XYPlNcOEG','Parent','+৮৮০৭৮৮৩১১৫','Deactivate','parents/1547110277.jpg','','','','','1','','2019-01-10 14:51:17','2019-01-10 14:51:17');
INSERT INTO users VALUES('52','0','Abdullah Khan','abdullahkhan@gmail.com','$2y$10$4NcFbPr.5mCKw1r170741upt/3UEr7euGiIejNkEJ8kkyO7K/rQaK','Parent','+৮৮০৭৮৮৩১১৫','Deactivate','parents/1547110381.jpg','','','','','1','','2019-01-10 14:53:01','2019-01-14 17:03:50');
INSERT INTO users VALUES('53','0','Mr. Karim Ali','lsekha@sekha.net','$2y$10$UnK2yv1n50qzUXsr0xAH7OvOi5ylR5y/T5gOTomuUrcwne4fX..e.','Teacher','01888888888','Active','teachers/1547110415.jpg','','','','','2','','2019-01-10 14:53:35','2019-01-14 17:15:45');
INSERT INTO users VALUES('54','0','Lalima Sayeed','imrulkhan@gmail.com','$2y$10$CFxqGa.afw5kkpsrHSnXkugvG1taBdj0CiYXg6km4nUDHjG/pIsiK','Parent','+৮৮০৭৮৮৩১১৫','Deactivate','parents/1547110450.jpg','','','','','1','','2019-01-10 14:54:10','2019-01-14 17:03:30');
INSERT INTO users VALUES('55','0','Rahim Sheikh','hasina.sekha@gmail.com','$2y$10$A8/BsNwqLSwu2MIMfKYYieHq.XHy/vwcTTlGUpsaAm0DdvHzEmQC.','Teacher','0144444444','Active','teachers/1547110512.jpg','','','','','2','','2019-01-10 14:55:12','2019-01-14 17:15:37');
INSERT INTO users VALUES('56','0','Kobir Shikdar','annantotabassum@gmail.com','$2y$10$ere65sbeCRCp0siIai0s6eHMzKEwWLfVLz8amrulzRByilgoLNZB.','Parent','+৮৮০৭৮৮৩১১৫','Deactivate','parents/1547110555.jpg','','','','','1','','2019-01-10 14:55:55','2019-01-14 17:02:20');
INSERT INTO users VALUES('57','0','Kobir Shikdar','jolil@gmail.com','$2y$10$dy4QJnm7RVE7/WmAJBcGjOrN31ZWxnnr0.x5ISOWTmxQbRu9iA25q','Parent','+৮৮০৭৮৮৩১১৫','Deactivate','parents/1547110673.jpg','','','','','1','','2019-01-10 14:57:53','2019-01-14 17:02:08');
INSERT INTO users VALUES('58','0','Mr. Abdullah Sheikh','masanuna.sikadara@khana.com','$2y$10$9pbFFh63WDH1oM1O7cvVJeVxzBv.JbRU.0fcrnEBkqnk72YxzcoAe','Teacher','019999999999','Active','teachers/1547110677.jpg','','','','','2','','2019-01-10 14:57:57','2019-01-14 17:15:27');
INSERT INTO users VALUES('59','0','Aladdin Khalil','aladdinkhalil@gmail.com','$2y$10$G8B5Vs2ZezgIS7EodHI/z.EuVw8upeXXxWINWSc.X3ZaE6DV18jSy','Parent','+৮৮০৭৮৮৩১১৫','Deactivate','parents/1547110861.jpg','','','','','1','','2019-01-10 15:01:02','2019-01-10 15:01:02');
INSERT INTO users VALUES('60','0','Jarin Tasneem','ytabasasuma@gmail.com','$2y$10$KFdZQWVUYId0c7EZBsuK2eax0X4WzlGBDwIXeiz7Zx4rtsTnNMapm','Teacher','01666666666','Active','teachers/1547110863.jpg','','','','','2','jTda1tfMvAYetiBXFVC3jcSSkuzqPOSjHJXdxGoyicA4Gowwfi8HKkJWn6YA','2019-01-10 15:01:03','2019-01-14 17:15:16');
INSERT INTO users VALUES('61','0','Asad Tannous','nomaduzzal@gmail.com','$2y$10$eNaxuLNXLi1Uou8gcoCU3.NyzgyWfC1nrQ/ZAKbj/JAW7RI2b19HS','Student','+৮৮০৯৯০৩১৫৪','Active','students/1547111393.jpg','','','','','1','','2019-01-10 15:09:53','2019-01-14 16:29:44');
INSERT INTO users VALUES('62','0','Jarin Tasneem','tabasasuma@gmail.com','$2y$10$HMvK3AdqSX7/6l.byyyjOOHQktBVKwHegoH7TLzU2dsHrav6jcUHO','Student','','Active','students/1547112370.jpg','','','','','1','','2019-01-10 15:25:23','2019-01-14 17:05:30');
INSERT INTO users VALUES('63','0','Ahsan Khan','ahsankhan@gmail.com','$2y$10$1S/zwENIXqixZcoKEO9t1.uj6F8I9jKqzeCBQsTRuvcDNYqJw0Vpa','Student','+৮৮০৬৪৬৪৬৬১','Active','students/1547112349.jpg','','','','','1','','2019-01-10 15:25:49','2019-01-14 17:04:24');
INSERT INTO users VALUES('64','0','Imrul Tabassum','sikadara@gmail.com','$2y$10$HLAoUdF/itDbuwQ7gykOUuMPJs2xtYmFhKTSsnw1so7Ah0f.CjPZa','Student','','Active','students/1547112654.jpg','','','','','1','','2019-01-10 15:30:54','2019-01-14 17:05:49');
INSERT INTO users VALUES('65','0','Ahsan Shikdar','ali.ahasana@yahoo.com','$2y$10$FFWxnZe0WJEckNW0oFxymu5xo3HGC735HKu3VXSGzDWtZXDdTeq1W','Student','','Active','students/1547112801.jpg','','','','','1','','2019-01-10 15:33:21','2019-01-14 17:06:11');
INSERT INTO users VALUES('66','0','Barirah Talibah','barirahtalibah@gmail.com','$2y$10$M4HPk036ZSWyZW0kLk/5XeNDGHirnCdkrmKQ3aH1slGhLUR7N7SPS','Student','0168000000','Active','students/1547112929.jpg','','','','','1','','2019-01-10 15:35:29','2019-01-10 15:35:29');
INSERT INTO users VALUES('67','0','Zerin Sheikh','tabasasuma.labani@yahoo.com','$2y$10$dmSh4gEg.lYUnnoGEolvfu7yU2Y/0frApqF8.vPW6bM2aJa1VNDDS','Student','','Active','students/1547112933.jpg','','','','','1','','2019-01-10 15:35:33','2019-01-14 17:06:25');
INSERT INTO users VALUES('68','0','Hasnat Khan','sikadara@khana.com','$2y$10$jiobWzN0tvpmflnf0M58gOnI4.0I6xvSn3m6Cxd5w/R/ElSTcZ1Q6','Student','','Active','students/1547114240.jpg','','','','','1','','2019-01-10 15:57:20','2019-01-14 17:06:40');
INSERT INTO users VALUES('69','0','Jarin Ali','imarula22@ali.com','$2y$10$hRhFn95Mh4TewjHZHeNM1unjr7xDlQfcu2Y1d/SLt8WKqAEzWLoI2','Student','','Active','students/1547120092.png','','','','','1','','2019-01-10 17:34:26','2019-01-14 17:06:57');
INSERT INTO users VALUES('70','0','Alima Buhaisah','alimabuhaisah@gmail.com','$2y$10$LEn45QyTMJlUcQRHRutYpOk02CNCBXzu7bFTST6FBZWKK9/vtFaiW','Student','+৮৮০৪৭৫৪৮৫৯','Active','students/1547267657.jpg','','','','','1','','2019-01-12 10:34:17','2019-01-12 10:34:17');
INSERT INTO users VALUES('71','0','Laboni Tabasasum','labonitabassum@gmail.com','$2y$10$qk2t/1FiizBF0WHIhjAwP.HjFuUPHLnUvwIaccG8kENG3UKdzQ1Ru','Student','+৮৮০৬৪৪৩০২৪','Active','students/1547267833.jpg','','','','','1','','2019-01-12 10:37:13','2019-01-14 17:04:42');
INSERT INTO users VALUES('72','0','Karim Sikder','korimshikdar@gmail.com','$2y$10$znIgH9aimLb1cyXfe4nC7.t0lvJfH2WBQZhhvh7eo/Qa3LyPF0on.','Student','+৮৮০৪২৭৫৪৭৬','Active','students/1547268146.jpg','','','','','1','','2019-01-12 10:42:26','2019-01-14 17:05:07');
INSERT INTO users VALUES('73','0','Devidas  Mitra','devidas@gmail.com','$2y$10$/6CfU24mY9DjeQFX68Y0juZLpkRnbTtjSnv3ngRP.tTO4Ylksg1hi','Parent','0152140000','Deactivate','parents/1547276747.png','','','','','2','','2019-01-12 13:04:09','2019-01-12 13:05:47');
INSERT INTO users VALUES('74','0','Rafah Nihad','rafahnihad@gmail.com','$2y$10$eXnCQyIt.MtGqBe6ujW4OOexeMOKvpOLanD88QJr1c/xo7T58iDvO','Student','','Active','students/1547276661.jpg','','','','','2','','2019-01-12 13:04:21','2019-01-12 15:19:41');
INSERT INTO users VALUES('75','0','Shaahir Salah','salah@gmail.com','$2y$10$.tuPHsa8rpcmw/QzP30qz.ZBkNCIG4ElAWCXnv21uHXzlJmBxcsTG','Parent','01521450000','Deactivate','parents/1547276872.jpg','','','','','2','','2019-01-12 13:07:52','2019-01-12 13:07:52');
INSERT INTO users VALUES('76','0','Fareed Abid','abid@gmail.com','$2y$10$38IePhX5bq6/2qM0v7INqOYEogmDHzF.otYqF6CueukIxGgQVXuES','Parent','01521434325','Deactivate','parents/1547276925.jpg','','','','','2','','2019-01-12 13:08:45','2019-01-12 13:08:45');
INSERT INTO users VALUES('77','0','Mudrik Shahin','shahin@gmail.com','$2y$10$RGIkPtYGoN1ouSxJrY6lLe4vAQHVnw0mw9qr32ZqmFFP2ZV7yjiKW','Parent','01521456556','Deactivate','parents/1547277008.jpg','','','','','2','','2019-01-12 13:10:08','2019-01-12 13:10:08');
INSERT INTO users VALUES('78','0','Muntasir Farag','farag@gmail.com','$2y$10$s9A6uHY1xwFlQyHmm47AWu8NsQ3lH1ClW1ylNIkEPlQS4vCnkdr06','Parent','01521434365','Deactivate','parents/1547277065.jpg','','','','','2','','2019-01-12 13:11:05','2019-01-12 13:11:05');
INSERT INTO users VALUES('79','0','Haani Ali','haani.ali@gmail.com','$2y$10$GQ8WVgLLlza25QfHaPukee4B0i.94rcg4ys7/GL0JJEJq15yu/Keu','Parent','0152145626','Deactivate','parents/1547277240.png','','','','','2','','2019-01-12 13:14:00','2019-01-12 13:14:00');
INSERT INTO users VALUES('80','0','Hudhaifah Nawwaf','hudhaifahnawwaf@gmail.com','$2y$10$sgk42DtgjFxwhwKpuxOWSuHFloi88ll8OeDMlJ2mfBqHUXWh5WbEC','Student','','Active','students/1547279182.jpg','','','','','2','','2019-01-12 13:46:22','2019-01-12 13:46:22');
INSERT INTO users VALUES('81','0','Warda Mustafa','wardamustafa@gmail.com','$2y$10$5FZKGCRQdXmkQ5YkSTTwHu9NjRcmFo6Txrd7Kdv/bnr6qDB9DLRZK','Student','','Active','students/1547279424.jpg','','','','','2','','2019-01-12 13:50:24','2019-01-12 13:50:24');
INSERT INTO users VALUES('82','0','Hur Tahan','hurtahan@gmail.com','$2y$10$HqCd3qUNt/9QZlDaBtXvHeIp5Zo7Yr1tkpO5EmlGmjaZ2E5iD3xyK','Student','','Active','students/1547281623.jpg','','','','','2','','2019-01-12 14:27:03','2019-01-12 14:27:03');
INSERT INTO users VALUES('83','0','Basma Morcos','basmamorcos@gmail.com','$2y$10$ytLPXVISKNgOUdDcUHsL3OyiyHOx9W3icuQMTH1RuUPBQy6M/N/h.','Student','','Active','students/1547281918.jpg','','','','','2','','2019-01-12 14:31:58','2019-01-12 14:31:58');
INSERT INTO users VALUES('84','0','Murshid Quraishi','murshidquraishi@gmail.com','$2y$10$KyWNlWUGXDTIiGuIVbcb2OO8sBe5ot78fp7MXvHqGWQQ.8d52u4jO','Student','','Active','students/1547282153.jpg','','','','','2','','2019-01-12 14:35:53','2019-01-12 14:35:53');
INSERT INTO users VALUES('85','0','Rajih Ayyub','rajihayyub@gmail.com','$2y$10$635yIeW2uRfDvkWYwxPXn./HhM3ULuOpjiOj37M/OAIZ2X/17gQlW','Student','','Active','students/1547282409.jpg','','','','','2','','2019-01-12 14:40:09','2019-01-12 14:40:09');
INSERT INTO users VALUES('86','0','Falak Maalouf','falakmaalouf@gmail.com','$2y$10$plTCnD6pZy6skTYObrpLLe4/otjXq9Vmhfw7uktU0eoLMg7KI7l3S','Student','','Active','students/1547282572.jpg','','','','','2','','2019-01-12 14:42:52','2019-01-12 14:42:52');
INSERT INTO users VALUES('87','0','Wahbiyah Shamoun','wahbiyahshamoun@gmail.com','$2y$10$sngKzWi.5ug2z3W90UgqH.e/JSm38vjQMUUxKixpeU/2BEyhPCen2','Student','','Active','students/1547282801.jpg','','','','','2','','2019-01-12 14:46:42','2019-01-12 14:46:42');
INSERT INTO users VALUES('88','0','Iffah Guirguis','iffahguirguis@gmail.com','$2y$10$4pHVbIo6W7lkcClqifa28.WBXweV5MBjup952/myAcyqz8x05Cj1q','Student','','Active','students/1547283161.jpg','','','','','2','','2019-01-12 14:52:41','2019-01-12 14:52:41');
INSERT INTO users VALUES('89','0','Ziad Deeb','Ziaddeeb@gmail.com','$2y$10$YgLRDV48EygXUDYuQhmc6u8VMkJ6MYCc2qe6gcPbcDR.ce5PbtMlS','Student','','Active','students/1547283569.jpg','','','','','2','','2019-01-12 14:59:29','2019-01-12 14:59:29');
INSERT INTO users VALUES('90','0','Aziz Naser','aziznaser@gmail.com','$2y$10$wWjrUJHDQCElpSL1pO2BUuqJrngkOMy80CpW5Xk5zVHhLV5L2PsR2','Student','','Active','students/1547283693.jpg','','','','','2','','2019-01-12 15:01:33','2019-01-12 15:01:33');
INSERT INTO users VALUES('91','0','Shamal','Shamal@gmail.com','$2y$10$7vfa3X.UI83d6ZAhD1DK8OndPYscg8PkrR6PVnoueBRPIne3P7sqm','Teacher','972-846-6543','Active','teachers/1547374963.jpg','','','','','1','','2019-01-13 16:22:43','2019-01-13 16:22:43');
INSERT INTO users VALUES('92','0','Mulhim Mustafa','mulhimmustafa@gmail.com','$2y$10$XuCnTrT3iYD0/biDYaf9lOjADQjNweUdIMr.YGIkmaqD7L80V.yqu','Teacher','+৮৮০৫৫০৮৩৮৪','Active','teachers/1547375299.jpg','','','','','1','2knJlSnM3sPFQgYY6G4OrcmlMM0dsSu98jniCKojxRzdUgMee8RBQQLjIx0P','2019-01-13 16:28:19','2019-01-13 16:28:19');
INSERT INTO users VALUES('93','0','Alim Ganem','alimganem@gmail.com','$2y$10$C27ZdwhMPiSiir.n4rJzduRXavoDxmydlf.DVDwZc7uekxgsh4IYG','Student','','Active','students/1547378146.jpg','','','','','1','','2019-01-13 17:15:46','2019-01-13 17:15:46');
INSERT INTO users VALUES('94','0','Zahir Shamoun','zahirshamoun@gmail.com','$2y$10$ekxPvgyywmqFZQjirbMaZ.s6D0niL9ZH8hvLvj4NKoV/42zRWqdZS','Student','','Active','students/1547378345.jpg','','','','','1','','2019-01-13 17:19:05','2019-01-13 17:19:05');
INSERT INTO users VALUES('95','0','Dhakiy Halabi','dhakiyhalabi@gmail.com','$2y$10$JQlz7/s80rHheN7YdA766eKa90y.YHBCQMGGgi5oISQjQAN7V16my','Student','','Active','students/1547378578.jpg','','','','','1','','2019-01-13 17:22:58','2019-01-13 17:22:58');
INSERT INTO users VALUES('96','0','Suhaimah Hakimi','suhaimahhakimi@gmail.com','$2y$10$tArE9xKZUMwnYa07A9759Oo4cboLw1zwbecrOFvux9jWvbluVB/eO','Student','','Active','students/1547378768.jpg','','','','','1','','2019-01-13 17:26:08','2019-01-13 17:26:08');



DROP TABLE IF EXISTS visitor_informations;

CREATE TABLE `visitor_informations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `purpose` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `in_time` time DEFAULT NULL,
  `out_time` time DEFAULT NULL,
  `number_of_person` int(11) DEFAULT NULL,
  `id_card` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE IF EXISTS website_languages;

CREATE TABLE `website_languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




