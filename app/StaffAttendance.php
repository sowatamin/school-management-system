<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffAttendance extends Model
{
    protected $table = 'staff_attendances';
    protected $fillable = array('user_id', 'date', 'attendance','branch_id');
}
