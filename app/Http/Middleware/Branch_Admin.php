<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Branch_Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        if (Auth::user()->user_type === 'Branch_Admin') {
//            return $next($request);
//        } else {
//            Auth::logout();
//        }

        if(Auth::User()->user_type != 'Branch_Admin'){
            return back()->with('error',('You dont have permission to access this feature !'));
        }
        return $next($request);
    }

}
