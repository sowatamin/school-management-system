<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use Illuminate\Validation\Rule;
use App\Mail\UserCreate;
use Validator;
use App\ParentModel;
use App\User;
use Image;
use Hash;
use DB;
use App\Utilities\Overrider;
use File;

class ParentController extends Controller
{

    public function index()
    {
        $parents = ParentModel::select('users.*', 'parents.id AS parent_id')
            ->join('users', 'users.id', '=', 'parents.user_id')
            ->where('users.branch_id', get_branch_option('branch'))
            ->orderBy('parents.id', 'DESC')
            ->get();

//        $parents = ParentModel::select('users.*', 'students.first_name', 'students.last_name', 'parents.id AS id')
//            ->join('users', 'users.id', '=', 'parents.user_id')
//            ->leftJoin('students', 'parents.id', '=', 'students.parent_id')
//            ->where('users.branch_id', get_branch_option('branch'))
//            ->orderBy('parents.id', 'DESC')
//            ->get();
        return view('backend.parents.parent-list', compact('parents'));
    }

    public function get_parents()
    {
        $parents = [];
        if (!isset($_GET['term'])) {
            $parents = ParentModel::leftJoin('users', 'parents.user_id', 'users.id')
                ->where('users.branch_id', get_branch_option('branch'))
                ->select('parents.id', 'parent_name as text')
                ->orderBy('parents.id', 'DESC')
                ->get();
        } else {
            $parents = ParentModel::leftJoin('users', 'parents.user_id', 'users.id')
                ->where('users.branch_id', get_branch_option('branch'))
                ->select('id', 'parent_name as text')
                ->where('parents.parent_name', 'like', '%' . $_GET['term'] . '%')
                ->orderBy('parents.id', 'DESC')
                ->get();
        }
        echo json_encode($parents);
        //return response()->json($parents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->ajax()) {
            return view('backend.parents.parent-add');
        } else {
            return view('backend.parents.modal.create');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        @ini_set('max_execution_time', 0);
        @set_time_limit(0);

        Overrider::load("Settings");

        $validator = Validator::make($request->all(), [
            'parent_name' => 'required|string|max:191',
            'f_name' => 'required|string|max:191',
            'm_name' => 'required',
            'f_profession' => 'nullable|string|max:191',
            'm_profession' => 'nullable|string|max:191',
            'phone' => 'required|numeric|unique:parents',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'image' => 'nullable|image|max:5120',
            'nid_number' => 'required|numeric|unique:parents',
            'nid_copy' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf|max:1024',
        ]);


        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return redirect('parents/create')
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $ImageName = 'profile.png';
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $ImageName = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(200, 160)->save(base_path('public/uploads/images/parents/') . $ImageName);
        }

        if ($request->hasFile('nid_copy')) {
            $image = $request->file('nid_copy');
            $nid_copy = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(600, 600)->save(base_path('public/uploads/images/nid/') . $nid_copy);
        }

        $branch = get_branch_option('branch');

        $user = new User();
        $user->name = $request->parent_name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->user_type = 'Parent';
        $user->phone = $request->phone;
        $user->image = 'parents/' . $ImageName;
        $user->branch_id = $branch;
        $user->status = 'Deactivate';
        $user->save();

        $parent = new ParentModel();
        $parent->user_id = $user->id;
        $parent->parent_name = $request->parent_name;
        $parent->f_name = $request->f_name;
        $parent->m_name = $request->m_name;
        $parent->f_profession = $request->f_profession;
        $parent->m_profession = $request->m_profession;
        $parent->phone = $request->phone;
        $parent->address = $request->address;
        $parent->nid_number = $request->nid_number;
        $parent->nid_copy = $nid_copy;
        $parent->save();

        Mail::to($user->email)->send(new UserCreate($user, $request->password));


        if (!$request->ajax()) {
            return redirect('parents')->with('success', 'Information has been added');
        } else {
            return response()->json(['result' => 'success', 'action' => 'store', 'message' => ('Information has been added successfully'), 'data' => $parent]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $parent = ParentModel::leftJoin('users', 'parents.user_id', 'users.id')
            ->where('parents.id', $id)
            ->select('parents.*', 'users.image', 'users.email')
            ->first();

        $student = ParentModel::join('users', 'parents.user_id', 'users.id')
            ->leftJoin('students', 'parents.id', 'students.parent_id')
            ->leftJoin('student_sessions', 'students.id', 'student_sessions.student_id')
            ->leftJoin('classes', 'student_sessions.class_id', 'classes.id')
            ->leftJoin('sections', 'student_sessions.section_id', 'sections.id')
            ->where('parents.id', $id)
            ->select('students.*')
            ->first();


        return view('backend.parents.parent-view', compact('parent', 'student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parent = ParentModel::select('*', 'parents.id AS id')
            ->join('users', 'users.id', '=', 'parents.user_id')
            ->where('parents.id', $id)
            ->first();
        return view('backend.parents.parent-edit', compact('parent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        @ini_set('max_execution_time', 300);
        @set_time_limit(300);

        Overrider::load("Settings");

        $parent = ParentModel::find($id);
        $this->validate($request, [
            'parent_name' => 'required|string|max:191',
            'f_name' => 'required|string|max:191',
            'm_name' => 'required|string|max:191',
            'f_profession' => 'nullable|string|max:191',
            'm_profession' => 'nullable|string|max:191',
            'phone' => 'required|numeric|unique:parents,phone,'.$parent->id.'',
            'email' => [
                'required',
                Rule::unique('users')->ignore($parent->user_id),
            ],
            'password' => 'nullable|min:6|confirmed',
            'image' => 'nullable|image|max:5120',
        ]);

        $branch = get_branch_option('branch');

        $parent->parent_name = $request->parent_name;
        $parent->f_name = $request->f_name;
        $parent->m_name = $request->m_name;
        $parent->f_profession = $request->f_profession;
        $parent->m_profession = $request->m_profession;
        $parent->phone = $request->phone;
        $parent->address = $request->address;
        $parent->save();

        $user = User::find($parent->user_id);
        $user->name = $request->parent_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }
        if ($request->hasFile('image')) {
            $exist_image = public_path("uploads/images/parents/{$user->image}");
            if (File::exists($exist_image)) {
                unlink($exist_image);
            }

            $image = $request->file('image');
            $ImageName = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(200, 160)->save(base_path('public/uploads/images/parents/') . $ImageName);
            $user->image = 'parents/' . $ImageName;
        }
        $user->branch_id = $branch;
        $user->save();

        if (!empty($request->password)) {
            Mail::to($user->email)->send(new UserCreate($user, $request->password));
        }

        return redirect('parents')->with('success', ('Information has been updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $parent = ParentModel::find($id);
        $parent->delete();
        $user = User::find($parent->user_id);
        $user->delete();

        return redirect('parents')->with('success', ('Information has been deleted'));
    }

    public function nid_create($id)
    {
        $nid = ParentModel::where('parents.id', $id)
            ->select('parents.*')
            ->first();

        return view('backend.parents.parent_nid', compact('nid'));
    }

    public function nid_store(Request $request)
    {
        $parent_id = $request->parent_id;

        @ini_set('max_execution_time', 0);
        @set_time_limit(0);

        Overrider::load("Settings");

        $parent = ParentModel::find($parent_id);

        $this->validate($request, [
            'nid_number' => 'required|numeric|unique:parents,nid_number,'.$parent->id.'',
            'nid_copy' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf|max:1024',
        ]);


        $user_nid_file = public_path("uploads/images/nid/{$parent->nid_copy}");
        if (File::exists($user_nid_file)) {
            unlink($user_nid_file);
        }


        $img_name = $_FILES['nid_copy']['name'];
        $ext = pathinfo($img_name);
        $ext = $ext['extension'];
        if ($ext === 'jpeg' || $ext === 'png' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'PNG') {
            $image = $request->file('nid_copy');
            $img = Image::make($image->getRealPath());
            $nid_copy = date('YmdHis') . "NS" . rand(5, 10) . '.' . $request->nid_copy->getClientOriginalExtension();
            $img->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();

            })->save(('public/uploads/images/nid/') . $nid_copy);
            $parent->nid_number = $request->nid_number;
            $parent->nid_copy = $nid_copy;
            $parent->save();
        } else {
            $nid_copy = date('YmdHis') . "NS" . rand(5, 10) . '.' . $request->nid_copy->getClientOriginalExtension();
            $request->nid_copy->move(('public/uploads/images/nid/'), $nid_copy);
            $parent->nid_number = $request->nid_number;
            $parent->nid_copy = $nid_copy;
            $parent->save();
        }

        return redirect('parents/' . $parent_id)->with('success', ('Information has been added'));
    }

    public function get_parents_list($parent_class_id = "", $parent_section_id = "")
    {
        if ($parent_class_id != "" && $parent_section_id!= "") {
            $parents = ParentModel::leftJoin('students', 'parents.id', 'students.parent_id')
                ->leftJoin('student_sessions', 'students.id', '=', 'student_sessions.student_id')
                ->leftJoin('users', 'parents.user_id', 'users.id')
                ->where('student_sessions.session_id', get_option('academic_year'))
                ->where('student_sessions.class_id', $parent_class_id)
                ->where('student_sessions.section_id', $parent_section_id)
                ->where('users.branch_id', get_branch_option('branch'))
                ->where('users.status', 'Active')
                ->select('parents.parent_name', 'users.phone')
                ->groupBy('parents.id')
                ->get();
            return json_encode($parents);
        }
    }
}
