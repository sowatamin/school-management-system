<?php

namespace App\Http\Controllers;

use App\Utilities\Overrider;
use App\Mail\UserCreate;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Teacher;
use App\User;
use App\Picklist;
use Validator;
use Hash;
use Image;
use File;
use App\Certificate;


class TeacherController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::select('*', 'teachers.id AS id')
            ->join('users', 'users.id', '=', 'teachers.user_id')
            ->where('users.branch_id', get_branch_option('branch'))
            ->orderBy('teachers.id', 'DESC')
            ->get();
        return view('backend.teachers.teacher-list', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.teachers.teacher-add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function excel_import(Request $request)
    {
        if (!$request->ajax()) {
            return view('backend.teachers.excel_import');
        } else {
            return view('backend.teachers.modal.excel_import');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        @ini_set('max_execution_time', 0);
        @set_time_limit(0);

        Overrider::load("Settings");

        $this->validate($request, [
            'name' => 'required|string|max:191',
            'designation' => 'required|string|max:191',
            'birthday' => 'required',
            'gender' => 'required|string|max:191',
            'religion' => 'required|string|max:191',
            'phone' => 'required|numeric|unique:teachers',
            'address' => 'required',
            'joining_date' => 'required',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'image' => 'nullable|image|max:5120',
            'nid_number' => 'required|numeric|unique:teachers',
            'nid_copy' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf|max:1024',
            'teacher_certificate.*' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf|max:1024',
        ]);

        $ImageName = 'profile.png';
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $ImageName = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(400, 400)->save(base_path('public/uploads/images/teachers/') . $ImageName);
        }

        if ($request->hasFile('nid_copy')) {
            $image = $request->file('nid_copy');
            $nid_copy = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(600, 600)->save(base_path('public/uploads/images/nid/') . $nid_copy);
        }

        $branch = get_branch_option('branch');

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->user_type = 'Teacher';
        $user->phone = $request->phone;
        $user->image = 'teachers/' . $ImageName;
        $user->branch_id = $branch;
        $user->save();

        $teacher = new Teacher();
        $teacher->user_id = $user->id;
        $teacher->name = $request->name;
        $teacher->designation = $request->designation;
        $teacher->birthday = $request->birthday;
        $teacher->gender = $request->gender;
        $teacher->religion = $request->religion;
        $teacher->phone = $request->phone;
        $teacher->address = $request->address;
        $teacher->joining_date = $request->joining_date;
        $teacher->nid_number = $request->nid_number;
        $teacher->nid_copy = $nid_copy;
        $teacher->save();

        if ($request->hasFile('teacher_certificate')) {
            $img_name = $_FILES['teacher_certificate']['name'];
            $i = 0;
            foreach ($request->file("teacher_certificate") as $file) {
                //process each file
                $ext = pathinfo($img_name[$i]);
                $ext = $ext['extension'];
                if ($ext === 'jpeg' || $ext === 'png' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'PNG') {
                    $image = $request->file('teacher_certificate')[$i];
                    $img = Image::make($image->getRealPath());
                    $certificateName = $i . date('YmdHis') . "NS" . rand(5, 10) . '.' . $request->teacher_certificate[$i]->getClientOriginalExtension();
                    $img->resize(600, null, function ($constraint) {
                        $constraint->aspectRatio();

                    })->save(('public/uploads/images/teacher_certificate/') . $certificateName);

                    $certificate = new Certificate();
                    $certificate->user_id = $user->id;
                    $certificate->certificate = $certificateName;
                    $certificate->save();
                } else {
                    $certificateName = $i . date('YmdHis') . "NS" . rand(5, 10) . '.' . $request->teacher_certificate[$i]->getClientOriginalExtension();
                    $request->teacher_certificate[$i]->move(('public/uploads/images/teacher_certificate/'), $certificateName);

                    $certificate = new Certificate();
                    $certificate->user_id = $user->id;
                    $certificate->certificate = $certificateName;
                    $certificate->save();
                }
                $i++;
            }
        }

        Mail::to($user->email)->send(new UserCreate($user, $request->password));

        return redirect('teachers')->with('success', ('Information has been added'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function excel_store(Request $request)
    {
        $request->validate([
            'excel_file' => 'required|mimes:xlsx,xls'
        ]);

        $path = $request->file('excel_file')->getRealPath();
        $teachers = (new FastExcel)->import($path, function ($line) {
            if (($line['email'] != '') AND (!User::where('email', $line['email'])->exists())) {

                if (!Picklist::where('value', $line['designation'])->exists()) {
                    Picklist::insert(['value' => $line['designation']]);
                }
                //user data
                $user = new User();
                $user->name = $line['name'];
                $user->email = $line['email'];
                $user->password = Hash::make($line['password']);
                $user->user_type = 'Teacher';
                $user->phone = $line['phone'];
                $user->image = 'teachers/profile.png';
                $user->save();

                //teacher data
                $teacher = new Teacher();
                $teacher->user_id = $user->id;
                $teacher->name = $line['name'];
                $teacher->designation = $line['designation'];
                $teacher->birthday = $line['birthday'];
                $teacher->gender = $line['gender'];
                $teacher->religion = $line['religion'];
                $teacher->phone = $line['phone'];
                $teacher->address = $line['address'];
                $teacher->joining_date = $line['joining_date'];
                $teacher->save();
            }
        });
        return redirect('teachers')->with('success', ('Information has been import'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $teacher = Teacher::select('*', 'teachers.id AS id')
            ->join('users', 'users.id', '=', 'teachers.user_id')
            ->where('teachers.id', $id)
            ->first();

        $certificates = User::leftJoin('teachers', 'users.id', 'teachers.user_id')
            ->leftJoin('certificate', 'users.id', 'certificate.user_id')
            ->where('teachers.id', $id)
            ->select('certificate.*')
            ->get();

        return view('backend.teachers.teacher-view', compact('teacher', 'certificates'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::select('*', 'teachers.id AS id')
            ->join('users', 'users.id', '=', 'teachers.user_id')
            ->where('teachers.id', $id)
            ->first();
        return view('backend.teachers.teacher-edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        @ini_set('max_execution_time', 0);
        @set_time_limit(0);

        Overrider::load("Settings");

        $teacher = Teacher::find($id);
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'designation' => 'required|string|max:191',
            'birthday' => 'required',
            'gender' => 'required|string|max:191',
            'religion' => 'required|string|max:191',
            'phone' => 'required|numeric|unique:teachers,phone,' . $teacher->id . '',
            'address' => 'required',
            'joining_date' => 'required',
            'email' => [
                'required',
                Rule::unique('users')->ignore($teacher->user_id),
            ],
            'password' => 'nullable|min:6|confirmed',
            'image' => 'nullable|image|max:5120',
        ]);

        $branch = get_branch_option('branch');

        $teacher->name = $request->name;
        $teacher->designation = $request->designation;
        $teacher->birthday = $request->birthday;
        $teacher->gender = $request->gender;
        $teacher->religion = $request->religion;
        $teacher->phone = $request->phone;
        $teacher->address = $request->address;
        $teacher->joining_date = $request->joining_date;
        $teacher->save();

        $user = User::find($teacher->user_id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $ImageName = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(400, 400)->save(base_path('public/uploads/images/teachers/') . $ImageName);
            $user->image = 'teachers/' . $ImageName;
        }
        $user->branch_id = $branch;

        $user->save();

        if (!empty($request->password)) {
            Mail::to($user->email)->send(new UserCreate($user, $request->password));
        }

        return redirect('teachers')->with('success', ('Information has been updated'));
    }

    // Teacher deactivate
    public function deactivate($id)
    {
        $teacher = Teacher::find($id);

        $user = User::find($teacher->user_id);
        $user->status = 'Deactivate';
        $user->save();

        return redirect()->back()->with('success', ('Information has been updated'));
    }

    // Teacher Active
    public function active($id)
    {
        $teacher = Teacher::find($id);

        $user = User::find($teacher->user_id);
        $user->status = 'Active';
        $user->save();

        return redirect()->back()->with('success', ('Information has been updated'));
    }

    public function certificate_create($id)
    {
        $certificate = Teacher::join('users', 'users.id', '=', 'teachers.user_id')
            ->where('teachers.id', $id)
            ->select('teachers.*')
            ->first();

        return view('backend.teachers.teacher_certificate', compact('certificate'));
    }

    public function certificate_store(Request $request)
    {
        $teacher_id = $request->teacher_id;
        $user_id = $request->user_id;

        @ini_set('max_execution_time', 0);
        @set_time_limit(0);

        Overrider::load("Settings");

        $this->validate($request, [
            'teacher_certificate.*' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf|max:1024'
        ]);


        if (($request->hasFile('teacher_certificate'))) {
            $img_name = $_FILES['teacher_certificate']['name'];
            $i = 0;
            foreach ($request->file("teacher_certificate") as $file) {
                $ext = pathinfo($img_name[$i]);
                $ext = $ext['extension'];

                if ($ext === 'jpeg' || $ext === 'png' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'PNG') {
                    $image = $request->file('teacher_certificate')[$i];
                    $img = Image::make($image->getRealPath());

                    $certificateName = $i . date('YmdHis') . "NS" . rand(5, 10) . '.' . $request->teacher_certificate[$i]->getClientOriginalExtension();
                    $img->resize(600, null, function ($constraint) {
                        $constraint->aspectRatio();

                    })->save(('public/uploads/images/teacher_certificate/') . $certificateName);

                    $certificate = new Certificate();
                    $certificate->user_id = $user_id;
                    $certificate->certificate = $certificateName;
                    $certificate->save();
                } else {
                    $certificateName = $i . date('YmdHis') . "NS" . rand(5, 10) . '.' . $request->teacher_certificate[$i]->getClientOriginalExtension();
                    $request->teacher_certificate[$i]->move(('public/uploads/images/teacher_certificate'), $certificateName);

                    $certificate = new Certificate();
                    $certificate->user_id = $user_id;
                    $certificate->certificate = $certificateName;
                    $certificate->save();
                }
                $i++;
            }
        }

        return redirect('teachers/' . $teacher_id)->with('success', ('Information has been added'));
    }

    public function certificate_view(Request $request, $id)
    {
        $certificate = Certificate::find($id);
        return view('backend.teachers.teacher_certificate_update', compact('certificate'));

    }

    public function certificate_update(Request $request, $id)
    {
        $certificate = Certificate::find($id);

        @ini_set('max_execution_time', 0);
        @set_time_limit(0);

        Overrider::load("Settings");

        $this->validate($request, [
            'teacher_certificate' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf|max:1024',
        ]);

        $teacher_certificate_file = public_path("uploads/images/teacher_certificate/{$certificate->certificate}");
        if (File::exists($teacher_certificate_file)) {
            unlink($teacher_certificate_file);
        }

        $img_name = $_FILES['teacher_certificate']['name'];
        $ext = pathinfo($img_name);
        $ext = $ext['extension'];
        if ($ext === 'jpeg' || $ext === 'png' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'PNG') {
            $image = $request->file('teacher_certificate');
            $img = Image::make($image->getRealPath());
            $teacher_certificate = date('YmdHis') . "NS" . rand(5, 10) . '.' . $request->teacher_certificate->getClientOriginalExtension();
            $img->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();

            })->save(('public/uploads/images/teacher_certificate/') . $teacher_certificate);
            $certificate->certificate = $teacher_certificate;
            $certificate->save();
        } else {
            $teacher_certificate = date('YmdHis') . "NS" . rand(5, 10) . '.' . $request->teacher_certificate->getClientOriginalExtension();
            $request->teacher_certificate->move(('public/uploads/images/teacher_certificate/'), $teacher_certificate);
            $certificate->certificate = $teacher_certificate;
            $certificate->save();
        }
        $teacher_id = Teacher::where('user_id', $certificate->user_id)->first()->id;

        return redirect('teachers/' . $teacher_id)->with('success', ('Information has been added'));

    }

    public function nid_create($id)
    {
        $nid = Teacher::where('teachers.id', $id)
            ->select('teachers.*')
            ->first();

        return view('backend.teachers.teacher_nid', compact('nid'));
    }

    public function nid_store(Request $request)
    {
        $teacher_id = $request->teacher_id;

        @ini_set('max_execution_time', 0);
        @set_time_limit(0);

        Overrider::load("Settings");

        $teacher = Teacher::find($teacher_id);

        $this->validate($request, [
            'nid_number' => 'required|numeric|unique:teachers,nid_number,' . $teacher->id . '',
            'nid_copy' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf|max:1024',
        ]);


        $user_nid_file = public_path("uploads/images/nid/{$teacher->nid_copy}");
        if (File::exists($user_nid_file)) {
            unlink($user_nid_file);
        }

        $img_name = $_FILES['nid_copy']['name'];
        $ext = pathinfo($img_name);
        $ext = $ext['extension'];
        if ($ext === 'jpeg' || $ext === 'png' || $ext === 'jpg' || $ext === 'JPG' || $ext === 'JPEG' || $ext === 'PNG') {
            $image = $request->file('nid_copy');
            $img = Image::make($image->getRealPath());
            $nid_copy = date('YmdHis') . "NS" . rand(5, 10) . '.' . $request->nid_copy->getClientOriginalExtension();
            $img->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();

            })->save(('public/uploads/images/nid/') . $nid_copy);
            $teacher->nid_number = $request->nid_number;
            $teacher->nid_copy = $nid_copy;
            $teacher->save();
        } else {
            $nid_copy = date('YmdHis') . "NS" . rand(5, 10) . '.' . $request->nid_copy->getClientOriginalExtension();
            $request->nid_copy->move(('public/uploads/images/nid/'), $nid_copy);
            $teacher->nid_number = $request->nid_number;
            $teacher->nid_copy = $nid_copy;
            $teacher->save();
        }

        return redirect('teachers/' . $teacher_id)->with('success', ('Information has been added'));
    }

    /*
     * add remark in certificate
     */
    public function certificate_remark(Request $request, $certificate_id)
    {

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'certificate_remark.*' => 'required'
            ]);

             $certificate_remark = Certificate::where('id', $certificate_id)->first();
             $certificate_remark->certificate_remark = $request->certificate_remark;
             $certificate_remark->save();

            return redirect()->back()->with('success', ('Information has been added'));
        }else{
            $certificate_remarks = Certificate::find($certificate_id);
            if (!$request->ajax()) {
                return view('backend.teachers.teacher_certificate_remark', compact('certificate_remarks', 'certificate_id'));
            } else {
                return view('backend.teachers.teacher_certificate_remark', compact('certificate_remarks', 'certificate_id'));
            }
        }

    }

}
