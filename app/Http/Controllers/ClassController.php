<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassModel;
use App\Section;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = ClassModel::orderBy('id', 'DESC')
            ->where('branch_id', get_branch_option('branch'))
            ->where('academic_id', get_option("academic_year"))
            ->get();
        return view('backend.classes.class-add',compact('classes'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'class_name' => 'required|string|max:191',
            'branches' => 'required',
        ]);

        foreach ($request->branches as $branch){
            $class = new ClassModel();
            $class->class_name = $request->class_name;
            $class->branch_id = $request->branch;
            $class->branch_id = $branch;
            $class->academic_id = get_option('academic_year');
            $class->save();
        }

        return redirect('class')->with('success', ('Information has been added'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'class'=>ClassModel::find($id),
            'classes'=>ClassModel::orderBy('id', 'DESC')
                ->where('branch_id', get_branch_option('branch'))
                ->where('academic_id', get_option('academic_year'))
                ->get(),
        ];
        return view('backend.classes.class-edit' ,$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'class_name' => 'required|string|max:191',
        ]);

        $class = ClassModel::find($id);

        $class->class_name = $request->class_name;
        $class->branch_id = $request->branch;
        $class->academic_id = get_option('academic_year');

        $class->save();

        return redirect('class')->with('success', ('Information has been updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Section::where('class_id', $id)->first();
        if (!empty($section)) {
            return redirect('class')->with('error', ('Information can not be delete because this class belong ' .$section->section_name.' Section.'));
        } else {
            $class = ClassModel::find($id);
            $class->delete();
            return redirect('class')->with('success', ('Information has been deleted'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_class(Request $request)
    {
        $results = ClassModel::where('classes.branch_id', $request->branch_id)
            ->where('classes.academic_id', get_option('academic_year'))
            ->select('classes.*')
            ->get();
        $classes= '';
        $classes .= '<option value="">' . ('Select One') . '</option>';
        foreach ($results as $data) {
            $classes .= '<option value="' . $data->id . '">' . $data->class_name . '</option>';
        }
        return $classes;
    }
}
