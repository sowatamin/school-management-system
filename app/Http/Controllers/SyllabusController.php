<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Syllabus;
use Carbon\Carbon;

class SyllabusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $class = "";
        $syllabus = array();
        return view('backend.syllabus.syllabus-list', compact('syllabus','class'));
    }

    public function class($class = '')
    {
        $class = $class;
        $syllabus = Syllabus::select('*', 'syllabus.id AS id')
            ->join('classes', 'classes.id', '=', 'syllabus.class_id')
            ->where('syllabus.session_id', get_option('academic_year'))
            ->where('syllabus.branch_id', get_branch_option('branch'))
            ->where('syllabus.class_id', $class)
            ->orderBy('syllabus.id', 'DESC')
            ->get();
        return view('backend.syllabus.syllabus-list', compact('syllabus','class'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.syllabus.syllabus-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:191',
            'description' => 'nullable|string',
            'class_id' => 'required',
            'file' => 'required|mimes:doc,pdf,docx,zip',
            'branch' => 'required',
        ]);

        if ($request->branch == get_branch_option('branch')) {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $file_name = str_slug($request->title) . '-' . Carbon::now()->year . '.' . $file->getClientOriginalExtension();
                $file->move(base_path('public/uploads/files/syllabus/'), $file_name);
                //$syllabus->file = $file_name;
            }

            $syllabus = New Syllabus();
            $syllabus->session_id = get_option("academic_year");
            $syllabus->title = $request->title;
            $syllabus->description = $request->description;
            $syllabus->class_id = $request->class_id;
            $syllabus->branch_id = $request->branch;
            $syllabus->file = $file_name;

            $syllabus->save();

            return redirect('syllabus')->with('success', ('Information has been added'));
        }

        return redirect('syllabus')->with('error', ('Please select valid branch'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $syllabus = Syllabus::select('*', 'syllabus.id AS id')
            ->join('classes', 'classes.id', '=', 'syllabus.class_id')
            ->where('syllabus.id', $id)
            ->where("syllabus.session_id", get_option('academic_year'))
            ->first();
        return view('backend.syllabus.syllabus-view', compact('syllabus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $syllabus = Syllabus::leftJoin('branches', 'syllabus.branch_id', 'branches.id')
            ->where("syllabus.id", $id)
            ->where("session_id", get_option('academic_year'))
            ->select('syllabus.*', 'branches.branch_name')
            ->first();
        return view('backend.syllabus.syllabus-edit', compact('syllabus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|string|max:191',
            'description' => 'nullable|string',
            'class_id' => 'required',
            'file' => 'nullable|mimes:doc,pdf,docx,zip',
            'branch' => 'required',
        ]);

        if ($request->branch == get_branch_option('branch')) {
            $syllabus = Syllabus::find($id);
            $syllabus->session_id = get_option("academic_year");
            $syllabus->title = $request->title;
            $syllabus->description = $request->description;
            $syllabus->class_id = $request->class_id;
            $syllabus->branch_id = $request->branch;

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $file_name = str_slug($request->title) . '-' . Carbon::now()->year . '.' . $file->getClientOriginalExtension();
                $file->move(base_path('public/uploads/files/syllabus/'), $file_name);
                $syllabus->file = $file_name;
            }
            $syllabus->save();

            return redirect('syllabus')->with('success', ('Information has been updated'));
        }
        return redirect('syllabus')->with('error', ('Please select valid branch'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $syllabus = Syllabus::find($id);
        $syllabus->delete();

        return redirect('syllabus')->with('success', ('Information has been deleted'));
    }
}
