<?php

namespace App\Http\Controllers\Branch_Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use Validator;
use Illuminate\Validation\Rule;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::where('branch_id', get_admin_branch_id())
            ->where('academic_id', get_session_year_option())
            ->get()
            ->sortByDesc("id");
        return view('backend.branch_admin.event.list', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->ajax()) {
            return view('backend.branch_admin.event.create');
        } else {
            return view('backend.branch_admin.event.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required',
            'end_date' => 'required',
            'name' => 'required',
            'details' => 'required',
            'location' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return redirect('branch_admin_events/create')
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $event = new Event();
        $event->start_date = $request->input('start_date');
        $event->end_date = $request->input('end_date');
        $event->name = $request->input('name');
        $event->details = $request->input('details');
        $event->location = $request->input('location');
        $event->branch_id = get_admin_branch_id();
        $event->academic_id = get_session_year_option();
        $event->save();

        if (!$request->ajax()) {
            return redirect('branch_admin_events/create')->with('success', ('Information has been added successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'store', 'message' => ('Information has been added successfully'), 'data' => $event]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $event = Event::find($id);
        if (!$request->ajax()) {
            return view('backend.branch_admin.event.view', compact('event', 'id'));
        } else {
            return view('backend.branch_admin.event.modal.view', compact('event', 'id'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $event = Event::find($id);
        if (!$request->ajax()) {
            return view('backend.branch_admin.event.edit', compact('event', 'id'));
        } else {
            return view('backend.branch_admin.event.modal.edit', compact('event', 'id'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required',
            'end_date' => 'required',
            'name' => 'required',
            'details' => 'required',
            'location' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return redirect()->route('branch_admin_events.edit', $id)
                    ->withErrors($validator)
                    ->withInput();
            }
        }


        $event = Event::find($id);
        $event->start_date = $request->input('start_date');
        $event->end_date = $request->input('end_date');
        $event->name = $request->input('name');
        $event->details = $request->input('details');
        $event->location = $request->input('location');
        $event->branch_id = get_admin_branch_id();
        $event->academic_id = get_session_year_option();

        $event->save();

        if (!$request->ajax()) {
            return redirect('branch_admin_events')->with('success', ('Information has been updated successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'update', 'message' => ('Information has been updated successfully'), 'data' => $event]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();
        return redirect('branch_admin_events')->with('success', ('Information has been deleted successfully'));
    }
}
