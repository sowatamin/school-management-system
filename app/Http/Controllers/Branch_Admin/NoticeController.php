<?php

namespace App\Http\Controllers\Branch_Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notice;
use App\UserNotice;
use Validator;
use Illuminate\Validation\Rule;

class NoticeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::leftJoin('user_notices', 'notices.id', 'user_notices.notice_id')
            ->where('user_notices.branch_id', get_admin_branch_id())
            ->where('user_notices.academic_id', get_session_year_option())
            ->select('notices.*')
            ->groupBy("notices.id")
            ->get()
            ->sortByDesc("notices.id");

        return view('backend.branch_admin.notice.list', compact('notices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->ajax()) {
            return view('backend.branch_admin.notice.create');
        } else {
            return view('backend.branch_admin.notice.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'heading' => 'required',
            'content' => 'required',
            'user_type' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return redirect('branch_admin_notices/create')
                    ->withErrors($validator)
                    ->withInput();
            }
        }


        $notice = new Notice();
        $notice->heading = $request->input('heading');
        $notice->content = $request->input('content');
        $notice->save();

        foreach ($request->input('user_type') as $user_type) {
            $userNotice = new UserNotice();
            $userNotice->notice_id = $notice->id;
            $userNotice->user_type = $user_type;
            $userNotice->branch_id = get_admin_branch_id();
            $userNotice->academic_id = get_session_year_option();
            $userNotice->save();

        }

        if (!$request->ajax()) {
            return redirect('branch_admin_notices/create')->with('success', ('Information has been added successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'store', 'message' => ('Information has been added successfully'), 'data' => $notice]);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $notice = Notice::find($id);
        if (!$request->ajax()) {
            return view('backend.branch_admin.notice.view', compact('notice', 'id'));
        } else {
            return view('backend.branch_admin.notice.modal.view', compact('notice', 'id'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $notice = Notice::leftJoin('user_notices', 'notices.id', 'user_notices.notice_id')
            ->where('notices.id', $id)
            ->select('notices.*','user_notices.branch_id')
            ->first();

        if (!$request->ajax()) {
            return view('backend.branch_admin.notice.edit', compact('notice', 'id'));
        } else {
            return view('backend.branch_admin.notice.modal.edit', compact('notice', 'id'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'heading' => 'required',
            'content' => 'required',
            'user_type' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return redirect()->route('branch_admin_notices.edit', $id)
                    ->withErrors($validator)
                    ->withInput();
            }
        }


        $notice = Notice::find($id);
        $notice->heading = $request->input('heading');
        $notice->content = $request->input('content');
        $notice->save();

        $userNotice = UserNotice::where("notice_id", $id);
        $userNotice->delete();

        foreach ($request->input('user_type') as $user_type) {
            $userNotice = new UserNotice();
            $userNotice->notice_id = $notice->id;
            $userNotice->user_type = $user_type;
            $userNotice->branch_id = get_admin_branch_id();
            $userNotice->academic_id = get_session_year_option();
            $userNotice->save();
        }

        if (!$request->ajax()) {
            return redirect('branch_admin_notices')->with('success', ('Information has been updated successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'update', 'message' => ('Information has been updated successfully'), 'data' => $notice]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notice = Notice::find($id);
        $notice->delete();

        $userNotice = UserNotice::where("notice_id", $id);
        $userNotice->delete();

        return redirect('branch_admin_notices')->with('success', ('Information has been deleted successfully'));
    }
}
