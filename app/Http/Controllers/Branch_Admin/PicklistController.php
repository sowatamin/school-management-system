<?php

namespace App\Http\Controllers\Branch_Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Picklist;
use Validator;
use Illuminate\Validation\Rule;


class PicklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = "";
        $picklists = Picklist::all()->sortByDesc("id");
        return view('backend.branch_admin.administration.picklist.list', compact('picklists', 'type'));
    }


    public function type($type)
    {
        $type = $type;
        $picklists = Picklist::where('type', $type)->orderBy('id', 'desc')->get();
        return view('backend.branch_admin.administration.picklist.list', compact('picklists', 'type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->ajax()) {
            return view('backend.branch_admin.administration.picklist.create');
        } else {
            return view('backend.branch_admin.administration.picklist.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'value' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return redirect('branch_admin_picklists/create')
                    ->withErrors($validator)
                    ->withInput();
            }
        }


        $picklist = new Picklist();
        $picklist->type = $request->input('type');
        $picklist->value = $request->input('value');

        $picklist->save();

        if (!$request->ajax()) {
            return redirect('branch_admin_picklists/create')->with('success', ('Information has been added successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'store', 'message' => ('Information has been added successfully'), 'data' => $picklist]);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $picklist = Picklist::find($id);
        if (!$request->ajax()) {
            return view('backend.branch_admin.administration.picklist.view', compact('picklist', 'id'));
        } else {
            return view('backend.branch_admin.administration.picklist.modal.view', compact('picklist', 'id'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $picklist = Picklist::find($id);
        if (!$request->ajax()) {
            return view('backend.branch_admin.administration.picklist.edit', compact('picklist', 'id'));
        } else {
            return view('backend.branch_admin.administration.picklist.modal.edit', compact('picklist', 'id'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'value' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return redirect()->route('branch_admin_picklists.edit', $id)
                    ->withErrors($validator)
                    ->withInput();
            }
        }


        $picklist = Picklist::find($id);
        $picklist->type = $request->input('type');
        $picklist->value = $request->input('value');

        $picklist->save();

        if (!$request->ajax()) {
            return redirect('branch_admin_picklists')->with('success', ('Information has been updated successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'update', 'message' => ('Information has been updated successfully'), 'data' => $picklist]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $picklist = Picklist::find($id);
        $picklist->delete();
        return redirect('branch_admin_picklists')->with('success', ('Information has been deleted successfully'));
    }
}
