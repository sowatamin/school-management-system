<?php

namespace App\Http\Controllers\Branch_Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\Mark;
use App\MarkDistribution;
use App\MarkDetails;
use App\StudentSession;
use App\User;
use App\ClassModel;
use App\Section;
use App\Subject;
use App\Exam;
use Validator;
use Illuminate\Validation\Rule;
use DB;

class MarkController extends Controller
{

    public function index($class = '')
    {
        $class = $class;
        $students = Student::join('users', 'users.id', '=', 'students.user_id')
            ->join('student_sessions', 'students.id', '=', 'student_sessions.student_id')
            ->join('classes', 'classes.id', '=', 'student_sessions.class_id')
            ->join('sections', 'sections.id', '=', 'student_sessions.section_id')
            ->select('users.*', 'student_sessions.roll', 'classes.class_name', 'sections.section_name', 'students.id as id', 'classes.id as class_id')
            ->where('student_sessions.session_id', get_session_year_option())
            ->where('student_sessions.class_id', $class)
            ->where('users.user_type', 'Student')
            ->where('users.branch_id', get_admin_branch_id())
            ->where('users.status', 'Active')
            ->orderBy('student_sessions.roll', 'ASC')
            ->get();
        return view('backend.branch_admin.marks.mark_list', compact('students', 'class'));
    }

    public function view_marks($student_id = '', $class_id = '')
    {
        $student = Student::join('users', 'users.id', '=', 'students.user_id')
            ->join('student_sessions', 'students.id', '=', 'student_sessions.student_id')
            ->join('classes', 'classes.id', '=', 'student_sessions.class_id')
            ->leftjoin('student_groups', 'student_groups.id', '=', 'students.group')
            ->join('sections', 'sections.id', '=', 'student_sessions.section_id')
            ->where('student_sessions.session_id', get_session_year_option())
            ->where('students.id', $student_id)->first();

        $exams = DB::select("SELECT marks.class_id,marks.section_id,marks.subject_id, mark_distributions.mark_distribution_type, mark_distributions.id AS mark_distributions_id
		    FROM marks,mark_distributions, mark_details WHERE marks.id=mark_details.mark_id AND mark_distributions.id=mark_details.mark_distributions_id
		    AND marks.student_id=:student_id AND marks.class_id=:class AND mark_distributions.session_id=:session GROUP BY mark_distributions.id",
            ["student_id" => $student_id, "class" => $class_id, "session" => get_session_year_option()]);

        $subjects = Subject::where("class_id", $class_id)->get();

        $existing_marks = DB::select("SELECT marks.subject_id, mark_distributions.id as mark_distributions_id,mark_details.* from mark_details,marks,mark_distributions WHERE mark_details.mark_id=marks.id 
		  AND mark_details.mark_distributions_id=mark_distributions.id AND marks.class_id=:class AND marks.student_id=:student AND mark_distributions.session_id=:session", ["class" => $class_id, "student" => $student_id, "session" => get_session_year_option()]);


        $mark_head = DB::select("SELECT distinct mark_details.mark_distributions_id from mark_distributions 
		JOIN mark_details JOIN marks ON mark_details.mark_distributions_id = mark_distributions.id 
		AND mark_details.mark_id=marks.id WHERE marks.class_id=:class AND marks.student_id=:student", ["class" => $class_id, "student" => $student_id]);

        $mark_details = [];

        foreach ($existing_marks as $key => $val) {
            if ($val->mark_id != "") {
                $mark_details[$val->subject_id][$val->mark_distributions_id][$val->mark_distributions_id] = $val;
            }
        }

        return view('backend.branch_admin.marks.view_mark', compact('exams', 'mark_head', 'mark_details', 'subjects', 'student'));

    }

    public function create(Request $request)
    {
        $class_id = $request->input('class_id');
        $section_id = $request->input('section_id');
        $subject_id = $request->input('subject_id');
        $marks = [];
        $mark_destributions = [];
        $new_fields = [];

        if ($class_id != "" && $section_id != "" && $subject_id != "") {
            $marks = Student::select('*', 'marks.id as mark_id')
                ->leftJoin('marks', function ($join) use ($class_id, $subject_id, $section_id) {
                    $join->on('marks.student_id', '=', 'students.id');
                    $join->where('marks.subject_id', '=', $subject_id);
                    $join->where('marks.class_id', '=', $class_id);
                    $join->where('marks.section_id', '=', $section_id);
                })
                ->join('student_sessions', 'student_sessions.student_id', '=', 'students.id')
                ->where('student_sessions.class_id', $class_id)
                ->where('student_sessions.section_id', $section_id)
                ->orderBy('student_sessions.roll', 'ASC')
                ->get();

            $mark_destributions = MarkDistribution::where("is_active", "=", "yes")
                ->select('mark_distributions.*', 'mark_distributions.id as mark_distributions_id')
                ->where('session_id',  get_session_year_option())
                ->get();

            $existing_marks = DB::select("SELECT mark_distributions.*, mark_details.*, mark_distributions.id AS mark_distributions_id from mark_distributions 
			LEFT JOIN mark_details ON mark_details.mark_distributions_id = mark_distributions.id AND mark_details.mark_id IN 
			(SELECT id FROM marks WHERE class_id=:class AND section_id=:section AND subject_id=:subject) 
			WHERE mark_distributions.is_active='yes'", ["class" => $class_id, "section" => $section_id, "subject" => $subject_id]);

            $mark_details = [];

            foreach ($existing_marks as $key => $val) {
                if ($val->mark_id != "") {
                    $mark_details[$val->mark_id][$val->mark_distributions_id] = $val;
                }
            }
        }
        return view('backend.branch_admin.marks.mark_register.create', compact('mark_details', 'mark_destributions', 'new_fields', 'marks', 'exam', 'class_id', 'section_id', 'subject_id'));

    }

    public function store(Request $request)
    {
        for ($i = 0; $i < count($request->student_id); $i++) {
            $temp = array();
            $temp['subject_id'] = (int)$request->subject_id[$i];
            $temp['student_id'] = (int)$request->student_id[$i];
            $temp['class_id'] = (int)$request->class_id[$i];
            $temp['section_id'] = (int)$request->section_id[$i];

            $marks = Mark::firstOrNew($temp);
            $marks->subject_id = $temp['subject_id'];
            $marks->student_id = $temp['student_id'];
            $marks->class_id = $temp['class_id'];
            $marks->section_id = $temp['section_id'];
            $marks->save();

            //Store Mark Details
            foreach ($request->marks as $key => $value) {

                $temp2 = array();
                $temp2['mark_id'] = $marks->id;
                $temp2['mark_distributions_id'] = $key;

                $marksDt = MarkDetails::firstOrNew($temp2);
                $marksDt->mark_id = $marks->id;
                $marksDt->mark_distributions_id = $key;
                $marksDt->mark_value = $value[$temp['student_id']];

                $mark_weight = MarkDistribution::where('id', $marksDt->mark_distributions_id)->first();
                $marksDt->mark_weight_value = ($marksDt->mark_value / $mark_weight->mark_for_the_exam) * $mark_weight->mark_weight;
                $marksDt->save();
            }
        }


        if (!$request->ajax()) {
            return redirect('/branch_admin_marks/create')->with('success', ('Saved Successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'store', 'message' => ('Saved Successfully')]);
        }
    }

    public function student_ranks($class = "")
    {

        $students = [];
        if ($class != "") {
            $class_id = $class;
            $session = get_session_year_option();
            $branch = get_admin_branch_id();

            $students = DB::select("SELECT marks.student_id,students.first_name,students.last_name,users.image,student_sessions.roll,classes.class_name,sections.section_name, 
	   IFNULL(SUM(mark_details.mark_value),0) as total_marks FROM marks,mark_details,exams,students,users,student_sessions,classes,sections
	   WHERE marks.id=mark_details.mark_id AND marks.exam_id=exams.id AND marks.student_id=students.id AND students.user_id=users.id AND 
	   students.id=student_sessions.student_id AND student_sessions.class_id=classes.id AND student_sessions.section_id=sections.id  
	   AND marks.class_id=:class AND exams.session_id=:session AND users.branch_id=$branch GROUP BY marks.student_id ORDER BY total_marks DESC", ["session" => $session, "session" => $session, "class" => $class_id]);

        }
        return view('backend.branch_admin.marks.rank_list', compact('class', 'students'));
    }
}
