<?php

namespace App\Http\Controllers\Branch_Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Account;
use Validator;
use Illuminate\Validation\Rule;
use Auth;

class AccountController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Account::where('branch_id', get_admin_branch_id())
            ->get()
            ->sortByDesc("id");
        return view('backend.branch_admin.accounting.bank_cash_account.list',compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( ! $request->ajax()){
            return view('backend.branch_admin.accounting.bank_cash_account.create');
        }else{
            return view('backend.branch_admin.accounting.bank_cash_account.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'account_name' => 'required|max:50',
            'opening_balance' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            if($request->ajax()){
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return redirect('branch_admin_accounts/create')
                    ->withErrors($validator)
                    ->withInput();
            }
        }


        $account= new Account();
        $account->account_name = $request->input('account_name');
        $account->opening_balance = $request->input('opening_balance');
        $account->note = $request->input('note');
        $account->create_user_id = Auth::user()->id;
        $account->branch_id = get_admin_branch_id();

        $account->save();

        if(! $request->ajax()){
            return redirect('branch_admin.accounts/create')->with('success', ('Information has been added successfully'));
        }else{
            return response()->json(['result'=>'success','action'=>'store','message'=>('Information has been added successfully'),'data'=>$account]);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $account = Account::find($id);
        if(! $request->ajax()){
            return view('backend.branch_admin.accounting.bank_cash_account.view',compact('account','id'));
        }else{
            return view('backend.branch_admin.accounting.bank_cash_account.modal.view',compact('account','id'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $account = Account::find($id);
        if(! $request->ajax()){
            return view('backend.branch_admin.accounting.bank_cash_account.edit',compact('account','id'));
        }else{
            return view('backend.branch_admin.accounting.bank_cash_account.modal.edit',compact('account','id'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'account_name' => 'required|max:50',
            'opening_balance' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            if($request->ajax()){
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return redirect()->route('branch_admin_accounts.edit', $id)
                    ->withErrors($validator)
                    ->withInput();
            }
        }



        $account = Account::find($id);
        $account->account_name = $request->input('account_name');
        $account->opening_balance = $request->input('opening_balance');
        $account->note = $request->input('note');
        $account->update_user_id = Auth::user()->id;
        $account->branch_id = get_admin_branch_id();

        $account->save();

        if(! $request->ajax()){
            return redirect('branch_admin_accounts')->with('success', ('Information has been updated successfully'));
        }else{
            return response()->json(['result'=>'success','action'=>'update', 'message'=>('Information has been updated successfully'),'data'=>$account]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Account::find($id);
        $account->delete();
        return redirect('branch_admin_accounts')->with('success',('Information has been deleted successfully'));
    }
}
