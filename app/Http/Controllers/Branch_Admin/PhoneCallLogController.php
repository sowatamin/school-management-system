<?php

namespace App\Http\Controllers\Branch_Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PhoneCallLog;

class PhoneCallLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('branch_admin_frontoffice/phone_call_logs');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->ajax()) {
            return view('backend.branch_admin.frontoffice.phone_call_logs.create');
        } else {
            return view('backend.branch_admin.frontoffice.phone_call_logs.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'phone' => 'required|string|max:30',
            'call_type' => 'required|string|max:30',
            'date' => 'required',
            'note' => 'nullable|string|max:250',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return redirect('branch_admin_phone_call_logs/create')
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $phone_call_log = new PhoneCallLog();
        $phone_call_log->session_id = get_session_year_option();
        $phone_call_log->name = $request->name;
        $phone_call_log->phone = $request->phone;
        $phone_call_log->call_type = $request->call_type;
        $phone_call_log->date = $request->date;
        $phone_call_log->start_time = $request->start_time;
        $phone_call_log->end_time = $request->end_time;
        $phone_call_log->note = $request->note;
        $phone_call_log->branch_id = get_admin_branch_id();
        $phone_call_log->save();

        if (!$request->ajax()) {
            return redirect('branch_admin_frontoffice/phone_call_logs')->with('success', ('Information has been added successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'store_with_redirect', 'message' => ('Information has been added successfully'), 'data' => $phone_call_log]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $phone_call_log = PhoneCallLog::find($id);
        if (!$request->ajax()) {
            return view('backend.branch_admin.frontoffice.phone_call_logs.modal.show', compact('phone_call_log'));
        } else {
            return view('backend.branch_admin.frontoffice.phone_call_logs.modal.show', compact('phone_call_log'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $phone_call_log = PhoneCallLog::find($id);
        if (!$request->ajax()) {
            return view('backend.branch_admin.frontoffice.phone_call_logs.edit', compact('phone_call_log'));
        } else {
            return view('backend.branch_admin.frontoffice.phone_call_logs.modal.edit', compact('phone_call_log'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'phone' => 'required|string|max:30',
            'call_type' => 'required|string|max:30',
            'date' => 'required',
            'note' => 'nullable|string|max:250',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return back()->withErrors($validator)->withInput();
            }
        }

        $phone_call_log = PhoneCallLog::find($id);
        $phone_call_log->name = $request->name;
        $phone_call_log->phone = $request->phone;
        $phone_call_log->call_type = $request->call_type;
        $phone_call_log->date = $request->date;
        $phone_call_log->start_time = $request->start_time;
        $phone_call_log->end_time = $request->end_time;
        $phone_call_log->note = $request->note;
        $phone_call_log->branch_id = get_admin_branch_id();
        $phone_call_log->save();

        if (!$request->ajax()) {
            return redirect('branch_admin_frontoffice/phone_call_logs')->with('success', ('Information has been updated successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'update_with_redirect', 'message' => ('Information has been updated successfully'), 'data' => $phone_call_log]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phone_call_log = PhoneCallLog::find($id);
        $phone_call_log->delete();
        return redirect('branch_admin_frontoffice/phone_call_logs')->with('success', ('Information has been deleted'));
    }
}
