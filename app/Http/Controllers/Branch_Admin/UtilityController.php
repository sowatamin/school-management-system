<?php

namespace App\Http\Controllers\Branch_Admin;

use App\BranchAdminSettings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use Carbon\Carbon;
use DB;
use App\Utilities\PHPMySQLBackup;

class UtilityController extends Controller
{
    public function __construct()
    {
        header('Cache-Control: no-cache');
        header('Pragma: no-cache');
    }

    public function change_session($session_id)
    {
        if ($session_id != "") {
            $data = array();
            $data['session_id'] = $session_id;
            $data['updated_at'] = Carbon::now();
            BranchAdminSettings::where('branch_id', get_admin_branch_id())->update($data);
        }
        return redirect($_SERVER['HTTP_REFERER'])->with('success', ('Session Changed Successfully.'));
    }
}
