<?php

namespace App\Http\Controllers;

use App\Mark;
use App\MarkDetails;
use App\Student;
use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Khill\Lavacharts\Lavacharts;
use App\Exam;
use DB;

class ChartController extends Controller
{
    public function markChart()
    {
        $lava = new Lavacharts;

        $markChart = $lava->DataTable();

        $student_name = Student::where('id', 1)->first();
        $student_name = $student_name->first_name .' '. $student_name->last_name;

        $subject_name = Subject::where('id', 1)->first();

        $marks = MarkDetails::leftJoin('marks', 'mark_details.mark_id', 'marks.id')
            ->leftJoin('exams', 'marks.exam_id', 'exams.id')
            ->leftJoin('subjects', 'marks.subject_id', 'subjects.id')
            ->leftJoin('students', 'marks.student_id', 'students.id')
            ->where('subjects.id', 1)
            ->where('students.id', 1)
            ->where('exams.session_id', get_option('academic_year'))
            ->get();

        $mark_data = array();

        foreach ($marks as $mark) {
            $avg_mark = Mark::leftJoin('mark_details', 'marks.id', 'mark_details.mark_id')
                ->leftJoin('exams', 'marks.exam_id', 'exams.id')
                ->where('marks.subject_id', 1)
                ->where('marks.exam_id', $mark->exam_id)
                ->where('exams.session_id', get_option('academic_year'))
                ->avg('mark_details.mark_value');

            $avg_mark = round($avg_mark, 2);

            $higest_mark = MarkDetails::leftJoin('marks', 'mark_details.mark_id', 'marks.id')
                ->leftJoin('exams', 'marks.exam_id', 'exams.id')
                ->leftJoin('subjects', 'marks.subject_id', 'subjects.id')
                ->where('subjects.id', 1)
                ->where('marks.exam_id', $mark->exam_id)
                ->where('exams.session_id', get_option('academic_year'))
                ->max('mark_details.mark_value');

            $higest_mark = round($higest_mark, 2);

            $mark_data[] = array(0 => $mark->name, 1 => $mark->mark_value, 2 => $avg_mark, 3 => $higest_mark);

        }

        $markChart = $markChart->addStringColumn('Exam')
            ->addNumberColumn('Student Mark')
            ->addNumberColumn('Average Mark')
            ->addNumberColumn('Highest Mark')
            ->addRows($mark_data);


        $lava->LineChart('LineChart', $markChart, [
            'title' => $subject_name->subject_name  . ' Mark Sheet of '. $student_name
        ]);

        return view("chart", ["lava" => $lava]);
    }
}
