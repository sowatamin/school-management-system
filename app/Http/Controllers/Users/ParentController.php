<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\StudentSession;
use App\Mark;
use App\MarkDetails;
use Khill\Lavacharts\Lavacharts;
use App\Student;
use App\Subject;

class ParentController extends Controller
{

    public function my_profile()
    {
        $parent = \App\ParentModel::join('users', 'users.id', '=', 'parents.user_id')
            ->leftJoin('students', 'parents.id', '=', 'students.parent_id')
            ->leftJoin('student_sessions', 'students.id', '=', 'student_sessions.student_id')
            ->leftJoin('classes', 'classes.id', '=', 'student_sessions.class_id')
            ->leftJoin('sections', 'sections.id', '=', 'student_sessions.section_id')
            ->where('parents.id', get_parent_id())->first();
        return view('backend.private.parent.profile', compact('parent'));
    }

    public function my_children($student_id)
    {
        $student = \App\Student::join('users', 'users.id', '=', 'students.user_id')
            ->join('student_sessions', 'students.id', '=', 'student_sessions.student_id')
            ->join('classes', 'classes.id', '=', 'student_sessions.class_id')
            ->join('sections', 'sections.id', '=', 'student_sessions.section_id')
            ->join('parents', 'parents.id', '=', 'students.parent_id')
            ->where('student_sessions.session_id', get_option('academic_year'))
            ->where('students.parent_id', get_parent_id())
            ->where('students.id', $student_id)
            ->first();
        return view('backend.private.parent.children_profile', compact('student', 'student_id'));
    }

    public function children_attendance(Request $request, $student_id)
    {
        if ($request->month != "") {
            $month = (explode("/", $request->month));
            $num_of_days = cal_days_in_month(CAL_GREGORIAN, $month[0], $month[1]);

            $query = DB::table('student_attendances')
                ->select('student_attendances.*')
                ->join('students', 'student_attendances.student_id', '=', 'students.id')
                ->where('student_id', $student_id)
                ->where('students.parent_id', get_parent_id())
                ->whereMonth('date', $month[0])
                ->whereYear('date', $month[1])
                ->orderBy('date', 'asc')
                ->get();

            $report_data = array();

            for ($i = 1; $i <= $num_of_days; $i++) {
                $date = new \DateTime($month[1] . "-" . $month[0] . "-" . $i);
                $date = $date->format('Y-m-d');
                $attendance_value = array("0" => "", "1" => ('Present'), "2" => ('Absent'), "3" => ('Late'), "4" => ('Holiday'));
                foreach ($query as $data) {
                    if ($date == $data->date) {
                        $report_data[$date] = $attendance_value[$data->attendance];
                    } else {
                        if (!isset($report_data[$date])) {
                            $report_data[$date] = $attendance_value[0];
                        }
                    }
                }
            }

            $month = $request->month;
            return view('backend.private.parent.children_attendance', compact('student_id', 'report_data', 'month'));
        } else {
            return view('backend.private.parent.children_attendance', compact('student_id'));
        }
    }

    public function progress_card($student_id)
    {
        $class_id = 0;
        $section_id = "";

        $student = \App\StudentSession::where("student_id", $student_id)
            ->where("session_id", get_option('academic_year'))
            ->first();

        $class_id = $student->class_id;
        $section_id = $student->section_id;


        $student = \App\Student::join('users', 'users.id', '=', 'students.user_id')
            ->join('student_sessions', 'students.id', '=', 'student_sessions.student_id')
            ->join('classes', 'classes.id', '=', 'student_sessions.class_id')
            ->leftjoin('student_groups', 'student_groups.id', '=', 'students.group')
            ->join('sections', 'sections.id', '=', 'student_sessions.section_id')
            ->where('student_sessions.session_id', get_option('academic_year'))
            ->where('students.parent_id', get_parent_id())
            ->where('students.id', $student_id)->first();


        $exams = DB::select("SELECT marks.class_id,marks.section_id,marks.subject_id, mark_distributions.mark_distribution_type, mark_distributions.id AS mark_distributions_id
		    FROM marks,mark_distributions, mark_details WHERE marks.id=mark_details.mark_id AND mark_distributions.id=mark_details.mark_distributions_id
		    AND marks.student_id=:student_id AND marks.class_id=:class AND mark_distributions.session_id=:session GROUP BY mark_distributions.id",
            ["student_id" => $student_id, "class" => $class_id, "session" => get_option('academic_year')]);

        $subjects = Subject::where("class_id", $class_id)->get();

        $existing_marks = DB::select("SELECT marks.subject_id, mark_distributions.id as mark_distributions_id,mark_details.* from mark_details,marks,mark_distributions WHERE mark_details.mark_id=marks.id 
		  AND mark_details.mark_distributions_id=mark_distributions.id AND marks.class_id=:class AND marks.student_id=:student AND mark_distributions.session_id=:session", ["class" => $class_id, "student" => $student_id, "session" => get_option('academic_year')]);


        $mark_head = DB::select("SELECT distinct mark_details.mark_distributions_id from mark_distributions 
		JOIN mark_details JOIN marks ON mark_details.mark_distributions_id = mark_distributions.id 
		AND mark_details.mark_id=marks.id WHERE marks.class_id=:class AND marks.student_id=:student", ["class" => $class_id, "student" => $student_id]);

        $mark_details = [];

        foreach ($existing_marks as $key => $val) {
            if ($val->mark_id != "") {
                $mark_details[$val->subject_id][$val->mark_distributions_id][$val->mark_distributions_id] = $val;
            }
        }

        return view('backend.private.parent.progress_card', compact('class_id', 'section_id', 'student', 'exams', 'mark_head', 'mark_details', 'subjects', 'student_id'));

    }

    public function payment_history($student_id)
    {
        $studentpayments = \App\StudentPayment::join('invoices', 'invoices.id', '=', 'student_payments.invoice_id')
            ->select('invoices.*', 'student_payments.*', 'student_payments.id as id')
            ->where('invoices.session_id', get_option('academic_year'))
            ->where('invoices.student_id', $student_id)
            ->orderBy('student_payments.id', 'DESC')
            ->get();
        return view('backend.private.parent.payment_history', compact('studentpayments', 'class', 'student_id'));
    }

    public function view_invoice(Request $request, $id)
    {
        $invoice = \App\Invoice::join('students', 'invoices.student_id', '=', 'students.id')
            ->join('student_sessions', 'students.id', '=', 'student_sessions.student_id')
            ->join('classes', 'classes.id', '=', 'student_sessions.class_id')
            ->join('sections', 'sections.id', '=', 'student_sessions.section_id')
            ->select('invoices.*', 'students.first_name', 'students.last_name', 'student_sessions.roll', 'classes.class_name', 'sections.section_name', 'invoices.id as id')
            ->where('student_sessions.session_id', get_option('academic_year'))
            ->where('invoices.session_id', get_option('academic_year'))
            ->where('invoices.id', $id)
            ->first();

        $invoiceItems = \App\InvoiceItem::join("fee_types", "invoice_items.fee_id", "=", "fee_types.id")
            ->where("invoice_id", $id)
            ->get();

        $transactions = \App\StudentPayment::where("invoice_id", $id)
            ->get();

        if (!$request->ajax()) {
            return view('backend.private.parent.invoice.view', compact('invoice', 'id', 'invoiceItems', 'transactions'));
        } else {
            return view('backend.private.parent.invoice.modal.view', compact('invoice', 'id', 'invoiceItems', 'transactions'));
        }

    }

    public function my_invoice($student_id)
    {
        $invoices = \App\Invoice::join('students', 'invoices.student_id', '=', 'students.id')
            ->join('student_sessions', 'students.id', '=', 'student_sessions.student_id')
            ->join('classes', 'classes.id', '=', 'student_sessions.class_id')
            ->join('sections', 'sections.id', '=', 'student_sessions.section_id')
            ->select('invoices.*', 'students.first_name', 'students.last_name', 'student_sessions.roll', 'classes.class_name', 'sections.section_name', 'invoices.id as id')
            ->where('student_sessions.session_id', get_option('academic_year'))
            ->where('invoices.session_id', get_option('academic_year'))
            ->where('invoices.student_id', $student_id)
            ->orderBy('invoices.id', 'DESC')
            ->get();

        return view('backend.private.parent.invoice.list', compact('invoices', 'student_id'));
    }

    public function mark_chart(Request $request, $student_id)
    {
        $lava = "";
        $subject_id = $request->subject_id;

        if (empty($subject_id)) {
            return view('backend.private.parent.mark_chart', compact('student_id', 'subject_id'))
                ->with('message', '');
        } else {
            $mark_empty = Mark::leftJoin('mark_details', 'marks.id', 'mark_details.mark_id')
                ->where('marks.subject_id', $subject_id)
                ->where('marks.student_id', $student_id)
                ->first();

            if (!empty($mark_empty)) {
                $lava = new Lavacharts;
                $markChart = $lava->DataTable();

                $subject_name = Subject::where('id', $subject_id)->first();

                $marks = MarkDetails::leftJoin('marks', 'mark_details.mark_id', 'marks.id')
                    ->leftJoin('mark_distributions', 'mark_details.mark_distributions_id', 'mark_distributions.id')
                    ->where('marks.subject_id', $subject_id)
                    ->where('marks.student_id', $student_id)
                    ->where('mark_distributions.session_id', get_option('academic_year'))
                    ->select('mark_details.*', 'marks.*', 'mark_distributions.*', 'mark_distributions.id as mark_distributions_id')
                    ->get();



                //return $marks;


                foreach ($marks as $mark) {
                    $avg_mark = Mark::leftJoin('mark_details', 'marks.id', 'mark_details.mark_id')
                        ->where('marks.subject_id', $subject_id)
                        ->where('mark_details.mark_distributions_id', $mark->mark_distributions_id)
                        ->avg('mark_details.mark_value');

                    $avg_mark = round($avg_mark, 2);

                    $higest_mark = MarkDetails::leftJoin('marks', 'mark_details.mark_id', 'marks.id')
                        ->where('marks.subject_id', $subject_id)
                        ->where('mark_details.mark_distributions_id', $mark->mark_distributions_id)
                        ->max('mark_details.mark_value');

                    $higest_mark = round($higest_mark, 2);

                    $mark_data[] = array(
                        0 => $mark->mark_distribution_type . ': ' . $mark->mark_for_the_exam,
                        1 => $mark->mark_value,
                        2 => (($mark->mark_value/$mark->mark_for_the_exam)*100),
                        3 => (($avg_mark/$mark->mark_for_the_exam)*100),
                        4 => (($higest_mark/$mark->mark_for_the_exam)*100));
                }

                $student_marks = Mark::leftJoin('mark_details', 'marks.id', 'mark_details.mark_id')
                    ->where('marks.subject_id', $subject_id)
                    ->where('marks.student_id', $student_id)
                    ->select('mark_details.*')
                    ->sum('mark_details.mark_weight_value');

                $students = Mark::where('marks.subject_id', $subject_id)->get();
                foreach ($students as $student) {
                    $all_marks[] = Mark::leftJoin('mark_details', 'marks.id', 'mark_details.mark_id')
                        ->where('marks.subject_id', $subject_id)
                        ->where('marks.student_id', $student->student_id)
                        ->select('mark_details.*')
                        ->sum('mark_details.mark_weight_value');
                }

                $students_max_mark = max($all_marks);
                $students_avg_mark = round(array_sum($all_marks )/ count($students), 2);
                $total = array(0 => 'Total: 100', 1 => $student_marks, 2 => $students_avg_mark, 3 => $students_max_mark);

                $markChart = $markChart->addStringColumn('Exam')
                    ->addNumberColumn('Mark Obtained')
                    ->addNumberColumn('Mark Obtained (%)')
                    ->addNumberColumn('Average Class Mark (%)')
                    ->addNumberColumn('Highest Class Mark (%)')
                    ->addRows($mark_data)
                    ->addRow($total);


                // Bar Chart
                $lava->BarChart('BarChart', $markChart, [
                    'title' => 'Mark Sheet of ' . $subject_name->subject_name,
                    'titlePosition' => 'out',
                    'legend' => ['position' => 'bottom'], 'height' => 500,
                ]);

                // Line Chart
                $lava->LineChart('LineChart', $markChart, [
                    'title' => 'Mark Sheet of ' . $subject_name->subject_name,
                    'titlePosition' => 'out',
                    'legend' => ['position' => 'bottom'], 'height' => 500,
                ]);

                /*
                 * for mark details
                 */
                 $student_subjects = Mark::leftJoin('mark_details', 'marks.id', 'mark_details.mark_id')
                    ->leftJoin('mark_distributions', 'mark_details.mark_distributions_id', 'mark_distributions.id')
                    ->where('marks.student_id', $student_id)
                    ->where('marks.subject_id', $subject_id)
                    ->get();

                /*
                 * for grade prediction
                 */
                $current_grade[] = array();
                $total_mark  = 0;
                $total_mark_weight = 0;
                for ($i = 0 ; $i<count($student_subjects); $i++)
                {
                    $mark = $student_subjects[$i]->mark_weight_value;
                    $current_grade[$i]['mark_weight'] = $student_subjects[$i]->mark_weight;
                    $total_mark +=  $mark;
                    $total_mark_weight += $current_grade[$i]['mark_weight'];
                    if ($i+1 < count($student_subjects) ){
                        if($student_subjects[$i+1]['mark_value'] === NULL)
                        {
                            $weighted_mark_converted = round(($total_mark/$total_mark_weight)*100);
                            $grade = get_grade($weighted_mark_converted);
                            $current_grade[$i]['current_mark'] = $grade;
                            $next_exam = $student_subjects[$i+1]['mark_distribution_type'];
                            $next_exam_mark = $student_subjects[$i+1]['mark_weight'];
                            $predicted_grades = get_grade_prediction($weighted_mark_converted, $next_exam);
                            break;
                        }
                    } else {
                        $predicted_grades = $next_exam = $grade = $next_exam_mark = 0;
                    }

                }

                //return $predicted_grades;
                return view('backend.private.parent.mark_chart', compact('student_id', 'subject_id', 'lava', 'student_subjects', 'predicted_grades', 'next_exam', 'grade', 'next_exam_mark'))->with('message', '');
            } else {
                return view('backend.private.parent.mark_chart', compact('student_id', 'subject_id', 'lava', 'student_subjects', 'predicted_grades', 'next_exam', 'grade', 'next_exam_mark'))
                    ->with('message', 'Result not publish yet!');
            }

        }

    }
}
