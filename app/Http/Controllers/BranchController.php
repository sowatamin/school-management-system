<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Setting;
use App\StudentSession;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::all()->sortByDesc("branch_id");
        return view('backend.administration.branch.list', compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->ajax()) {
            return view('backend.administration.branch.create');
        } else {
            return view('backend.administration.branch.modal.create');
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'branch_name' => 'required|max:100',
            'branch_address' => 'required|max:150',
            'branch_phone' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return redirect('branches/create')
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $branch = Setting::where('name', 'branch')->first();

        if (empty($branch)){
            $branch = new Branch();
            $branch->branch_name = $request->input('branch_name');
            $branch->branch_address = $request->input('branch_address');
            $branch->branch_phone = $request->input('branch_phone');
            $branch->branch_status = 'Active';
            $branch->save();

            $create_branch = new Setting();
            $create_branch->name = 'branch';
            $create_branch->value = $branch->id;
            $create_branch->save();

        }else{
            $branch = new Branch();
            $branch->branch_name = $request->input('branch_name');
            $branch->branch_address = $request->input('branch_address');
            $branch->branch_phone = $request->input('branch_phone');
            $branch->branch_status = 'Active';

            $branch->save();
        }


        if (!$request->ajax()) {
            return redirect('branches')->with('success', ('Information has been added successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'store', 'message' => ('Information has been added successfully'), 'data' => $branch]);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $branch = Branch::find($id);
        if (!$request->ajax()) {
            return view('backend.administration.branch.view', compact('branch ', 'id'));
        } else {
            return view('backend.administration.branch.modal.view', compact('branch ', 'id'));
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $branch = Branch::find($id);
        if (!$request->ajax()) {
            return view('backend.administration.branch.edit', compact('branch', 'id'));
        } else {
            return view('backend.administration.branch.modal.edit', compact('branch', 'id'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'branch_name' => 'required|max:100',
            'branch_address' => 'required|max:150',
            'branch_phone' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            } else {
                return redirect()->route('branches.edit', $id)
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $branch = Branch::find($id);
        $branch->branch_name = $request->input('branch_name');
        $branch->branch_address = $request->input('branch_address');
        $branch->branch_phone = $request->input('branch_phone');

        $branch->save();

        if (!$request->ajax()) {
            return redirect('branches')->with('success', ('Information has been updated successfully'));
        } else {
            return response()->json(['result' => 'success', 'action' => 'update', 'message' => ('Information has been updated successfully'), 'data' => $branch]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $current_branch = StudentSession::where('branch_id' ,$id)->first();
        if ($current_branch) {
            return redirect('branches')->with('error', ('Sorry, You cannot delete Academic Branch!'));
        } else {
            $branch = Branch::find($id);
            $branch->delete();
            return redirect('branches')->with('success', ('Information has been deleted successfully'));
        }
    }


}
