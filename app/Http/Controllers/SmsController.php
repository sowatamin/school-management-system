<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SmsLog;
use Validator;
use Auth;
use App\SmsService;

class SmsController extends Controller
{
    public function create()
    {
        return view('backend.sms.create');
    }

    public function logs()
    {
        $messages = SmsLog::leftJoin("users", "sms_logs.sender_id", "users.id")
            ->where('sms_logs.branch_id', get_branch_option('branch'))
            ->where('sms_logs.academic_id', get_option('academic_year'))
            ->select('sms_logs.*', 'users.name as sender')
            ->orderBy("sms_logs.id", "DESC")
            ->paginate(15);

        return view('backend.sms.logs', compact('messages'));
    }

    public function send(Request $request)
    {
        @ini_set('max_execution_time', 0);
        @set_time_limit(0);


        $this->validate($request, [
            'body' => 'required|max:300',
            'user_id' => 'required_without_all:student_id,parent_id',
            'student_id' => 'required_without_all:user_id,parent_id',
            'parent_id' => 'required_without_all:student_id,user_id',
        ]);

        $body = strip_tags($request->input("body"));

        try {
            if ($request->input('user_id') != "") {
                if ($request->input('user_id') == "all") {
                    foreach ($request->input('users') as $mobile_number) {
                        if (Auth::user()->phone == $mobile_number || $mobile_number == "") {
                            continue;
                        }

                        // user sms send function
                        $smsService = new SmsService();
                        $userMobileNo = '88' . $mobile_number;
                        $text = $body;
                        $smsService->sendSms($userMobileNo, $text);
                        // End user sms send function

                        $log = new SmsLog();
                        $log->receiver = $mobile_number;
                        $log->message = $body;
                        $log->sender_id = Auth::user()->id;
                        $log->academic_id = get_option('academic_year');
                        $log->branch_id = get_branch_option('branch');
                        $log->save();
                    }
                } else {
                    if (Auth::user()->phone != $request->input('user_id') || $request->input('user_id') != "") {


                        // user sms send function
                        $mobile_number =  $request->input('user_id');

                        $smsService = new SmsService();
                        $userMobileNo = '88' . $mobile_number;
                        $text = $body;
                        $smsService->sendSms($userMobileNo, $text);
                        // End user sms send function

                        $log = new SmsLog();
                        $log->receiver = $request->input('user_id');
                        $log->message = $body;
                        $log->sender_id = Auth::user()->id;
                        $log->academic_id = get_option('academic_year');
                        $log->branch_id = get_branch_option('branch');
                        $log->save();
                    } else {
                        return redirect('sms/compose')->with('error', ('Invalid mobile number Or Illegal Operation !'))->withInput();
                    }

                }
            }

            if ($request->input('student_id') != "") {
                if ($request->input('student_id') == "all") {
                    foreach ($request->input('students') as $mobile_number) {
                        if (Auth::user()->phone == $mobile_number || $mobile_number == "") {
                            continue;
                        }

                        // student sms send function
                        $smsService = new SmsService();
                        $userMobileNo = '88' . $mobile_number;
                        $text = $body;
                        $smsService->sendSms($userMobileNo, $text);
                        // End student sms send function

                        $log = new SmsLog();
                        $log->receiver = $mobile_number;
                        $log->message = $body;
                        $log->sender_id = Auth::user()->id;
                        $log->academic_id = get_option('academic_year');
                        $log->branch_id = get_branch_option('branch');
                        $log->save();
                    }
                } else {
                    if (Auth::user()->phone != $request->input('student_id') || $request->input('student_id') != "") {

                        //student sms send function
                        $mobile_number =  $request->input('student_id');

                        $smsService = new SmsService();
                        $userMobileNo = '88' . $mobile_number;
                        $text = $body;
                        $smsService->sendSms($userMobileNo, $text);
                        //End student sms send function

                        $log = new SmsLog();
                        $log->receiver = $request->input('student_id');
                        $log->message = $body;
                        $log->sender_id = Auth::user()->id;
                        $log->academic_id = get_option('academic_year');
                        $log->branch_id = get_branch_option('branch');
                        $log->save();
                    } else {
                        return redirect('message/compose')->with('error', ('Invalid mobile number Or Illegal Operation !'))->withInput();
                    }
                }
            }

            if ($request->input('parent_id') != "") {
                if ($request->input('parent_id') == "all") {
                    foreach ($request->input('parents') as $mobile_number) {
                        if (Auth::user()->phone == $mobile_number || $mobile_number == "") {
                            continue;
                        }

                        //parent sms send function
                        $smsService = new SmsService();
                        $userMobileNo = '88' . $mobile_number;
                        $text = $body;
                        $smsService->sendSms($userMobileNo, $text);
                        // End parent sms send function

                        $log = new SmsLog();
                        $log->receiver = $mobile_number;
                        $log->message = $body;
                        $log->sender_id = Auth::user()->id;
                        $log->academic_id = get_option('academic_year');
                        $log->branch_id = get_branch_option('branch');
                        $log->save();
                    }
                } else {
                    if (Auth::user()->phone != $request->input('parent_id') || $request->input('parent_id') != "") {

                        // parent sms send function
                         $mobile_number =  $request->input('parent_id');

                        $smsService = new SmsService();
                        $userMobileNo = '88' . $mobile_number;
                        $text = $body;
                        $smsService->sendSms($userMobileNo, $text);

                        // End parent sms send function

                        $log = new SmsLog();
                        $log->receiver = $request->input('parent_id');
                        $log->message = $body;
                        $log->sender_id = Auth::user()->id;
                        $log->academic_id = get_option('academic_year');
                        $log->branch_id = get_branch_option('branch');
                        $log->save();
                    } else {
                        return redirect('message/compose')->with('error', ('Invalid mobile number Or Illegal Operation !'))->withInput();
                    }
                }
            }

            return redirect()->back()->with('success', ('Message Send Successfully.'));

        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
