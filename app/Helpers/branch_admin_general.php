<?php

if (!function_exists('get_admin_branch')) {
    function get_admin_branch()
    {
        $admin_branch = \App\User::leftJoin('branches', 'users.branch_id', 'branches.id')
            ->where('users.id', Auth::user()->id)
            ->where('users.user_type', 'Branch_Admin')
            ->first()
            ->branch_name;

        return $admin_branch;
    }
}

if (!function_exists('get_admin_branch_id')) {
    function get_admin_branch_id()
    {
        $admin_branch_id = \App\User::leftJoin('branches', 'users.branch_id', 'branches.id')
            ->where('users.id', Auth::user()->id)
            ->where('users.user_type', 'Branch_Admin')
            ->first()
            ->id;

        return $admin_branch_id;
    }
}

if (!function_exists('get_branch_admin_students_list')) {
    function get_branch_admin_students_list($table, $parent_id)
    {
        $options = "";

        $query = \App\Student::where('parent_id', $parent_id)->get();

        if (count($query) > 0){
            foreach ($query as $d) {
                $url = action('Branch_Admin\StudentController@show', $d->id);
                $options .= "<li><a href='".$url."'>".$d->first_name.' '.$d->last_name."</a></li>";
            }
        }else{
            $options .= "<li><a href='#'>Student not found</a></li>";
        }

        echo $options;
    }
}

if (!function_exists('create_admin_branch_class_option')) {
    function create_admin_branch_class_option($table, $value, $display, $selected = "")
    {
        $options = "";
        $current_branch = get_admin_branch_id();
        $current_academic = get_session_year_option();

        if ($current_branch) {
            $query = DB::select("SELECT $value, $display FROM $table  WHERE branch_id = $current_branch AND academic_id = $current_academic");
            foreach ($query as $d) {
                if ($selected != "" && $selected == $d->$value) {
                    $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
                } else {
                    $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
                }
            }
        } else {

        }

        echo $options;
    }
}

if (!function_exists('create_admin_branch_parent_option')) {
    function create_admin_branch_parent_option($table, $value, $display, $selected = "", $where = NULL)
    {
        $options = "";
        $condition = "";
        if ($where != NULL) {
            $condition .= "WHERE ";
            foreach ($where as $key => $v) {
                $condition .= $key . "'" . $v . "' ";
            }
        }

        $query = \App\ParentModel::leftJoin('users', 'parents.user_id', 'users.id')
            ->where('users.branch_id', get_admin_branch_id())
            ->select($table . '.' . $display, $table . '.' . $value)
            ->get();
        foreach ($query as $d) {
            if ($selected != "" && $selected == $d->$value) {
                $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
            } else {
                $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
            }
        }

        echo $options;
    }
}

if (!function_exists('create_branch_admin_teacher_option')) {
    function create_branch_admin_teacher_option($table, $value, $display, $selected = "")
    {
        $options = "";
        $current_branch = get_admin_branch_id();

        if ($current_branch) {
            $query = \App\Teacher::leftJoin('users', $table . '.' . 'user_id', 'users.id')
                ->where('users.branch_id', $current_branch)
                ->where('status', 'Active')
                ->select('teachers.*')
                ->get();

            foreach ($query as $d) {
                if ($selected != "" && $selected == $d->$value) {
                    $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
                } else {
                    $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
                }
            }

        } else {

        }

        echo $options;
    }
}

if (!function_exists('create_branch_admin_class_section_option')) {
    function create_branch_admin_class_section_option($table, $value, $display, $selected = "", $class_id)
    {
        $options = "";

        $query = \App\Section::leftJoin('classes', $table . '.' . 'class_id', 'classes.id')
            ->where('classes.id', $class_id)
            ->select($table . '.' . '*')
            ->get();
        foreach ($query as $d) {
            if ($selected != "" && $selected == $d->$value) {
                $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
            } else {
                $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
            }
        }

        echo $options;
    }
}

if (!function_exists('create_branch_admin_account_option')) {
    function create_branch_admin_account_option($table, $value, $display, $selected = "")
    {
        $options = "";
        $current_branch = get_admin_branch_id();

        if ($current_branch) {
            $query = DB::select("SELECT $value, $display FROM $table  WHERE branch_id = $current_branch");
            foreach ($query as $d) {
                if ($selected != "" && $selected == $d->$value) {
                    $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
                } else {
                    $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
                }
            }
        } else {

        }

        echo $options;
    }
}

if (!function_exists('create_branch_admin_book_option')) {
    function create_branch_admin_book_option($table, $value, $display, $selected = "", $where = NULL)
    {
        $options = "";
        $branch = get_admin_branch_id();

        $query = DB::select("SELECT $value, $display FROM $table  where branch_id = $branch");
        foreach ($query as $d) {
            if ($selected != "" && $selected == $d->$value) {
                $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
            } else {
                $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
            }
        }

        echo $options;
    }
}

if (!function_exists('create_branch_admin_library_option')) {
    function create_branch_admin_library_option($table, $value, $display, $selected = "", $where = NULL)
    {
        $branch = get_admin_branch_id();
        $options = "";
        $condition = "";
        if ($where != NULL) {
            $condition .= "WHERE ";
            foreach ($where as $key => $v) {
                $condition .= $key . "'" . $v . "' ";
            }
        }

        $query = \App\LibraryMember::leftJoin('users', 'library_members.user_id', 'users.id')
            ->where('users.branch_id', $branch)
            ->select('library_members.*')
            ->get();

        //$query = DB::select("SELECT $value, $display FROM $table $condition WHERE ");
        foreach ($query as $d) {
            if ($selected != "" && $selected == $d->$value) {
                $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
            } else {
                $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
            }
        }

        echo $options;
    }
}

if (!function_exists('create_branch_admin_vehicle_option')) {
    function create_branch_admin_vehicle_option($table, $value, $display, $selected = "", $where = NULL)
    {
        $branch = get_admin_branch_id();
        $options = "";
        $condition = "";
        if ($where != NULL) {
            $condition .= "WHERE ";
            foreach ($where as $key => $v) {
                $condition .= $key . "'" . $v . "' ";
            }
        }

        $query = DB::select("SELECT $value, $display FROM $table $condition WHERE branch_id = $branch ");
        foreach ($query as $d) {
            if ($selected != "" && $selected == $d->$value) {
                $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
            } else {
                $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
            }
        }

        echo $options;
    }
}

if (!function_exists('create_branch_admin_road_option')) {
    function create_branch_admin_road_option($table, $value, $display, $selected = "", $where = NULL)
    {
        $options = "";
        $branch = get_admin_branch_id();
        $condition = "";
        if ($where != NULL) {
            $condition .= "WHERE ";
            foreach ($where as $key => $v) {
                $condition .= $key . "'" . $v . "' ";
            }
        }

        $query = DB::select("SELECT $value, $display FROM $table $condition WHERE branch_id = $branch");
        foreach ($query as $d) {
            if ($selected != "" && $selected == $d->$value) {
                $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
            } else {
                $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
            }
        }

        echo $options;
    }
}

if (!function_exists('get_branch_admin_exam_list')) {
    function get_branch_admin_exam_list($table, $value, $display, $selected = "", $where)
    {
        $options = "";

        if ($where != NUll) {
            $subject_id = $where[0];
            $branch = $where[1];
            $current_year = get_session_year_option();


            $query = \App\Exam::select($table . "." . $value, $table . "." . $display)
                ->leftJoin('exam_schedules', 'exams.id', 'exam_schedules.exam_id')
                ->where('exam_schedules.subject_id', $subject_id)
                ->where('exam_schedules.branch_id', $branch)
                ->where('exams.session_id', $current_year)
                ->orderBy($table . '.id', 'DESC')
                ->get();


            foreach ($query as $d) {
                if ($selected != "" && $selected == $d->$value) {
                    $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
                } else {
                    $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
                }
            }
        }

        echo $options;
    }
}

if (!function_exists('create_branch_admin_exam_option')) {
    function create_branch_admin_exam_option($table, $value, $display, $selected = "", $where = NULL)
    {
        $options = "";
        $current_session = get_session_year_option();

        $query = DB::select("SELECT $value, $display FROM $table  WHERE session_id = $current_session");
        foreach ($query as $d) {
            if ($selected != "" && $selected == $d->$value) {
                $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
            } else {
                $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
            }
        }

        echo $options;
    }
}

if (!function_exists('create_branch_admin_section_student_option')) {
    function create_branch_admin_section_student_option($table, $value, $display, $selected = "", $section_id)
    {
        $options = "";


        $query = \App\Student::join('student_sessions', $table . '.' . $value, '=', 'student_sessions.student_id')
            ->leftJoin('users', 'students.user_id', 'users.id')
            ->where('student_sessions.session_id', get_session_year_option())
            ->where('student_sessions.section_id', $section_id)
            ->where('users.branch_id', get_admin_branch_id())
            ->select('students.*', $table . '.' . $value, 'users.name', 'student_sessions.roll')
            ->get();

        foreach ($query as $d) {
            if ($selected != "" && $selected == $d->$value) {
                $options .= "<option value='" . $d->$value . "' selected='true'>Roll " . ucwords($d->roll) . ' - ' . ucwords($d->$display) . "</option>";
            } else {
                $options .= "<option value='" . $d->$value . "'>Roll " . ucwords($d->roll) . ' - ' . ucwords($d->$display) . "</option>";
            }
        }

        echo $options;
    }
}

if (!function_exists('get_session_year_option')) {
    function get_session_year_option()
    {
        $branch_admin_setting = \App\BranchAdminSettings::where('branch_id', get_admin_branch_id())
            ->first()
            ->session_id;

        return $branch_admin_setting;


    }
}

if (!function_exists('get_branch_session_table')) {
    function get_branch_session_table()
    {
        $query = App\AcademicBranchModel::leftJoin('academic_years', 'academic_branch.academic_id', 'academic_years.id')
            ->select('academic_years.*')
            ->where('branch_id', get_admin_branch_id())->get();
        return $query;
    }
}


if (!function_exists('branch_admin_user_count')) {
    function branch_admin_user_count($user_type)
    {
        $count = \App\User::where("user_type", $user_type)
            ->selectRaw("COUNT(id) as total")
            ->where('branch_id', get_admin_branch_id())
            ->first()->total;
        return $count;
    }
}

if (!function_exists('get_branch_admin_notices')) {
    function get_branch_admin_notices($user_type = "Website", $limit = 5)
    {

        $notices = \App\Notice::join("user_notices", "notices.id", "user_notices.notice_id")
            ->select('notices.*')
            ->where("user_notices.user_type", $user_type)
            ->where('user_notices.academic_id', get_session_year_option())
            ->orderBy("notices.id", "desc")
            ->limit($limit)
            ->get();
        return $notices;
    }
}

if (!function_exists('create_branch_admin_option')) {
    function create_branch_admin_option($table, $value, $display, $selected = "", $where = NULL)
    {
        $options = "";

        $query = App\User::leftJoin('library_members', 'users.id', 'library_members.user_id')
            ->where('users.branch_id', get_admin_branch_id())
            ->get();

        foreach ($query as $d) {
            if ($selected != "" && $selected == $d->$value) {
                $options .= "<option value='" . $d->$value . "' selected='true'>" . ucwords($d->$display) . "</option>";
            } else {
                $options .= "<option value='" . $d->$value . "'>" . ucwords($d->$display) . "</option>";
            }
        }

        echo $options;
    }
}
