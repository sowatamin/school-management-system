<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentAttendance extends Model
{
    protected $table = 'student_attendances';

    protected $fillable = array('student_id', 'class_id', 'section_id', 'date', 'attendance');
}
