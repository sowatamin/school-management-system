<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branches';

    protected $primaryKey = 'id';

    protected $fillable = [
        'branch_name', 'branch_address', 'branch_phone', 'branch_status'
    ];
}
