<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreate extends Mailable
{
    use Queueable, SerializesModels;

    public $content;
    public $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content,$password)
    {
        $this->content = $content;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('backend.email.user_create')
                    ->with(['content',$this->content, 'password',$this->password]);
    }
}
