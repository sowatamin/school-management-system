<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchAdminSettings extends Model
{
    protected $table = 'branch_admin_settings';
}
