<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicBranchModel extends Model
{
    protected $table = 'academic_branch';

    protected $primaryKey = 'id';

    protected $fillable = [
        'academic_id', 'branch_id'
    ];
}
