<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['install']], function () {
    //Auth Route
    Route::get('/login', function () {
        return redirect('login');
    });

    Auth::routes();


    Route::group(['middleware' => ['auth']], function () {

        /** Permission Route Group **/
        Route::group(['middleware' => ['Branch_Admin']], function () {

            Route::group(['namespace' => 'Branch_Admin'], function () {
                //FrontOffice Route
                Route::get('branch_admin_frontoffice/{page?}','FrontOfficeController@index')->name('branch_admin_frontoffice.main_screen');
                Route::resource('branch_admin_admission_enquiries','AdmissionEnquiryController');
                Route::resource('branch_admin_visitor_informations','VisitorInformationController');
                Route::resource('branch_admin_phone_call_logs','PhoneCallLogController');
                Route::resource('branch_admin_complains','ComplainController');

                //Student Route
                Route::get('branch_admin_students/excel_import', 'StudentController@excel_import')->name('branch_admin_students.excel_import');
                Route::post('branch_admin_students/excel_store', 'StudentController@excel_store')->name('branch_admin_students.excel_store');
                Route::get('branch_admin_students/id_card/{student_id}', 'StudentController@id_card')->name('branch_admin_students.view_id_card');
                Route::get('branch_admin_students/class/{class_id?}', 'StudentController@class')->name('branch_admin_students.index');
                Route::get('branch_admin_students/get_subjects/{class_id}', 'StudentController@get_subjects');
                Route::get('branch_admin_students/get_students/{class_id}/{section_id}', 'StudentController@get_students');
                Route::match(['get', 'post'], 'branch_admin_students/promote/{step?}', 'StudentController@promote')->name('branch_admin_students.promote');
                Route::resource('branch_admin_students', 'StudentController');
                Route::post('branch_admin_students/active/{id}', 'StudentController@active')->name('branch_admin_students.active');
                Route::post('branch_admin_students/deactivate/{id}', 'StudentController@deactivate')->name('branch_admin_students.deactivate');
                Route::post('branch_admin_students/certificate_remove/{id}','StudentController@certificate_remove');
                Route::get('branch_admin_students/certificate/{id}','StudentController@certificate_create');
                Route::post('branch_admin_students/certificate_store','StudentController@certificate_store');


                //Parents Route
                Route::get('branch_admin_parents/get_parents','ParentController@get_parents');
                Route::get('branch_admin_parents/get_parents_list/{class_id}/{section_id}', 'ParentController@get_parents_list');
                Route::resource('branch_admin_parents','ParentController');
                Route::get('branch_admin_parents/nid/{id}','ParentController@nid_create');
                Route::post('branch_admin_parents/nid_store','ParentController@nid_store');

                //Teacher Route
                Route::get('branch_admin_teachers/excel_import','TeacherController@excel_import')->name('branch_admin_teachers.excel_import');
                Route::post('branch_admin_teachers/excel_store','TeacherController@excel_store')->name('branch_admin_teachers.excel_import');
                Route::resource('branch_admin_teachers','TeacherController');
                Route::post('branch_admin_teachers/active/{id}','TeacherController@active')->name('branch_admin_teachers.active');
                Route::post('branch_admin_teachers/deactivate/{id}','TeacherController@deactivate')->name('branch_admin_teachers.deactivate');
                Route::get('branch_admin_teachers/certificate/{id}','TeacherController@certificate_create');
                Route::post('branch_admin_teachers/certificate_store','TeacherController@certificate_store');
                Route::get('branch_admin_teachers/nid/{id}','TeacherController@nid_create');
                Route::post('branch_admin_teachers/nid_store','TeacherController@nid_store');
                Route::get('branch_admin_teachers/certificate/view/{id}','TeacherController@certificate_view');
                Route::post('branch_admin_teachers/certificate_update/{id}','TeacherController@certificate_update');
                Route::match(['get', 'post'], 'branch_admin_teachers/certificate_remark/{id}', 'TeacherController@certificate_remark');

                //Class Controller
                Route::resource('branch_admin_class','ClassController');
                Route::post('branch_admin_classes/class', 'ClassController@get_class');


                //Section Route
                Route::post('branch_admin_sections/section', 'SectionController@get_section');
                Route::get('branch_admin_sections/class/{class_id}', 'SectionController@index')->name('branch_admin_sections.index');
                Route::resource('branch_admin_sections','SectionController');

                //Subject Route
                Route::get('branch_admin_subjects/class/{class_id}', 'SubjectController@index')->name('branch_admin_subjects.index');
                Route::post('branch_admin_subjects/subject', 'SubjectController@get_subject');
                Route::resource('branch_admin_subjects','SubjectController');

                //Assign Subject Route
                Route::post('branch_admin_assignsubjects/search', 'AssignSubjectController@search')->name('branch_admin_assignsubjects.index');
                Route::resource('branch_admin_assignsubjects','AssignSubjectController');

                //Assign Syllabus Route
                Route::resource('branch_admin_syllabus','SyllabusController');
                Route::get('branch_admin_syllabus/class/{class_id?}', 'SyllabusController@class');

                //Assign Assignment Route
                Route::resource('branch_admin_assignments','AssignmentController');
                Route::get('branch_admin_assignments/class/{class_id}', 'AssignmentController@class');

                //Class Routine
                Route::get('branch_admin_class_routines', 'ClassRoutineController@index')->name('branch_admin_class_routines.index');
                Route::get('branch_admin_class_routines/class/{class_id}', 'ClassRoutineController@class')->name('branch_admin_class_routines.index');
                Route::get('branch_admin_class_routines/manage/{class_id}/{section_id}', 'ClassRoutineController@manage')->name('branch_admin_class_routines.edit');
                Route::post('branch_admin_class_routines/store', 'ClassRoutineController@store')->name('branch_admin_class_routines.create');
                Route::get('branch_admin_class_routines/show/{class_id}/{section_id}', 'ClassRoutineController@show')->name('branch_admin_class_routines.index');

                //Attendance Controller
                Route::match(['get','post'],'branch_admin_student/attendance','AttendanceController@student_attendance')->name('branch_admin_student_attendance.create');
                Route::post('branch_admin_student/attendance/save', 'AttendanceController@student_attendance_save')->name('branch_admin_student_attendance.create');
                Route::match(['get','post'],'branch_admin_staff/attendance','AttendanceController@staff_attendance')->name('branch_admin_staff_attendance.create');
                Route::post('branch_admin_staff/attendance/save', 'AttendanceController@staff_attendance_save')->name('branch_admin_staff_attendance.create');

                //Bank & Cash Account Controller
                Route::resource('branch_admin_accounts','AccountController');

                //Chart Of Accounts Controller
                Route::resource('branch_admin_chart_of_accounts','ChartOfAccountController');

                //Payment Method Controller
                Route::resource('branch_admin_payment_methods','PaymentMethodController');

                //Payee/Payer Controller
                Route::resource('branch_admin_payee_payers','PayeePayerController');

                //Transaction Controller
                Route::get('branch_admin_transactions/income', 'TransactionController@income')->name('branch_admin_transactions.manage_income');
                Route::get('branch_admin_transactions/expense', 'TransactionController@expense')->name('branch_admin_transactions.manage_expense');
                Route::get('branch_admin_transactions/add_income', 'TransactionController@add_income')->name('branch_admin_transactions.add_income');
                Route::get('branch_admin_transactions/add_expense', 'TransactionController@add_expense')->name('branch_admin_transactions.add_expense');
                Route::resource('branch_admin_transactions','TransactionController');

                //Fee Type
                Route::resource('branch_admin_fee_types','FeeTypeController');

                //Invoice
                Route::get('branch_admin_invoices/class/{class_id}', 'InvoiceController@index')->name('branch_admin_invoices.index');
                Route::resource('branch_admin_invoices','InvoiceController');

                //Student Payments
                Route::get('branch_admin_student_payments/create/{invoice_id?}', 'StudentPaymentController@create')->name('branch_admin_student_payments.create');
                Route::get('branch_admin_student_payments/class/{class_id}', 'StudentPaymentController@index')->name('branch_admin_student_payments.index');
                Route::resource('branch_admin_student_payments','StudentPaymentController');

                //Library Controller
                Route::get('branch_admin_librarymembers/librarycard/{id}', 'LibraryMemberController@library_card')->name('branch_admin_librarymembers.view_library_card');
                Route::post('branch_admin_librarymembers/section', 'LibraryMemberController@get_section');
                Route::post('branch_admin_librarymembers/student', 'LibraryMemberController@get_student');
                Route::resource('branch_admin_librarymembers','LibraryMemberController');

                //Book Controller
                Route::resource('branch_admin_books','BookController');

                //Book Issue  Controller
                Route::match(['get','post'],'branch_admin_bookissues/list/{library_id?}','BookIssueController@index')->name('branch_admin_bookissues.index');
                Route::get('branch_admin_bookissues/return/{id}', 'BookIssueController@book_return')->name('branch_admin_bookissues.return');
                Route::resource('branch_admin_bookissues','BookIssueController');

                //BookCategory Controller
                Route::resource('branch_admin_bookcategories','BookCategoryController');

                //Transport Controller
                Route::resource('branch_admin_transportvehicles','TransportVehicleController');

                //Transport Controller
                Route::resource('branch_admin_transports','TransportController');

                //Transport Member Controller
                Route::post('branch_admin_transportmembers/section', 'TransportMemberController@get_section');
                Route::post('branch_admin_transportmembers/student', 'TransportMemberController@get_student');
                Route::post('branch_admin_transportmembers/transport_fee', 'TransportMemberController@get_transport_fee');
                Route::get('branch_admin_transportmembers/list/{type?}/{class?}', 'TransportMemberController@index')->name('branch_admin_transportmembers.index');
                Route::resource('branch_admin_transportmembers','TransportMemberController');

                // Exam Controller
                Route::match(['get', 'post'],'branch_admin_exams/schedule/{type?}', 'ExamController@exam_schedule')->name('branch_admin_exams.view_schedule');
                Route::match(['get', 'post'],'branch_admin_exams/attendance', 'ExamController@exam_attendance')->name('branch_admin_exams.store_exam_attendance');
                Route::post('branch_admin_exams/store_exam_attendance', 'ExamController@store_exam_attendance')->name('branch_admin_exams.store_exam_attendance');
                Route::post('branch_admin_exams/store_schedule', 'ExamController@store_exam_schedule')->name('branch_admin_exams.store_exam_schedule');
                Route::post('branch_admin_exams/get_exam', 'ExamController@get_exam');
                Route::post('branch_admin_exams/get_subject', 'ExamController@get_subject');
                Route::post('branch_admin_exams/get_teacher_subject', 'ExamController@get_teacher_subject');
                Route::resource('branch_admin_exams','ExamController');

                //Grade Controller
                Route::resource('branch_admin_grades','GradeController');

                //Mark Distribution Controller
                Route::resource('branch_admin_mark_distributions','MarkDistributionController');

                //Mark Register
                Route::match(['get', 'post'],'branch_admin_marks/rank/{class?}', 'MarkController@student_ranks')->name('branch_admin_marks.view_student_rank');
                Route::match(['get', 'post'],'branch_admin_marks/create', 'MarkController@create')->name('branch_admin_marks.create');
                Route::post('branch_admin_marks/store','MarkController@store')->name('branch_admin_marks.store');
                Route::match(['get', 'post'],'branch_admin_marks/{class?}', 'MarkController@index')->name('branch_admin_marks.index');
                Route::get('branch_admin_marks/view/{student_id}/{class_id}', 'MarkController@view_marks')->name('branch_admin_marks.show');

                //Message Controller
                Route::get('branch_admin_message/compose', 'MessageController@create');
                Route::get('branch_admin_message/outbox', 'MessageController@send_items');
                Route::get('branch_admin_message/inbox', 'MessageController@inbox_items');
                Route::get('branch_admin_message/outbox/{id}', 'MessageController@show_outbox');
                Route::get('branch_admin_message/inbox/{id}', 'MessageController@show_inbox');
                Route::post('branch_admin_message/send', 'MessageController@send');

                //Notice Controller
                Route::get('branch_admin_notices/{id}','NoticeController@show')->where('id', '[0-9]+');
                Route::resource('branch_admin_notices','NoticeController');

                //Event Controller
                Route::get('branch_admin_events/{id}','EventController@show')->where('id', '[0-9]+');
                Route::resource('branch_admin_events','EventController');

                //SMS Controller
                Route::get('branch_admin_sms/compose', 'SmsController@create')->name('branch_admin_sms.compose');
                Route::get('branch_admin_sms/logs', 'SmsController@logs')->name('branch_admin_sms.view_logs');
                Route::post('branch_admin_sms/send', 'SmsController@send')->name('branch_admin_sms.compose');

                //Email Controller
                Route::get('branch_admin_email/compose', 'EmailController@create')->name('branch_admin_email.compose');
                Route::get('branch_admin_email/logs', 'EmailController@logs')->name('branch_admin_email.view_logs');
                Route::post('branch_admin_email/send', 'EmailController@send')->name('branch_admin_email.compose');

                //Report Controller
                Route::match(['get', 'post'],'branch_admin_reports/student_attendance_report/{view?}', 'ReportController@student_attendance_report')->name('branch_admin_reports.student_attendance_report');
                Route::match(['get', 'post'],'branch_admin_reports/staff_attendance_report/{view?}', 'ReportController@staff_attendance_report')->name('branch_admin_reports.staff_attendance_report');
                Route::match(['get', 'post'],'branch_admin_reports/student_id_card/{view?}', 'ReportController@student_id_card')->name('branch_admin_reports.student_id_card');
                Route::match(['get', 'post'],'branch_admin_reports/exam_report/{view?}', 'ReportController@exam_report')->name('branch_admin_reports.exam_report');
                Route::match(['get', 'post'],'branch_admin_reports/progress_card/{view?}', 'ReportController@progress_card')->name('branch_admin_reports.progress_card');
                Route::match(['get', 'post'],'branch_admin_reports/class_routine/{view?}', 'ReportController@class_routine')->name('branch_admin_reports.class_routine');
                Route::match(['get', 'post'],'branch_admin_reports/exam_routine/{view?}', 'ReportController@exam_routine')->name('branch_admin_reports.exam_routine');
                Route::match(['get', 'post'],'branch_admin_reports/income_report/{view?}', 'ReportController@income_report')->name('branch_admin_reports.income_report');
                Route::match(['get', 'post'],'branch_admin_reports/expense_report/{view?}', 'ReportController@expense_report')->name('branch_admin_reports.expense_report');
                Route::get('branch_admin_reports/account_balance', 'ReportController@account_balance')->name('branch_admin_reports.account_balance');

                //User Controller
                Route::get('branch_admin_users/get_users/{user_type}', 'UserController@get_users');
                Route::resource('branch_admin_users','UserController');

                //PickList Controller
                Route::get('branch_admin_picklists/type/{type}', 'PicklistController@type')->name('branch_admin_picklists.index');
                Route::resource('branch_admin_picklists','PicklistController');

                //Utility Controller
                Route::get('branch_admin_administration/change_session/{session_id}', 'UtilityController@change_session')->name('branch_admin_general_settings.update');

            });

        });


    });

});


