<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <title>Laravel Chart</title>
</head>
<body>
<h4 class="text-center">Total Number of Visitors</h4>
<div class="row">
    <div class="col-md-12 mt-4">
        <div id="temps_div"></div>
        <?= $lava->render('LineChart', 'LineChart', 'temps_div') ?>
    </div>
</div>

<div>
</div>

</body>
</html>
