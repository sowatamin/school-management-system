@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Event</div>

                <div class="panel-body">
                    <form method="post" class="validate" autocomplete="off" action="{{url('events')}}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Start Date</label>
                                <input type="text" class="form-control datetimepicker" name="start_date"
                                       value="{{ old('start_date') }}" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">End Date</label>
                                <input type="text" class="form-control datetimepicker" name="end_date"
                                       value="{{ old('end_date') }}" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Details</label>
                                <textarea class="form-control summernote" name="details"
                                          required>{{ old('details') }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Location</label>
                                <input type="text" class="form-control" name="location" value="{{ old('location') }}"
                                       required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Branch</label>
                                <select name="branches[]" class="form-control branch_select2" multiple="multiple"
                                        required>
                                    {{ get_branch_list('branches','id','branch_name',old('branch')) }}
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="reset" class="btn btn-danger">Reset</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


