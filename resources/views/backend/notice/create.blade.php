@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading panel-title">Add Notice</div>

                <div class="panel-body">
                    <form method="post" class="validate" autocomplete="off" action="{{url('notices')}}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Heading</label>
                                <input type="text" class="form-control" name="heading" value="{{ old('heading') }}"
                                       required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Content</label>
                                <textarea class="form-control summernote" name="content"
                                          required>{{ old('content') }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">User Type</label>
                                <select class="form-control select2" name="user_type[]" multiple="multiple" required>
                                    <option value="Website">Website</option>
                                    <option value="Student">Student</option>
                                    <option value="Parent">Parent</option>
                                    <option value="Teacher">Teacher</option>
                                    <option value="Accountant">Accountant</option>
                                    <option value="Librarian">Librarian</option>
                                    <option value="Employee">Employee</option>
                                    <option value="Admin">Admin</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Branch</label>
                                <select name="branch" class="form-control branch_select2" required>
                                    <option value="">Select One</option>
                                    {{ get_branch_list('branches','id','branch_name',old('branch')) }}
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="reset" class="btn btn-danger">{{ ('Reset') }}</button>
                                <button type="submit" class="btn btn-primary">{{ ('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


