@extends('layouts.backend')
@section('content')
<div class="row">
    <form action="{{ url('sms/send') }}" class="validate" autocomplete="off" method="post" accept-charset="utf-8">
		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-title" >Compose SMS Text</div>
				</div>
				<div class="panel-body">
					@csrf
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">User Type</label>
							<select name="user_type" id="user_type" class="form-control select2" required>
								<option value="">Select One</option>
								<option value="Admin">Admin</option>
								<option value="Branch_Admin">Branch Admin</option>
								<option value="Student">Student</option>
								<option value="Parent">Parent</option>
								<option value="Teacher">Teacher</option>
								<option value="Accountant">Accountant</option>
								<option value="Librarian">Librarian</option>
								<option value="Employee">Employee</option>
							</select>
						</div>
					</div>

					<div class="col-sm-6 student-group">
					   <div class="form-group">
							<label class="control-label">Select Class</label>
							<select name="class_id" onchange="getSection();" class="form-control select2">
								<option value="">Select One</option>
								{{ create_option('classes','id','class_name',old('class_id')) }}
							</select>
						</div>
					</div>

					<div class="col-sm-6 student-group">
					   <div class="form-group">
							<label class="control-label">Select Section</label>
							<select name="section_id" onchange="get_students();" id="section_id" class="form-control select2">
								<option value="">Select One</option>
							</select>
						</div>
					</div>

					<div class="col-sm-12 student-group">
					   <div class="form-group">
							<label class="control-label">Select Student</label>
							<select name="student_id" id="student_id" onchange="get_all_students();" class="form-control select2">
								<option value="">Select One</option>
							</select>
						</div>
					</div>

					<div class="col-sm-6 parent-group">
						<div class="form-group">
							<label class="control-label">Select Class</label>
							<select name="parent_class_id" onchange="parentGetSection();" class="form-control select2">
								<option value="">Select One</option>
								{{ create_option('classes','id','class_name',old('class_id')) }}
							</select>
						</div>
					</div>

					<div class="col-sm-6 parent-group">
						<div class="form-group">
							<label class="control-label">Select Section</label>
							<select name="parent_section_id" onchange="get_parents();" id="parent_section_id" class="form-control select2">
								<option value="">Select One</option>
							</select>
						</div>
					</div>

					<div class="col-sm-12 parent-group">
						<div class="form-group">
							<label class="control-label">Select Parent</label>
							<select name="parent_id" id="parent_id" onchange="get_all_parents();" class="form-control select2">
								<option value="">Select One</option>
							</select>
						</div>
					</div>

					<div class="col-sm-12 general-group">
					   <div class="form-group">
							<label class="control-label">Select Receiver</label>
							<select name="user_id" id="user_id" onchange="get_all_users();" class="form-control select2">
								<option value="">Select One</option>
							</select>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Message (MAX 300)</label>
							<textarea class="form-control" name="body" required>{{ old('body') }}</textarea>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Send SMS</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">User List</div>
				<div class="panel-body" id="user_list">
				</div>
			</div>
		</div>
	</form>
</div>
@endsection

@section('js-script')
<script type="text/javascript">
    $(document).on('change','#user_type',function(){
		var user_type = $(this).val();

		if(user_type == "Student"){
			$(".student-group").fadeIn();
			$(".parent-group").css("display","none");
			$(".general-group").css("display","none");
			$("#student_id").prop("required",true);
			$("#parent_id").prop("required",false);
			$("#user_id").prop("required",false);
		}else if(user_type == "Parent"){
			$(".parent-group").fadeIn();
			$(".general-group").css("display","none");
			$(".student-group").css("display","none");
			$("#parent_id").prop("required",true);
			$("#student_id").prop("required",false);
			$("#user_id").prop("required",false);
		}else{
			$(".general-group").fadeIn();
			$(".student-group").css("display","none");
			$(".parent-group").css("display","none");
			$("#user_id").prop("required",true);
			$("#parent_id").prop("required",false);
			$("#student_id").prop("required",false);
			getUsers(user_type);
		}
	});

	function getUsers( type ) {
		$.ajax({
			url: "{{ url('users/get_users') }}/"+type,
			beforeSend: function(){
			    $("#preloader").css("display","block");
			},success: function(data){
				$("#preloader").css("display","none");
				var json =JSON.parse(data);
			    $('select[name=user_id]').html("");
				$('#user_list').html("");

				jQuery.each( json, function( i, val ) {
					$('select[name=user_id]').append("<option value='"+val['phone']+"'>"+val['name']+"</option>");
				});

				if( $('#user_id').has('option').length > 0 ) {
					$('select[name=user_id]').prepend("<option value='all'>All "+type+"</option>");
				}
			}
		});
	}

	function getSection() {
		if( $('select[name=class_id]').val() != "" ){
			var _token=$('input[name=_token]').val();
			var class_id=$('select[name=class_id]').val();
			$.ajax({
				type: "POST",
				url: "{{ url('sections/section') }}",
				data:{_token:_token,class_id:class_id},
				beforeSend: function(){
					$("#preloader").css("display","block");
				},success: function(data){
					$("#preloader").css("display","none");
					$('select[name=section_id]').html(data);
				}
			});
		}
	}

	function parentGetSection() {
		if( $('select[name=parent_class_id]').val() != "" ){
			var _token=$('input[name=_token]').val();
			var parent_class_id=$('select[name=parent_class_id]').val();
			$.ajax({
				type: "POST",
				url: "{{ url('sections/section') }}",
				data:{_token:_token,class_id:parent_class_id},
				beforeSend: function(){
					$("#preloader").css("display","block");
				},success: function(data){
					$("#preloader").css("display","none");
					$('select[name=parent_section_id]').html(data);
				}
			});
		}
	}

	function get_students(){
		if( $("#user_type").val() == "Student" && $('select[name=section_id]').val() !=""){
			var class_id = "/"+$('select[name=class_id]').val();
			var section_id = "/"+$('select[name=section_id]').val();
			var link = "{{ url('students/get_students') }}"+class_id+section_id;
			$.ajax({
				url: link,
				beforeSend: function(){
					$("#preloader").css("display","block");
				},success: function(data){
					$("#preloader").css("display","none");
					var json =JSON.parse(data);
					   $('select[name=student_id]').html("");
					   $('#user_list').html("");

					jQuery.each( json, function( i, val ) {
					   $('select[name=student_id]').append("<option value='"+val['phone']+"'>"+val['roll']+" - "+val['first_name']+" "+val['last_name']+"</option>");
					});

					if( $('#student_id').has('option').length > 0 ) {
						$('select[name=student_id]').prepend("<option value='all'>All Student</option>");
					}
				}
			});
		}
	}

	function get_parents(){
		if( $("#user_type").val() == "Parent" && $('select[name=parent_section_id]').val() !=""){
			var parent_class_id = "/"+$('select[name=parent_class_id]').val();
			var parent_section_id = "/"+$('select[name=parent_section_id]').val();
			var link = "{{ url('parents/get_parents_list') }}"+parent_class_id+parent_section_id;
			$.ajax({
				url: link,
				beforeSend: function(){
					$("#preloader").css("display","block");
				},success: function(data){
					$("#preloader").css("display","none");
					var json =JSON.parse(data);
					$('select[name=parent_id]').html("");
					$('#user_list').html("");

					jQuery.each( json, function( i, val ) {
						$('select[name=parent_id]').append("<option value='"+val['phone']+"'>"+val['parent_name']+" - "+val['phone']+"</option>");
					});

					if( $('#parent_id').has('option').length > 0 ) {
						$('select[name=parent_id]').prepend("<option value='all'>All Parent</option>");
					}
				}
			});
		}
	}

	function get_all_students(){
		if($("#student_id").val() == "all"){
			var class_id = "/"+$('select[name=class_id]').val();
			var section_id = "/"+$('select[name=section_id]').val();
			var link = "{{ url('students/get_students') }}"+class_id+section_id;
			$.ajax({
				url: link,
				beforeSend: function(){
					$("#preloader").css("display","block");
				},success: function(data){
					$("#preloader").css("display","none");
					var json =JSON.parse(data);
					$('#user_list').html("");

					jQuery.each( json, function( i, val ) {
					   $('#user_list')
					   .append('<div class="col-md-12">'+
									'<label class="c-container">'+
									   '<input type="checkbox" value="'+val['phone']+'" name="students[]" checked="true">'+val['roll']+" - "+val['first_name']+" "+val['last_name']+
									   '<span class="checkmark"></span>'+
									'</label>'+
								'</div>');
					});

				}
			});
		}else{
		  $('#user_list').html("");
		}
	}

	function get_all_parents(){
		if($("#parent_id").val() == "all"){
			var parent_class_id = "/"+$('select[name=parent_class_id]').val();
			var parent_section_id = "/"+$('select[name=parent_section_id]').val();
			var link = "{{ url('parents/get_parents_list') }}"+parent_class_id+parent_section_id;
			$.ajax({
				url: link,
				beforeSend: function(){
					$("#preloader").css("display","block");
				},success: function(data){
					$("#preloader").css("display","none");
					var json =JSON.parse(data);
					$('#user_list').html("");

					jQuery.each( json, function( i, val ) {
						$('#user_list')
								.append('<div class="col-md-12">'+
										'<label class="c-container">'+
										'<input type="checkbox" value="'+val['phone']+'" name="parents[]" checked="true">'+val['parent_name']+" - "+val['phone']+
										'<span class="checkmark"></span>'+
										'</label>'+
										'</div>');
					});

				}
			});
		}else{
			$('#user_list').html("");
		}
	}

	function get_all_users(){
		if($("#user_id").val() == "all"){
			var user_type = "/"+$('select[name=user_type]').val();
			var link = "{{ url('users/get_users') }}"+user_type;
			$.ajax({
				url: link,
				beforeSend: function(){
					$("#preloader").css("display","block");
				},success: function(data){
					$("#preloader").css("display","none");
					var json =JSON.parse(data);
					$('#user_list').html("");

					jQuery.each( json, function( i, val ) {
					   $('#user_list')
					   .append('<div class="col-md-12">'+
									'<label class="c-container">'+
									   '<input type="checkbox" value="'+val['phone']+'" name="users[]" checked="true">'+val['name']+
									   '<span class="checkmark"></span>'+
									'</label>'+
								'</div>');
					});

				}
			});
		}else{
		  $('#user_list').html("");
		}
	}

</script>
@stop

