@extends('layouts.backend')
@section('content')
    <div class="row">
        <strong class="panel-title" style="display: none;">{{ ('Student Details') }}</strong>
        <div class="col-md-4">
            <div class="card card-user">
                <div class="image">
                    <img src="{{ asset('public/uploads/images/background/background.jpg') }}" alt="...">
                </div>
                <div class="content">
                    <div class="author">
                        <img class="avatar border-white" src="{{ asset('public/uploads/images/'.$student->image) }}"
                             alt="{{ $student->first_name }}">
                        <h4 class="title">{{ $student->first_name .' '.$student->last_name }}</h4>
                    </div>
                </div>
                <hr>
                <div class="text-center">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-1">
                            <h5>{{ ('Class') }}<br>
                                <small>{{ $student->class_name }}</small>
                            </h5>
                        </div>
                        <div class="col-md-4">
                            <h5>{{ ('Section') }}<br>
                                <small>{{ $student->section_name }}</small>
                            </h5>
                        </div>
                        <div class="col-md-3">
                            <h5>{{ ('Roll') }}<br>
                                <small>{{ $student->roll }}</small>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <ul class="nav nav-tabs setting-tab">
                <li class="active">
                    <a data-toggle="tab" href="#profile" aria-expanded="true">{{ ('Profile') }}</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#parent" aria-expanded="false">{{ ('Parent') }}</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#invoices" aria-expanded="false">{{ ('Invoices') }}</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#payments_history" aria-expanded="false">{{ ('Payments History') }}</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#student_certificate" aria-expanded="false">{{ ('Certificate') }}</a>
                </li>
                <li class="pull-right">
                    <div style="padding: 7px;">
                        <a href="{{ url('students/certificate',$student->id) }}"
                           class="btn btn-primary rect-btn btn-sm"><i
                                    class="fa fa-upload" aria-hidden="true"></i>{{ ('Upload Certificate') }}</a>
                    </div>
                </li>
                <li class="pull-right">
                    <div style="padding: 7px;">
                        <a href="{{ route('students.edit',$student->id) }}" class="btn btn-primary rect-btn btn-sm"><i
                                    class="fa fa-pencil" aria-hidden="true"></i>{{ ('Edit') }}</a>
                    </div>
                </li>
            </ul>
            <div class="tab-content">
                <div id="profile" class="tab-pane fade in active">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table table-custom" width="100%">
                                <tbody>
                                <tr>
                                    <td>{{ ('Register NO') }}</td>
                                    <td>{{ $student->register_no }}</td>
                                </tr>
                                @if($student->group != '')
                                    <tr>
                                        <td>{{ ('Group') }}</td>
                                        <td>{{ $student->group }}</td>
                                    </tr>
                                @endif
                                @if($student->optional_subject != '')
                                    <tr>
                                        <td>{{ ('Optional Subject') }}</td>
                                        <td>{{ $student->optional_subject }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <td>{{ ('Date Of Birth') }}</td>
                                    <td>{{ date('d M, Y', strtotime($student->birthday)) }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Gender') }}</td>
                                    <td>{{ $student->gender  }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Blood Group') }}</td>
                                    <td>{{ $student->blood_group }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Religion') }}</td>
                                    <td>{{ $student->religion }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Phone') }}</td>
                                    <td>{{ $student->phone }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Address') }}</td>
                                    <td>{{ $student->address }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('State') }}</td>
                                    <td>{{ $student->state }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Country') }}</td>
                                    <td>{{ $student->country }}</td>
                                </tr>
                                @if($student->activities != '')
                                    <tr>
                                        <td>{{ ('Extra Curricular Activities') }}</td>
                                        <td>{{ $student->activities }}</td>
                                    </tr>
                                @endif
                                @if($student->remarks != '')
                                    <tr>
                                        <td>{{ ('Remarks') }}</td>
                                        <td>{{ $student->remarks }}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="parent" class="tab-pane fade">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table table-custom" width="100%">
                                <tbody>
                                @if(!empty($parent))
                                    <tr>
                                        <td>{{ ("Father's Name") }}</td>
                                        <td>{{ $parent->f_name  }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ ("Mother's Name") }}</td>
                                        <td>{{ $parent->m_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ ("Father's Profession") }}</td>
                                        <td>{{ $parent->f_profession }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ ("Mother's Profession") }}</td>
                                        <td>{{ $parent->m_profession }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ ("Guardian's Name") }}</td>
                                        <td>{{ $parent->parent_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ ('Phone') }}</td>
                                        <td>{{ $parent->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ ('Email') }}</td>
                                        <td>{{ $parent->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ ('Address') }}</td>
                                        <td>{{ $parent->address }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="2">{{ ('No Data Found') }}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="invoices" class="tab-pane fade">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="content no-export">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>{{ ('Due Date') }}</th>
                                        <th>{{ ('Title') }}</th>
                                        <th>{{ ('Total') }}</th>
                                        <th>{{ ('Paid') }}</th>
                                        <th>{{ ('Status') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $currency = get_option('currency_symbol') @endphp
                                    @foreach($invoices as $invoice)
                                        <tr>
                                            <td>{{ date('d M, Y', strtotime($invoice->due_date)) }}</td>
                                            <td style='max-width:250px;'>{{ $invoice->title }}</td>
                                            <td>{{ $currency." ".$invoice->total }}</td>
                                            <td>{{ $currency." ".$invoice->paid }}</td>
                                            <td>
                                                {!! $invoice->status=="Paid" ? '<i class="fa fa-circle paid"></i>'.$invoice->status : '<i class="fa fa-circle unpaid"></i>'.$invoice->status !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="payments_history" class="tab-pane fade">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="content no-export">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>{{ ('Date') }}</th>
                                        <th>{{ ('Amount') }}</th>
                                        <th>{{ ('Note') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $currency = get_option('currency_symbol') @endphp
                                    @foreach($payments_history as $data)
                                        <tr>
                                            <td class='date'>{{ date('d M, Y', strtotime($data->date)) }}</td>
                                            <td class='amount'>{{ $currency." ".$data->amount }}</td>
                                            <td class='note'>{{ $data->note }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="student_certificate" class="tab-pane fade">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            @foreach($certificates as $certificate)
                                @if(($certificate['certificate']) != NULL)
                                    @php
                                        $strExt = explode('.', $certificate->certificate);
                                    @endphp
                                    <div class="col-md-4">
                                        <div class="well well-sm">
                                            <div class="">
                                                @if($strExt[1] === 'pdf')
                                                    <img style="height: 175px; width: 100%"
                                                         class="thumbnail img-responsive"
                                                         src="{{ asset('public/uploads/images/pdf.png') }}"/>
                                                @else
                                                    <img style="height: 175px; width: 100%"
                                                         class="thumbnail img-responsive"
                                                         src="{{ asset('public/uploads/images/student_certificate/'.$certificate->certificate) }}"/>
                                                @endif
                                                <form action="{{ url('students/certificate_remove',$certificate->id) }}"
                                                      method="post" style="margin-left: 5px">
                                                    <a download class="btn btn-primary btn-xl"
                                                       href="{{ asset('public/uploads/images/student_certificate/'.$certificate->certificate) }}">
                                                        <i class="fa fa-download"></i>
                                                    </a>
                                                    <a class="btn btn-info btn-xl" data-fancybox="filter"
                                                       data-caption="student_certificate" target="_blank"
                                                       href="{{ asset('public/uploads/images/student_certificate/'.$certificate->certificate) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>

                                                    {{ method_field('POST') }}
                                                    @csrf
                                                    <button type="submit"
                                                            class="btn btn-danger btn-xl btn-certificate-remove"><i
                                                                class="fa fa-eraser" aria-hidden="true"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-fancybox="filter"]').fancybox({
                buttons: [
                    'slideShow',
                    'fullScreen',
                    'thumbs',
                    'download',
                    'zoom',
                    'close'
                ]
            });
        });
    </script>
@stop
