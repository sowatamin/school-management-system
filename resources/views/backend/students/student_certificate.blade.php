@extends('layouts.backend')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">{{  $certificate->first_name }} {{ $certificate->last_name }}`s certificate Upload</div>
                </div>
                <div class="panel-body">
                    <div class="col-md-10">
                        <form id="studentApplicationInf" action="{{url('students/certificate_store')}}"  autocomplete="off"
                              class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data"
                              method="post" accept-charset="utf-8">
                            @csrf

                            <input type="hidden" name="student_id" value="{{ $certificate->id }}">
                            <input type="hidden" name="user_id" value="{{ $certificate->user_id }}">

                            <div class="form-group">
                                <label for="student_certificate" class="col-sm-3 control-label">
                                    Student Certificate
                                </label>
                                <div class="col-md-5">
                                    <input id="student_certificate" type="file" class="form-control student_certificate"
                                           name="student_certificate[]" value="" style="height: auto;" required>
                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                </div>
                                <div class="col-md-2 col-xs-6 text-center addButtonWrap">
                                            <span type="button"
                                                  class="label label-success pictureAttachment certificateAttachmentElement"
                                                  style="background: black !important;cursor: pointer">
                                                <i class="fa fa-plus"></i> Add More
                                            </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="studentCertificateElement hide">
                                <div class="col-md-5 col-md-offset-3 appendElement" style="margin-top: 15px">
                                    <input id="" type="file" class="form-control student_certificate"
                                           name="student_certificate[]" value="" style="height: auto;" required>
                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                </div>
                                <div class="col-md-2 col-xs-6 text-center deleteButtonWrap" style="margin-top: 15px">
                                    <span type="button" class="label label-danger deleteAttachment"
                                          style="cursor: pointer">
                                        <i class="fa fa-trash"></i> Delete
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-info">Add Certificate</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".pictureAttachment").on("click", function () {
                var attachInput = $(this).parents(".addButtonWrap").siblings("div").find("input");
                if ($(this).hasClass("certificateAttachmentElement")) {
                    var appendHtml = $(".studentCertificateElement").html();
                } else {
                    alert("something went wrong !!!");
                }
                if (attachInput.val() == '') {
                    attachInput.addClass("errorInput");
                } else {
                    var appendElement = $(this).parents(".addButtonWrap").siblings(".appendElement:last");
                    if (appendElement.length === 0) {
                        attachInput.removeClass('errorInput');
                        $(this).parents(".addButtonWrap").before(appendHtml);
                        $(this).parents(".addButtonWrap").css("margin-top", "15px");
                    } else {
                        if (appendElement.find("input").val() == '') {
                            appendElement.find("input").addClass("errorInput");
                        } else {
                            appendElement.find("input").removeClass("errorInput");
                            appendElement.siblings(".addButtonWrap").before(appendHtml);
                            appendElement.siblings(".addButtonWrap").css("margin-top", "15px");
                        }
                    }

                }
            });
            $("#studentApplicationInf").on("click", ".deleteAttachment", function () {
                var appendElement = $(this).parents(".deleteButtonWrap").siblings(".appendElement").length;
                $(this).parents(".deleteButtonWrap").prev(".appendElement").remove();
                $(this).parents(".deleteButtonWrap").remove();
                if (appendElement === 1) {
                    $(".pictureAttachment").parents(".addButtonWrap").css("margin-top", "0px");
                }
            });
        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            var maxSize = '1024';
            var _validFileExtensions = [];
            var imgExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG"];
            var fileExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG", ".pdf", ".PDF"];

            function validateSingleInput(oInput) {
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        if (oInput.className.match(/\bonlyImg\b/)) {
                            _validFileExtensions = imgExtensions;
                        } else {
                            _validFileExtensions = fileExtensions;
                        }
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }

            function fileSizeValidate(fdata) {
                if (fdata.files && fdata.files[0]) {
                    var fsize = fdata.files[0].size / 1024;
                    if (fsize > maxSize) {
                        fdata.value = "";
                        return false;
                    } else {
                        return true;
                    }
                }
            }

//                For student certificate

            $(document).on("change", ".student_certificate", function () {
                var noExtentionError = validateSingleInput(this);
                var noSizeError = fileSizeValidate(this);
                if (noExtentionError === false) {
                    $(this).siblings(".oError").show().text("The certified copy must be JPG, JPEG, PNG, PDF format.");
                    return false;
                } else {
                    $(this).siblings(".oError").hide();
                    if (noSizeError === false) {
                        $(this).siblings(".oError").show().text("The certified copy will be less than 1 MB. ");
                        return false;
                    } else {
                        $(this).siblings(".oError").hide();
                    }
                }
            });

        });
    </script>

@stop

