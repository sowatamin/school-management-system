@extends('layouts.backend')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title">Students List</span>
                    <select id="class" class="select_class pull-right" onchange="showClass(this);">
                        <option value="">Select Class</option>
                        {{ create_branch_class_option('classes','id','class_name',$class) }}
                    </select>
                    <a href="{{url('students/excel_import')}}" style="margin-left: 10px;"
                       class="btn btn-primary btn-sm pull-right ajax-modal"
                       data-title="Import Excel">Import Excel</a>
                    <a href="{{route('students.create')}}"
                       class="btn btn-primary btn-sm pull-right">{{('Add New Student')}}</a>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered data-table">
                        <thead>
                        <th>Profile</th>
                        <th>Roll</th>
                        <th>Name</th>
                        <th>Class</th>
                        <th>Section</th>
                        <th>Email</th>
                        <th>ID Card</th>
                        <th>Active</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @foreach($students AS $data)
                            <tr>
                                <td>
                                    <img class="img-circle" src="{{ asset('public/uploads/images/'.$data->image) }}" width="50px" alt="">
                                </td>
                                <td>{{ $data->roll }}</td>
                                <td>{{ $data->name }}</td>
                                <td>{{ $data->class_name }}</td>
                                <td>{{ $data->section_name }}</td>
                                <td>{{ $data->email }}</td>
                                <td><a href="{{ url('students/id_card/'.$data->id) }}"
                                       class="btn btn-primary btn-sm ajax-modal">{{ ('View') }}</a></td>
                                <td>
                                    @if ($data->status == 'Active')
                                        <form action="{{ route('students.deactivate',$data->id) }}" method="post">
                                            {{ method_field('POST') }}
                                            @csrf
                                            <button type="submit" class="btn btn-primary btn-xs btn-inactive"><i
                                                        class="fa fa-check" aria-hidden="true"></i></button>
                                        </form>
                                    @else
                                        <form action="{{ route('students.active',$data->id) }}" method="post">
                                            {{ method_field('POST') }}
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-xs btn-active"><i
                                                        class="fa fa-minus-circle" aria-hidden="true"></i></button>
                                        </form>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('students.show',$data->id) }}"
                                       class="btn btn-info btn-xs"><i
                                                class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="{{ route('students.edit',$data->id) }}"
                                       class="btn btn-warning btn-xs"><i
                                                class="fa fa-pencil" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-script')
    <script>
        function showClass(elem) {
            if ($(elem).val() == "") {
                return;
            }
            window.location = "<?php echo url('students/class') ?>/" + $(elem).val();
        }
    </script>
@stop
