@extends('layouts.backend')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading panel-title">{{ ('View Page') }}</div>

	<div class="panel-body">

	  <table class="table table-bordered">
		<tr><td>{{ ('Featured Image') }}</td><td><img src="{{ $page->featured_image !="" ? asset('public/uploads/media/'.$page->featured_image) : asset('public/uploads/no_image.jpg') }}" style="max-width:200px;"></td></tr>
		<tr><td>{{ ('Page Template') }}</td><td>{{ $page->page_template }}</td></tr>
		<tr><td>{{ ('Title') }}</td><td>{{ $page->content[0]->page_title }}</td></tr>
		<tr><td>{{ ('Content') }}</td><td>{!! $page->content[0]->page_content !!}</td></tr>
		<tr><td>{{ ('Slug') }}</td><td>{{ $page->slug }}</td></tr>
		<tr><td>{{ ('Post Status') }}</td><td>{{ $page->page_status }}</td></tr>
		<tr><td>{{ ('Author') }}</td><td>{{ $page->author->name }}</td></tr>
		<tr><td>{{ ('Author Type') }}</td><td>{{ $page->author->user_type }}</td></tr>
	  </table>

	</div>
  </div>
 </div>
</div>
@endsection


