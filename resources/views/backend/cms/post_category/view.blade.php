@extends('layouts.backend')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading">{{ ('View Post Category') }}</div>

	<div class="panel-body">
	  <table class="table table-bordered">
		<tr><td>{{ ('Category Name') }}</td><td>{{ $postcategory->category }}</td></tr>
		<tr><td>{{ ('Note') }}</td><td>{{ $postcategory->note }}</td></tr>
		<tr><td>{{ ('Parent Category') }}</td><td>{{ $postcategory->parent_id }}</td></tr>
	  </table>
	</div>
  </div>
 </div>
</div>
@endsection


