@extends('layouts.backend')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading">{{ ('Add Site Navigation') }}</div>

	<div class="panel-body">
	  <form method="post" class="validate" autocomplete="off" action="{{url('site_navigations')}}" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="col-md-12">
		  <div class="form-group">
			<label class="control-label">{{ ('Menu Name') }}</label>
			<input type="text" class="form-control" name="menu_name" value="{{ old('menu_name') }}" required>
		  </div>
		</div>


		<div class="form-group">
		  <div class="col-md-12">
		    <button type="reset" class="btn btn-danger">{{ ('Reset') }}</button>
			<button type="submit" class="btn btn-primary">{{ ('Save') }}</button>
		  </div>
		</div>
	  </form>
	</div>
  </div>
 </div>
</div>
@endsection


