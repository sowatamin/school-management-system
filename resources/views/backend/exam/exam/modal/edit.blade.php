<form method="post" class="validate" autocomplete="off" action="{{action('ExamController@update', $id)}}" enctype="multipart/form-data">
	{{ csrf_field()}}
	<input name="_method" type="hidden" value="PATCH">

	<div class="col-md-12">
	 <div class="form-group">
		<label class="control-label">{{ ('Name') }}</label>
		<input type="text" class="form-control" name="name" value="{{$exam->name}}" required>
	 </div>
	</div>

	<div class="col-md-12">
	 <div class="form-group">
		<label class="control-label">{{ ('Note') }}</label>
		<textarea class="form-control" name="note">{{$exam->note}}</textarea>
	 </div>
	</div>


	<div class="form-group">
	  <div class="col-md-12">
		<button type="submit" class="btn btn-primary">{{ ('Update') }}</button>
	  </div>
	</div>
</form>


