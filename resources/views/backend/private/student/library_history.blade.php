@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title" >
					{{ ('My Library History')}}
				</div>
			</div>
			<div class="panel-body">
				@if( !empty($issues) )
				<div class="col-md-12">
					<div class="panel-heading text-center">
						<div class="panel-title" >
							{{  ('Book Issues Of') }}<br>
							{{  ('Member Name - ') }}{{ $member->name }}<br>
							{{  ('Library Id - ') }}{{ $member->library_id }}<br>
						</div>
					</div>

					<table class="table table-bordered data-table">
						<thead>
							<tr>
								<th>{{ ('Book Name')}}</th>
								<th>{{ ('Category')}}</th>
								<th>{{ ('Issue Date')}}</th>
								<th>{{ ('Due Date')}}</th>
								<th>{{ ('Return Date')}}</th>
								<th>{{ ('Status')}}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($issues AS $data)
							<tr>

								<td>{{$data->name}}</td>
								<td>{{$data->category_name}}</td>
								<td>{{date('d/ M/ Y', strtotime($data->issue_date))}}</td>
								<td>{{date('d/ M/ Y', strtotime($data->due_date))}}</td>
								<td>@if($data->return_date != '' ){{date('d-M-Y', strtotime($data->return_date))}}@endif</td>
								<td>@if($data->status == '1') <span class="badge badge-danger">{{  ('Not Return Yet') }}</span> @else <span class="badge badge-success">{{ lang('Returned') }}</span> @endif</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				@else
					<h5 class="text-center">{{  ('Sorry, No Records Found !') }}</h5>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection
