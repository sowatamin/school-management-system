@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title">Grade Chart</span>
                </div>

                <div class="panel-body">
                    <nav class="navbar navbar-default child-profile-menu" style="margin-bottom: 10px">
                        <div class="container-fluid">
                            <ul class="nav navbar-nav">
                                <li class="active" >
                                    <a href="{{ url('student/mark_chart/'.$student_id) }}">Mark Chart</a>
                                </li>
                                <li>
                                    <a href="{{ url('student/grade_chart/'.$student_id) }}">Grade Chart</a>
                                </li>
                            </ul>
                        </div>
                    </nav>

                </div>
            </div>
        </div>
    </div>

@endsection

