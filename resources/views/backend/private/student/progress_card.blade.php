@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="panel-title" >
					{{('My Progress Card')}}
				</span>
			</div>
			<div class="panel-body">

				<!--Show Full Report Card-->
				  @if(!empty($exams))
					<div class="panel panel-default" id="progress_card">
						<div class="panel-heading">
							<span class="panel-title">Progress Card</span>
							<button type="button" data-print="progress_card"
									class="btn btn-primary btn-sm pull-right print"><i class="fa fa-print"></i> Print
								Progress Card
							</button>
						</div>
						<div class="panel-body">
							<table class="table table-bordered">
								<tr>
									<td colspan="4" style="text-align:center;"><img width="100px" style="border-radius: 8px; padding:5px; border:2px solid #ccc;"
																					src="{{ asset('public/uploads/images/'.$student->image) }}">
									</td>
								</tr>
								<tr>
									<td><b>School</b></td>
									<td>{{ get_option('school_name') }}</td>
									<td><b>Student Name</b></td>
									<td>{{ $student->first_name." ".$student->last_name }}</td>
								</tr>
								<tr>
									<td><b>Class</b></td>
									<td>{{ $student->class_name }}</td>
									<td><b>Section</b></td>
									<td>{{ $student->section_name }}</td>
								</tr>
								<tr>
									<td><b>Roll</b></td>
									<td>{{ $student->roll }}</td>
									<td><b>{{ ('Reg No') }}</b></td>
									<td>{{ $student->register_no }}</td>
								</tr>
								<tr>
									<td><b>Academic Year</b></td>
									<td>{{ get_academic_year(get_option('academic_year')) }}</td>
									<td><b>Group</b></td>
									<td>{{ $student->group_name }}</td>
								</tr>
							</table>
							<div class="table-responsive">
								<table class="table table-bordered report-card">
									<thead>
									<tr>
										<th rowspan="2">Subject</th>
										@foreach($exams as $exam)
											<th style="background:#bdc3c7;text-align:center">
												<b>{{ $exam->mark_distribution_type }}</b>
											</th>
										@endforeach
										<th rowspan="2">Total</th>
										<th rowspan="2">Grade</th>
										<th rowspan="2">Point</th>
									</tr>

									</thead>
									<tbody>

									@php $total = 0; @endphp
									@php $t_point = 0; @endphp
									@php $total_subject = 0; @endphp

									@foreach($subjects as $subject)
										@php $row_total=0; @endphp
										@php $point=0; @endphp
										<tr>
											<td>{{ $subject->subject_name }}</td>
											@foreach($exams as $exam)
												@if(isset($mark_details[$subject->id][$exam->mark_distributions_id]))
													@foreach($mark_details[$subject->id][$exam->mark_distributions_id] as $md)
														@php
															$row_total = floor($row_total + $md->mark_weight_value);
                                                            $point = get_point($row_total);
                                                            $grade = get_grade($row_total);
														@endphp
														@if ($md->mark_value != null)
															<td style="text-align:center">{{ floor($md->mark_weight_value) }}</td>
														@else
															<td style="text-align:center">N/A</td>
														@endif
													@endforeach
												@endif
												@php $total_subject++  @endphp
											@endforeach
											@if (!empty($row_total))
												<td>{{ $row_total }}</td>
												<td>{{ $grade }}</td>
												<td>{{ $point }}</td>
											@endif
										</tr>
										@php $total = $total + $row_total; @endphp
										@php $t_point = (double)$t_point + (double)$point; @endphp
									@endforeach
									<tr>
										<td colspan="100%" style="text-align:center">
											<h5>{!! ('Total Marks')." : <b>".floor($total)."</b>" !!}</h5>
										</td>
									</tr>
									<tr>
										<td colspan="100%" style="text-align:center">
											<h5>{!! ('Average Marks')." : <b>".floor(($total/count($subjects)))."</b>" !!}</h5>
										</td>
									</tr>
									<tr>
										<td colspan="100%" style="text-align:center">
											<h5>{!! ('Point')." : <b>".round(($t_point/count($subjects)), 2)."</b>" !!}</h5>
										</td>
									</tr>
									<tr>
										<td colspan="100%" style="text-align:center">
											<h5>{!! ('Grade')." : <b>".get_grade(floor($total/count($subjects)))."</b>" !!}</h5>
										</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				  @endif
			</div>
		</div>
	</div>
</div>
@endsection

@section('js-script')
<script type="text/javascript">
	function getData(val) {
		var _token=$('input[name=_token]').val();
		var class_id=$('select[name=class_id]').val();
		$.ajax({
			type: "POST",
			url: "{{url('sections/section')}}",
			data:{_token:_token,class_id:class_id},
			beforeSend: function(){
				$("#preloader").css("display","block");
			},success: function(sections){
				$("#preloader").css("display","none");
				$('select[name=section_id]').html(sections);
			}
		});
	}

	function get_students(){

		var class_id = "/"+$('select[name=class_id]').val();
		var section_id = "/"+$('select[name=section_id]').val();
		var link = "{{ url('students/get_students') }}"+class_id+section_id;
		$.ajax({
			url: link,
			beforeSend: function(){
				$("#preloader").css("display","block");
			},success: function(data){
				$("#preloader").css("display","none");
				var json =JSON.parse(data);
				   $('select[name=student_id]').html("");

				jQuery.each( json, function( i, val ) {
				   $('select[name=student_id]').append("<option value='"+val['id']+"'>Roll "+val['roll']+" - "+val['first_name']+" "+val['last_name']+"</option>");
				});

			}
		});
	}

</script>
@stop
