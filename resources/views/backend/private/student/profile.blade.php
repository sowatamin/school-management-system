@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title" >
					{{ ('My Profile')}}
				</div>
			</div>

			<table class="table table-striped table-bordered" width="100%">
				<tbody>
					<tr>
						<td style="text-align: center;" colspan="4"><img width="200px" style="border-radius: 7px;" src="{{ asset('public/uploads/images/'.$student->image) }}"></td>
					</tr>
					<tr>
						<td colspan="2">{{ ('Name')}}</td>
						<td colspan="2">{{$student->first_name." ".$student->last_name}}</td>
					</tr>
					<tr>
						<td>{{ ('Guardian')}}</td>
						<td>{{$student->parent_name}}</td>
						<td>{{ ('Date Of Birth')}}</td>
						<td>{{$student->birthday}}</td>
					</tr>
					<tr>
						<td>{{ ('Gender')}}</td>
						<td>{{$student->gender}}</td>
						<td>{{ ('Blood Group')}}</td>
						<td>{{$student->blood_group}}</td>
					</tr>
					<tr>
						<td>{{ ('Religion')}}</td>
						<td>{{$student->religion}}</td>
						<td>Address</td>
						<td>{{$student->address}}</td>
					</tr>
					<tr>
						<td>{{ ('Phone')}}</td>
						<td>{{$student->phone}}</td>
						<td>{{ ('Email')}}</td>
						<td>{{$student->email}}</td>
					</tr>
					<tr>
						<td>{{ ('State')}}</td>
						<td>{{$student->state}}</td>
						<td>{{ ('Country')}}</td>
						<td>{{$student->country}}</td>
					</tr>
					<tr>
						<td>{{ ('Class')}}</td>
						<td>{{$student->class_name}}</td>
						<td>{{ ('Section')}}</td>
						<td>{{$student->section_name}}</td>
					</tr>
					<tr>
						<td>{{ ('Register No')}}</td>
						<td>{{$student->register_no}}</td>
						<td>{{ ('Roll')}}</td>
						<td>{{$student->roll}}</td>
					</tr>
					<tr>
						<td>{{ ('Group')}}</td>
						<td>{{$student->group}}</td>
						<td>{{ ('Optional Subject')}}</td>
						<td>{{$student->optional_subject}}</td>
					</tr>
					<tr>
						<td>{{ ('Activities')}}</td>
						<td>{{$student->activities}}</td>
						<td>{{ ('Remarks')}}</td>
						<td>{{$student->remarks}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
