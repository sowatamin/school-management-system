@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="header">
				<div class="col-md-6">
					<h4 class="title">My Assignments</h4>
				</div>
			</div>
			<div class="content no-export">
				<table class="table table-bordered data-table">
					<thead>
						<th>Title</th>
						<th>Description</th>
						<th>Subject</th>
						<th>Details</th>
					</thead>
					<tbody>
						@foreach($assignments AS $data)
						<tr>
							<td>{{$data->title}}</td>
							<td>{{substr(strip_tags($data->description),0,100)}}...</td>
							<td>{{$data->subject_name}}</td>
							<td>
								@if($data->file)
								<li class="dropdown" style="display: inline;">
									<a href="#" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-download"></i>
									</a>
									<ul class="dropdown-menu">
										@if($data->file)
										<li><a href="{{ asset('public/uploads/files/assignments/'.$data->file) }}">File 1</a></li>
										@endif
										@if($data->file_2)
										<li><a href="{{ asset('public/uploads/files/assignments/'.$data->file_2) }}">File 2</a></li>
										@endif
										@if($data->file_3)
										<li><a href="{{ asset('public/uploads/files/assignments/'.$data->file_3) }}">File 3</a></li>
										@endif
										@if($data->file_4)
										<li><a href="{{ asset('public/uploads/files/assignments/'.$data->file_4) }}">File 4</a></li>
										@endif
									</ul>
								</li>
								@endif
								<a href="{{ url('student/view_assignment/'.$data->id) }}" class="btn btn-primary btn-xs ajax-modal" data-title="View Assignment" data-fullscreen="true"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

