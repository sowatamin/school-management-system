<div class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-bordered" width="100%">
			<tbody>
				<tr>
					<td>{{('Title')}}</td>
					<td colspan="5"><b>{{$assignment->title}}</b></td>
				</tr>
				<tr>
					<td>{{("Class")}}</td>
					<td><b>{{$assignment->class_name}}</b></td>
					<td>{{("Section")}}</td>
					<td><b>{{$assignment->section_name}}</b></td>
					<td>{{("Subject")}}</td>
					<td><b>{{$assignment->subject_name}}</b></td>
				</tr>
				<tr>
					<td colspan="6" class="details-td">{!! $assignment->description !!}</td>
				</tr>
				<tr>
					<td>{{("Assignment Files")}}</td>
					<td colspan="5">
						@if($assignment->file)
						<a class="btn btn-primary rect-btn" target="_blank" href="{{ asset('public/uploads/files/assignments/'.$assignment->file) }}">{{ ('Download File 1') }}</a>
						@endif
						@if($assignment->file_2)
						<a class="btn btn-primary rect-btn" target="_blank" href="{{ asset('public/uploads/files/assignments/'.$assignment->file_2) }}">{{ ('Download File 2') }}</a>
						@endif
						@if($assignment->file_3)
						<a class="btn btn-primary rect-btn" target="_blank" href="{{ asset('public/uploads/files/assignments/'.$assignment->file_3) }}">{{ ('Download File 3') }}</a>
						@endif
						@if($assignment->file_4)
						<a class="btn btn-primary rect-btn" target="_blank" href="{{ asset('public/uploads/files/assignments/'.$assignment->file_4) }}">{{ ('Download File 4') }}</a>
						@endif
					</td>
				</tr>
		</tbody>
	</table>
   </div>
</div>
