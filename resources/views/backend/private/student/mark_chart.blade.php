@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title">Mark Chart</span>
                </div>

                <div class="panel-body">
                    <nav class="navbar navbar-default child-profile-menu" style="margin-bottom: 10px">
                        <div class="container-fluid">
                            <ul class="nav navbar-nav">
                                <li class="active" >
                                    <a href="{{ url('student/mark_chart/'.$student_id) }}">Mark Chart</a>
                                </li>
                            </ul>
                        </div>
                    </nav>

                    <div class="col-md-12">
                        <form id="search_form" class="params-panel validate"
                              action="{{ url('student/mark_chart/' . $student_id) }}" method="post" autocomplete="off"
                              accept-charset="utf-8">
                            @csrf
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Subject</label>
                                    <select name="subject_id" class="form-control select2" required>
                                        <option value="">Select One</option>
                                        {{ create_student_subject_option('subjects','id','subject_name',$subject_id, $student_id) }}
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group" style="margin-top:24px;">
                                    <button type="submit" class="btn btn-primary btn-block rect-btn">View Mark Chart
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-12" style="margin-top: 10px">
                        @if( !empty($student_subjects) )
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Title</th>
                                    <th>Weighted Marks</th>
                                    <th>Marks For The Exam</th>
                                    <th>Marks Obtained</th>
                                    <th>Weighted Marks Obtained</th>
                                    <th>Avg Marks</th>
                                    <th>Highest Marks</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sl = 1; @endphp
                                @foreach($student_subjects as $student_subject)
                                    <tr id="row_{{ $student_subject->id }}">
                                        <td>{{ $sl++ }}</td>
                                        <td>{{ $student_subject->mark_distribution_type }}</td>
                                        <td>{{ (int)$student_subject->mark_weight }}%</td>
                                        <td>{{ $student_subject->mark_for_the_exam }}</td>
                                        <td>
                                            @if (!empty($student_subject->mark_value))
                                                {{ $student_subject->mark_value }}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                        <td>
                                            @if (!empty($student_subject->mark_value))
                                                {{ $student_subject->mark_weight_value }}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                        <td>{{ avg_value($student_subject->subject_id, $student_subject->mark_distributions_id) }}</td>
                                        <td>{{ max_value($student_subject->subject_id, $student_subject->mark_distributions_id) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <div class="col-md-12" style="margin-top: 15px">
                        @if( !empty($lava) )
                            <div class="params-panel">
                                <div id="bar_div"></div>
                                <?= $lava->render('BarChart', 'BarChart', 'bar_div') ?>
                            </div>

                            <div class="params-panel" style="margin-top: 15px">
                                <div id="line_div"></div>
                                <?= $lava->render('LineChart', 'LineChart', 'line_div') ?>
                            </div>
                        @elseif (!empty($message))
                            <div class="alert alert-danger"><h5>{{ $message }}</h5></div>
                        @endif
                    </div>
                    <div class="col-md-12" style="margin-top: 15px">
                        @if( !empty($lava) )
                            @if( $predicted_grades !=0)
                                @php $student_name = get_student_name($student_id) @endphp
                                <div class="params-panel" style="text-align: center">
                                    <div id="bar_div"></div>
                                    <h4 style="text-align: center;">Grade Prediction</h4>
                                    <h5 style="text-align: center">Marks & Grade Prediction for <strong>{{$next_exam}} Exam</strong></h5>
                                    <h5 style="text-align: center">Total Marks = <span style="color: green">{{$next_exam_mark}}</span></h5>
                                    <p>{{$student_name}} Grade so far is {{$grade}}</p>
                                    @for($i=0 ; $i<count($predicted_grades);$i++)
                                        @if($i<count($predicted_grades)-1 && $predicted_grades[$i+1]['grade'] == $predicted_grades[$i]['grade'])
                                            @continue;
                                        @else
                                            <p>{{$student_name}} Have to score at least  {{ $predicted_grades[$i]['mark'] }} to obtain {{ $predicted_grades[$i]['grade'] }}</p>
                                        @endif
                                    @endfor
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

