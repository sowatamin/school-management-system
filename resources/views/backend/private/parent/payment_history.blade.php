@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title">
                         {{ get_student_name($student_id) }}( Payment History )
                    </span>
                </div>

                <div class="panel-body">
                    <nav class="navbar navbar-default child-profile-menu" style="margin-bottom: 10px">
                        <div class="container-fluid">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ url('parent/my_children/'.$student_id) }}">Profile</a></li>
                                <li><a href="{{ url('parent/children_attendance/'.$student_id) }}">Attendance</a></li>
                                <li><a href="{{ url('parent/progress_card/'.$student_id) }}">Progress Card</a></li>
                                <li><a href="{{ url('parent/invoice/'.$student_id) }}">invoice</a></li>
                                <li class="active">
                                    <a href="{{ url('parent/payment_history/'.$student_id) }}">Payment History</a>
                                </li>
                                <li><a href="{{ url('parent/mark_chart/'.$student_id) }}">Mark Chart</a></li>
                            </ul>
                        </div>
                    </nav>

                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>{{ ('Invoice ID') }}</th>
                            <th>{{ ('Date') }}</th>
                            <th>{{ ('Amount') }}</th>
                            <th>{{ ('Note') }}</th>
                            <th>{{ ('View Invoice') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $currency = get_option('currency_symbol') @endphp
                        @foreach($studentpayments as $studentpayment)
                            <tr id="row_{{ $studentpayment->id }}">
                                <td class='invoice_id'>{{ $studentpayment->invoice_id }}</td>
                                <td class='date'>{{ date('d/ M/ Y', strtotime($studentpayment->date)) }}</td>
                                <td class='amount'>{{ $currency." ".$studentpayment->amount }}</td>
                                <td class='note'>{{ $studentpayment->note }}</td>
                                <td><a href="{{ url('parent/view_invoice/'.$studentpayment->invoice_id) }}"
                                       data-title="{{ ('View Invoice') }}" data-fullscreen="true"
                                       class="btn btn-primary btn-sm ajax-modal">{{ ('View Invoice') }}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js-script')
    <script>
        function showClass(elem) {
            if ($(elem).val() == "") {
                return;
            }
            window.location = "<?php echo url('student_payments/class') ?>/" + $(elem).val();
        }
    </script>
@stop
