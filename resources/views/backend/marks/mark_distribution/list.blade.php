@extends('layouts.backend')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading"><span class="panel-title">{{ ('List Mark Distribution') }}</span>
			<a class="btn btn-primary btn-sm pull-right" data-title="{{ ('Add Mark Distribution') }}" href="{{route('mark_distributions.create')}}">{{ ('Add New') }}</a>
			</div>

			<div class="panel-body">
			<table class="table table-bordered data-table">
			<thead>
			  <tr>
				<th>Mark Distribution Type</th>
				<th>Mark For The Exam</th>
				<th>Mark Weight</th>
				<th style="text-align: center;">Active</th>
				<th>Action</th>
			  </tr>
			</thead>
			<tbody>

			  @foreach($markdistributions as $markdistribution)
			  <tr id="row_{{ $markdistribution->id }}">
				    <td class='mark_distribution_type'>{{ $markdistribution->mark_distribution_type }}</td>
					<td class='mark_percentage'>{{ decimalPlace($markdistribution->mark_for_the_exam) }}</td>
					<td class='mark_weight'>{{ decimalPlace($markdistribution->mark_weight) }}</td>
					<td class='marks_to' style="text-align: center;">
						<span class="label {{ $markdistribution->is_active=='yes' ? 'label-info' : 'label-danger' }}">{{ ucwords($markdistribution->is_active) }}</span>
					</td>
					<td>
						<a href="{{action('MarkDistributionController@edit', $markdistribution['id'])}}" data-title="{{ ('Update Mark Distribution') }}" class="btn btn-warning btn-sm">{{ ('Edit') }}</a>
						<a href="{{action('MarkDistributionController@show', $markdistribution['id'])}}" data-title="{{ ('View Mark Distribution') }}" class="btn btn-info btn-sm ajax-modal">{{ ('View') }}</a>
					</td>
			   </tr>
			  @endforeach
			</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


