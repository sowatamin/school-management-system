@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="panel-title">Parents List</span>
				<a href="{{ route('parents.create') }}" class="btn btn-info btn-sm pull-right">Add New Parent</a>
			</div>
			<div class="panel-body">
				<table class="table table-bordered data-table">
					<thead>
						<th>Profile</th>
						<th>Name</th>
						<th>Student</th>
						<th>Email</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($parents as $data)
						<tr>
							<td><img class="img-circle" src="{{ asset('public/uploads/images/'.$data->image) }}" width="50px" alt=""></td>
							<td>{{ $data->name }}</td>
							<td>
								<li class="dropdown" style="display: inline;">
									<a href="#" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-list-alt"></i>Children Name
									</a>
									<ul class="dropdown-menu">
										{{ get_students_list('students' ,$data->parent_id) }}
									</ul>
								</li>
							</td>
							<td>{{ $data->email }}</td>
							<td>
								<form action="{{route('parents.destroy',$data->parent_id)}}" method="post">
									<a href="{{route('parents.show',$data->parent_id)}}" class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>
								    <a href="{{route('parents.edit',$data->parent_id)}}" class="btn btn-warning btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
									{{ method_field('DELETE') }}
    								@csrf
    								<button type="submit" class="btn btn-danger btn-xs btn-remove"><i class="fa fa-eraser" aria-hidden="true"></i></button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
