<form action="{{route('parents.store')}}" class="ajax-submit" enctype="multipart/form-data" method="post"
      accept-charset="utf-8">
    @csrf
    <div class="col-sm-12">
        <div class="form-group">
            <label class="control-label">{{ ('Parent Name')}}</label>
            <input type="text" class="form-control" name="parent_name" value="{{ old('parent_name') }}" required>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ("Father's Name")}}</label>
            <input type="text" class="form-control" name="f_name" value="{{ old('f_name') }}" required>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ("Mother's Name")}}</label>
            <input type="text" class="form-control" name="m_name" value="{{ old('m_name') }}" required>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ("Father's Profession")}}</label>
            <input type="text" class="form-control" name="f_profession" value="{{ old('f_profession') }}">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ("Mother's Profession")}}</label>
            <input type="text" class="form-control" name="m_profession" value="{{ old('m_profession') }}">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ('Phone')}}</label>
            <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ('Address')}}</label>
            <textarea class="form-control" name="address">{{ old('address') }}</textarea>
        </div>
    </div>


    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ('Email')}}</label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Parent NID Number</label>
            <input type="number" class="form-control" name="nid_number" value="{{ old('nid_number') }}" required>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Parent NID</label>
            <input type="file" class="form-control nid_copy" name="nid_copy" value="" required>
            <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ('Password')}}</label>
            <input type="password" class="form-control" name="password" required>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ('Confirm Password')}}</label>
            <input type="password" class="form-control" name="password_confirmation" required>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ('Profile Picture')}}</label>
            <input type="file" class="form-control dropify" name="image"
                   data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-info btn-block">Save Parent</button>
        </div>
    </div>
</form>
@section('js-script')
    <script type="text/javascript">

        $(document).ready(function () {
            var maxSize = '1024';
            var _validFileExtensions = [];
            var imgExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG"];
            var fileExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG", ".pdf", ".PDF"];

            function validateSingleInput(oInput) {
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        if (oInput.className.match(/\bonlyImg\b/)) {
                            _validFileExtensions = imgExtensions;
                        } else {
                            _validFileExtensions = fileExtensions;
                        }
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }

            function fileSizeValidate(fdata) {
                if (fdata.files && fdata.files[0]) {
                    var fsize = fdata.files[0].size / 1024;
                    if (fsize > maxSize) {
                        fdata.value = "";
                        return false;
                    } else {
                        return true;
                    }
                }
            }


            //                For teacher certificate

            $(document).on("change", ".nid_copy", function () {
                var noExtentionError = validateSingleInput(this);
                var noSizeError = fileSizeValidate(this);
                if (noExtentionError === false) {
                    $(this).siblings(".oError").show().text("The nid copy must be JPG, JPEG, PNG, PDF format.");
                    return false;
                } else {
                    $(this).siblings(".oError").hide();
                    if (noSizeError === false) {
                        $(this).siblings(".oError").show().text("The nid copy will be less than 1 MB. ");
                        return false;
                    } else {
                        $(this).siblings(".oError").hide();
                    }
                }
            });

        });

    </script>
@stop
