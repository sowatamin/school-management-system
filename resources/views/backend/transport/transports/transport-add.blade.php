@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title" >
					<i class="entypo-plus-circled"></i>{{ ('Add New Transport')}}
				</div>
			</div>
			<div class="panel-body">
			  <div class="col-md-8">
				<form action="{{route('transports.store')}}" class="form-horizontal validate" method="POST">
					@csrf
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Road Name')}}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="road_name" value="{{ old('road_name') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">{{ ('Branch')}}</label>
						<div class="col-md-9">
							<select name="branch" class="form-control select2" required>
								<option value="">{{  ('Select One') }}</option>
								{{ get_branch_list('branches','id','branch_name',old('branch')) }}
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Vehicle Serial No')}}</label>
						<div class="col-sm-9">
							<select class="form-control select2" name="vehicle_id" required>
								<option value="">{{ ('Select One') }}</option>
								{{--{{ create_option('transport_vehicles','id','serial_number',old('vehicle_id')) }}--}}
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{!!  ('Road Fare')." <b>".get_option('currency_symbol')."</b>" !!}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="road_fare" value="{{ old('road_fare') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Note')}}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="note" value="{{ old('note') }}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info">{{  ('Add Transport') }}</button>
						</div>
					</div>
				</form>
			   </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js-script')
	<script type="text/javascript">
		$('document').ready(function(){
			$('select[name=branch]').on('change',function(){
				var _token=$('input[name=_token]').val();
				var branch = $('select[name=branch]').val();
				$.ajax({
					type: "POST",
					url: "{{url('transports/vehicle')}}",
					data:{_token:_token, branch:branch},
					success: function(data){
						$('select[name=vehicle_id]').html(data);
					}
				});
			});
		});
	</script>
@stop
