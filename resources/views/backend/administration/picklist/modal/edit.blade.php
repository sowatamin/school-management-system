<form method="post" class="ajax-submit" autocomplete="off" action="{{action('PicklistController@update', $id)}}" enctype="multipart/form-data">
	{{ csrf_field()}}
	<input name="_method" type="hidden" value="PATCH">

	<div class="col-md-12">
	  <div class="form-group">
			<label class="control-label">{{ ('Type') }}</label>
			<select name="type" id="type" class="form-control select2" required>
			    <option value="">{{ ('Select Type') }}</option>
				<option>{{ ('Religion') }}</option>
				<option>{{ ('Designation') }}</option>
				<option>{{ ('Source') }}</option>
				<option>{{ ('Reference') }}</option>
				<option>{{ ('Purpose') }}</option>
				<option>{{ ('Complain') }}</option>
			</select>
	   </div>
	</div>

	<div class="col-md-12">
	 <div class="form-group">
		<label class="control-label">{{ ('Value') }}</label>
		<input type="text" class="form-control" name="value" value="{{$picklist->value}}" required>
	 </div>
	</div>


	<div class="form-group">
	  <div class="col-md-12">
		<button type="submit" class="btn btn-primary">{{ ('Update') }}</button>
	  </div>
	</div>
</form>

<script>
$("#type").val("{{$picklist->type}}");
</script>
