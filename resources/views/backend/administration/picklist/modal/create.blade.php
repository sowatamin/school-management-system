<form method="post" class="ajax-submit" autocomplete="off" action="{{route('picklists.store')}}" enctype="multipart/form-data">
	{{ csrf_field() }}

	<div class="col-md-12">
	  <div class="form-group">
			<label class="control-label">{{ ('Type') }}</label>
			<select name="type" class="form-control select2" required>
			    <option value="">{{ ('Select Type') }}</option>
				<option>{{ ('Religion') }}</option>
				<option>{{ ('Designation') }}</option>
				<option>{{ ('Source') }}</option>
				<option>{{ ('Reference') }}</option>
				<option>{{ ('Purpose') }}</option>
				<option>{{ ('Complain') }}</option>
			</select>
	   </div>
	</div>

	<div class="col-md-12">
	  <div class="form-group">
		<label class="control-label">{{ ('Value') }}</label>
		<input type="text" class="form-control" name="value" value="{{ old('value') }}" required>
	  </div>
	</div>


	<div class="col-md-12">
	  <div class="form-group">
	    <button type="reset" class="btn btn-danger">{{ ('Reset') }}</button>
		<button type="submit" class="btn btn-primary">{{ ('Save') }}</button>
	  </div>
	</div>
</form>
