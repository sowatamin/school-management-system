@extends('layouts.backend')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading">{{ ('Add Student Group') }}</div>

	<div class="panel-body">
	  <form method="post" class="validate" autocomplete="off" action="{{url('student_groups')}}" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="col-md-12">
		  <div class="form-group">
			<label class="control-label">{{ ('Group Name') }}</label>
			<input type="text" class="form-control" name="group_name" value="{{ old('group_name') }}" required>
		  </div>
		</div>


		<div class="form-group">
		  <div class="col-md-12">
		    <button type="reset" class="btn btn-danger">{{ ('Reset') }}</button>
			<button type="submit" class="btn btn-primary">{{ ('Save') }}</button>
		  </div>
		</div>
	  </form>
	</div>
  </div>
 </div>
</div>
@endsection


