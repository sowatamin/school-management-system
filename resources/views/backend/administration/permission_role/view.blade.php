@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ ('View Permission Role') }}</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <td>{{ ('Role Name') }}</td>
                            <td>{{ $role->role_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ ('Note') }}</td>
                            <td>{{ $role->note }}</td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection


