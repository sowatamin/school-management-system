<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-bordered">
            @if(!empty($branches))
                @foreach($branches as $branch)
                    <tr>
                        <td>{{ ('Branch Name') }}</td>
                        <td>{{ $branch->branch_name }}</td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
</div>
