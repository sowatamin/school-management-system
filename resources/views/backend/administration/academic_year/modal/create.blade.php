<form method="post" class="ajax-submit" autocomplete="off" action="{{route('academic_years.store')}}" enctype="multipart/form-data">
	{{ csrf_field() }}

	@if (empty(check_branch('branches')))
		<div class="col-md-12">
			<div class="form-group">
				<div class="alert alert-warning">
					<strong>Warning!</strong> <span style="color: #1d2124">Before Add Academic Year. Must <a href="{{route('branches.index')}}">Add Branch</a></span>
				</div>
			</div>
		</div>
	@endif

	<div class="col-md-12">
	  <div class="form-group">
		<label class="control-label">{{ ('Session Name') }}</label>
		<input type="text" class="form-control" name="session" value="{{ old('session') }}" required>
	  </div>
	</div>

	<div class="col-md-12">
	  <div class="form-group">
		<label class="control-label">{{ ('Academic Year') }}</label>
		<input type="text" class="form-control year" name="year" value="{{ old('year') }}" required>
	  </div>
	</div>

	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label">{{ ('Academic Branch') }}</label>
			<select multiple class="form-control" name="branch_id[]" required>
				@if(!empty($branches))
					@foreach($branches AS $branch)
						<option value="{{ $branch->id }}"> {{ $branch->branch_name }} </option>
					@endforeach
				@endif
			</select>
		</div>
	</div>


	<div class="col-md-12">
	  <div class="form-group">
	    <button type="reset" class="btn btn-danger">{{ ('Reset') }}</button>
		<button type="submit" class="btn btn-primary">{{ ('Save') }}</button>
	  </div>
	</div>
</form>
