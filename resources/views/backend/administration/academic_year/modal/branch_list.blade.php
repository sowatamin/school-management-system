<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-bordered">
            @if(!empty($branches))
                <tr>
                    <td style="font-weight: bold">{{ ('Branch Name') }}</td>
                </tr>
                @foreach($branches as $branch)
                    <tr>
                        <td>{{ $branch->branch_name }}</td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
</div>
