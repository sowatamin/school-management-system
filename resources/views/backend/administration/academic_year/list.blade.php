@extends('layouts.backend')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading"><span class="panel-title">{{ ('List Academic Session') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ ('Add Academic Year') }}" href="{{route('academic_years.create')}}">{{ ('Add New') }}</a>
			</div>

			<div class="panel-body">
			  <table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ ('Session Name') }}</th>
					<th>{{ ('Academic Year') }}</th>
					<th>{{ ('Branch Name') }}</th>
					<th>{{ ('Action') }}</th>
				  </tr>
				</thead>
				<tbody>
			    @php $current = get_option('academic_year'); @endphp
			    @foreach($academicyears as $academicyear)
				  <tr id="row_{{ $academicyear->id }}">
					<td class='session'>{{ $academicyear->session }} {{ $academicyear->id==$current ? "(Active)" : "" }}</td>
					<td class='year'>{{ $academicyear->year }}</td>
					<td class='branch_name'>
						<a href="{{action('AcademicYearController@branch_list', $academicyear['id'])}}" data-title="{{ ("Academic($academicyear->year) Branch List") }}" class="btn btn-primary btn-sm ajax-modal">{{ ('View Branch') }}</a>
					</td>

					<td>
					  <form action="{{action('AcademicYearController@destroy', $academicyear['id'])}}" method="post">
						<a href="{{action('AcademicYearController@edit', $academicyear['id'])}}" data-title="{{ ('Update Academic Year') }}" class="btn btn-warning btn-sm ajax-modal">{{ ('Edit') }}</a>
						{{ csrf_field() }}
						<input name="_method" type="hidden" value="DELETE">
						<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ ('Delete') }}</button>
					  </form>
					</td>
				  </tr>
			    @endforeach
				</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>
@endsection
