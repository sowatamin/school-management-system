@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">{{ ('Add Academic Branch') }}</div>

                <div class="panel-body">
                    <form method="post" class="validate" autocomplete="off" action="{{url('branches')}}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">{{ ('Branch Name') }}</label>
                                <input type="text" class="form-control" name="branch_name"
                                       value="{{ old('branch_name') }}" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">{{ ('Branch Address') }}</label>
                                <textarea class="form-control" name="branch_address" required
                                          rows="4">{{ old('branch_address') }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">{{ ('Branch Phone') }}</label>
                                <input type="number" class="form-control" name="branch_phone"
                                       value="{{ old('branch_phone') }}" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="reset" class="btn btn-danger">{{ ('Reset') }}</button>
                                <button type="submit" class="btn btn-primary">{{ ('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


