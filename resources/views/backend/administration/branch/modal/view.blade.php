<div class="panel panel-default">
<div class="panel-body">
  <table class="table table-bordered">
	 <tr><td>{{ ('Name') }}</td><td>{{ $branch->branch_name }}</td></tr>
	 <tr><td>{{ ('Address') }}</td><td>{{ $branch->branch_address }}</td></tr>
	 <tr><td>{{ ('Phone') }}</td><td>{{ $branch->branch_phone }}</td></tr>
  </table>
</div>
</div>
