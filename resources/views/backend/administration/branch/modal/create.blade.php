<form method="post" class="ajax-submit" autocomplete="off" action="{{route('branches.store')}}" enctype="multipart/form-data">
	{{ csrf_field() }}

	<div class="col-md-12">
	  <div class="form-group">
		<label class="control-label">{{ ('Branch Name') }}</label>
		<input type="text" class="form-control" name="branch_name" value="{{ old('branch_name') }}" required>
	  </div>
	</div>

	<div class="col-md-12">
	  <div class="form-group">
		<label class="control-label">{{ ('Branch Address') }}</label>
		  <textarea  class="form-control" name="branch_address" required rows="4">{{ old('branch_address') }}</textarea>
	  </div>
	</div>

	<div class="col-md-12">
	  <div class="form-group">
		<label class="control-label">{{ ('Branch Phone') }}</label>
		<input type="number" class="form-control" name="branch_phone" value="{{ old('branch_phone') }}" required>
	  </div>
	</div>


	<div class="col-md-12">
	  <div class="form-group">
	    <button type="reset" class="btn btn-danger">{{ ('Reset') }}</button>
		<button type="submit" class="btn btn-primary">{{ ('Save') }}</button>
	  </div>
	</div>
</form>
