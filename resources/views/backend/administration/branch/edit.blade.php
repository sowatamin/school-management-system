@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-default">
                <div class="panel-heading">{{ ('Update Academic Branch') }}</div>

                <div class="panel-body">
				<form method="post" class="validate" autocomplete="off" action="{{action('BranchController@update', $id)}}" enctype="multipart/form-data">
					{{ csrf_field()}}
					<input name="_method" type="hidden" value="PATCH">

					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">{{ ('Branch Name') }}</label>
							<input type="text" class="form-control" name="branch_name"
								   value="{{ $branch->branch_name }}" required>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">{{ ('Branch Address') }}</label>
							<textarea class="form-control" name="branch_address" required
									  rows="4">{{ $branch->branch_address }}</textarea>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">{{ ('Branch Phone') }}</label>
							<input type="number" class="form-control" name="branch_phone"
								   value="{{ $branch->branch_phone }}" required>
						</div>
					</div>

					<div class="form-group">
					  <div class="col-md-12">
						<button type="submit" class="btn btn-primary">{{ ('Update') }}</button>
					  </div>
					</div>
				</form>
                </div>
            </div>
        </div>
    </div>
@endsection


