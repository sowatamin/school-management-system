@extends('layouts.backend')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading"><span class="panel-title">{{ ('List Academic Branch') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ ('Add Academic Branch') }}" href="{{route('branches.create')}}">{{ ('Add New') }}</a>
			</div>

			<div class="panel-body">
			  <table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ ('Branch Name') }}</th>
					<th>{{ ('Branch Address') }}</th>
					<th>{{ ('Branch Phone') }}</th>
					<th>{{ ('Action') }}</th>
				  </tr>
				</thead>
				<tbody>
			    @foreach($branches as $branch)
				  <tr id="row_{{ $branch->id }}">
					<td class='branch_name'>{{ $branch->branch_name }}</td>
					<td class='branch_address'>{{ $branch->branch_address }}</td>
					<td class='branch_phone'>{{ $branch->branch_phone }}</td>
					<td>
					  {{--<form action="{{action('BranchController@destroy', $branch['id'])}}" method="post">--}}
						<a href="{{action('BranchController@edit', $branch['id'])}}" data-title="{{ ('Update Academic Branch') }}" class="btn btn-warning btn-sm ajax-modal">{{ ('Edit') }}</a>
						{{--{{ csrf_field() }}--}}
						{{--<input name="_method" type="hidden" value="DELETE">--}}
						{{--<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ ('Delete') }}</button>--}}
					  {{--</form>--}}
					</td>
				  </tr>
			    @endforeach
				</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>
@endsection
