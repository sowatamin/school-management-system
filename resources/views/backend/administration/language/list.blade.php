@extends('layouts.backend')

@section('content')

<div class="row">
	<div class="col-md-8">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ ('Languages') }}</span>
			<a class="btn btn-primary btn-sm pull-right" href="{{ route('languages.create') }}">{{ ('Add New') }}</a>
			</div>

			<div class="panel-body">
			 <table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ ('Language Name') }}</th>
					<th>{{ ('Edit Translation') }}</th>
					<th>{{ ('Remove') }}</th>
				  </tr>
				</thead>
				<tbody>

				  @foreach(getuage_list() as $language)
				  <tr>
					<td class='role_name'>{{ ucwords($language) }}</td>
					<td>
						<a href="{{ action('LanguageController@edit', $language) }}" class="btn btn-info btn-sm">{{ ('Edit Translation') }}</a>
					</td>
					<td>
					    <form action="{{action('LanguageController@destroy', $language)}}" method="post">
						   {{ csrf_field() }}
						   <input name="_method" type="hidden" value="DELETE">
						   <button class="btn btn-danger btn-sm btn-remove" type="submit">{{ ('Delete') }}</button>
						</form>
					</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>

@endsection


