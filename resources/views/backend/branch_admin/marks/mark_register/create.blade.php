@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-heading">
				<span class="panel-title" >
					{{('Mark Register')}}
				</span>
			</div>
			<div class="panel-body">
				<form id="search_form" class="params-panel validate" action="{{ url('branch_admin_marks/create') }}" method="post" autocomplete="off" accept-charset="utf-8">
					@csrf
					<div class="col-md-10">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Class</label>
								<select name="class_id" class="form-control select2" onChange="getData();" required>
									<option value="">Select One</option>
									{{ create_admin_branch_class_option('classes','id','class_name',$class_id) }}
								</select>
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Section</label>
								<select name="section_id" class="form-control select2" required>
									<option value="">Select One</option>
									{{ create_option('sections','id','section_name',$section_id, array("class_id = "=>$class_id)) }}
								</select>
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Subject</label>
								<select name="subject_id" id="subject_id" class="form-control select2" required>
									<option value="">Select One</option>
									{{ create_option('subjects','id','subject_name',$subject_id, array("class_id = "=>$class_id)) }}
								</select>
							</div>
						</div>
                    </div>

					<div class="col-md-2">
						<div class="form-group">
							<button type="submit" style="margin-top:24px;" class="btn btn-primary btn-block rect-btn">Marks</button>
						</div>
					</div>
				</form>

				@if( !empty($marks) )
					<div class="col-md-12">
						<div class="panel-heading text-center">
							<div class="panel-title" >
								{{ ('Mark Details for Class') }} {{ get_class_name($class_id) }}<br>
								{{ ('Section').": ".get_section_name($section_id) }} <br>
								{{ ('Subject').": ".get_subject_name($subject_id) }} <br>
							</div>
						</div>
						<form method="post" action="{{ url('branch_admin_marks/store') }}" class="appsvan-submit" autocomplete="off" accept-charset="utf-8">
							@csrf
							<table class="table table-bordered">
								<thead>
								<th>{{ ('Name') }}</th>
								<th>{{ ('Roll') }}</th>
								@foreach($mark_destributions as $md)
									<th>{{ $md->mark_distribution_type }} ({{ $md->mark_for_the_exam }})</th>
								@endforeach
								</thead>
								<tbody>
								@foreach($marks AS $key => $data)
									<tr>
										<td>{{ $data->first_name." ".$data->last_name }}</td>
										<td>{{ $data->roll }}</td>
										@if( empty($mark_details) )
											@foreach($mark_destributions as $md)
												<td><input type="text" class="form-control float-field" onkeyup="myFunction({{ $md->mark_for_the_exam }}, event, {{ $md->mark_value }})" name="marks[{{ $md->mark_distributions_id }}][{{ $data->student_id }}]" value=""></td>
											@endforeach
										@else
											@foreach($mark_details[$data->mark_id] as $md)
												<td><input type="text" class="form-control float-field" onkeyup="myFunction({{ $md->mark_for_the_exam }}, event, {{ $md->mark_value }})" name="marks[{{ $md->mark_distributions_id }}][{{ $data->student_id }}]" value="{{ $md->mark_value }}"></td>
											@endforeach
										@endif

										<input type="hidden" name="student_id[]" value="{{ $data->student_id }}">
										<input type="hidden" name="subject_id[]" value="{{ $subject_id  }}">
										<input type="hidden" name="class_id[]" value="{{ $data->class_id }}">
										<input type="hidden" name="section_id[]" value="{{ $data->section_id }}">
										<input type="hidden" name="mark_id[]" value="{{ $data->mark_id }}">
									</tr>
								@endforeach

								<tr>
									<td colspan="100"><button type="submit" class="btn btn-primary pull-right rect-btn">{{('Save Marks')}}</button></td>
								</tr>
								</tbody>
							</table>
						</form>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection

@section('js-script')
<script type="text/javascript">

	function myFunction(mark_for_the_exam, event, mark_value) {
		let input_value = event.target.value;

		if (input_value > mark_for_the_exam) {
			alert('Given mark can not be grater then Exam mark');

			if (mark_value != null) {
				event.target.value = mark_value;
			} else {
				event.target.value = "";
			}
		}
		return true;
	}

	function getData() {
		var _token=$('input[name=_token]').val();
		var class_id=$('select[name=class_id]').val();
		$.ajax({
			type: "POST",
			url: "{{url('branch_admin_sections/section')}}",
			data:{_token:_token,class_id:class_id},
			beforeSend: function(){
			    $("#preloader").css("display","block");
			},
			success: function(data){
				$("#preloader").css("display","none");
				$('select[name=section_id]').html(data);
			}
		});
		getSubject();
	}


	function getSubject() {
		var _token = $('input[name=_token]').val();
		var class_id = $('select[name=class_id]').val();
		$.ajax({
			type: "POST",
			url: "{{url('branch_admin_exams/get_subject')}}",
			data:{_token:_token, class_id:class_id},
			beforeSend: function(){
			    $("#preloader").css("display","block");
			},
			success: function(data){
				$("#preloader").css("display","none");
				$('select[name=subject_id]').html(data);
			}
		});
	}


</script>
@stop
