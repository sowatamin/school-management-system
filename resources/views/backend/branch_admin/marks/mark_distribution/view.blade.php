@extends('layouts.backend')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading">View Mark Distribution</div>

	<div class="panel-body">
	  <table class="table table-bordered">
		    <tr><td>Mark Distribution Type</td><td>{{ $markdistribution->mark_distribution_type }}</td></tr>
			<tr><td>Mark For The Exam</td><td>{{ $markdistribution->mark_for_the_exam }}</td></tr>
			<tr><td>Mark Weight</td><td>{{ $markdistribution->mark_weight }}</td></tr>
	  </table>
	</div>
  </div>
 </div>
</div>
@endsection


