<div class="panel panel-default">
<div class="panel-body">
  <table class="table table-bordered">
	 <tr><td>Mark Distribution Type</td><td>{{ $markdistribution->mark_distribution_type }}</td></tr>
	 <tr><td>Mark For The Exam</td><td>{{ $markdistribution->mark_for_the_exam }}</td></tr>
	 <tr><td>Mark Weight</td><td>{{ $markdistribution->mark_weight }}</td></tr>
	 <tr><td>Active</td><td>{{ $markdistribution->is_active }}</td></tr>
  </table>
</div>
</div>
