<form method="post" class="ajax-submit" autocomplete="off" action="{{route('branch_admin_mark_distributions.store')}}"
      enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ('Mark Distribution Type') }}</label>
            <input type="text" class="form-control" name="mark_distribution_type"
                   value="{{ old('mark_distribution_type') }}" required>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Mark For The Exam</label>
            <input type="text" class="form-control float-field" name="mark_for_the_exam"
                   value="{{ old('mark_for_the_exam') }}" required>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Mark Weight</label>
            <input type="text" class="form-control float-field" name="mark_weight"
                   value="{{ old('mark_weight') }}" required>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">{{ ('Active') }}</label>
            <select class="form-control" name="is_active" required>
                <option value="yes">Yes</option>
                <option value="no">No</option>
            </select>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <button type="reset" class="btn btn-danger">{{ ('Reset') }}</button>
            <button type="submit" class="btn btn-primary">{{ ('Save') }}</button>
        </div>
    </div>
</form>
