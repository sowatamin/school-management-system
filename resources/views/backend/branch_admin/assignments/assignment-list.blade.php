@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="panel-title">Assignments List</span>
				<select id="class" class="select_class pull-right" onchange="showClass(this);">
					<option value="">Select Class</option>
					{{ create_admin_branch_class_option('classes','id','class_name',$class) }}
				</select>
				<a href="{{route('branch_admin_assignments.create')}}"
				   class="btn btn-primary btn-sm pull-right">{{('Add New Assignment')}}</a>
			</div>

			<div class="panel-body content no-export">
				<table class="table table-bordered data-table">
					<thead>
					<th>Title</th>
					<th>Description</th>
					<th>Class</th>
					<th>Section</th>
					<th>Subject</th>
					<th style="width: 155px;">Action</th>
					</thead>
					<tbody>
					@foreach($assignments AS $data)
						<tr>
							<td>{{$data->title}}</td>
							<td>{{substr(strip_tags($data->description),0,100)}}...</td>
							<td>{{$data->class_name}}</td>
							<td>{{$data->section_name}}</td>
							<td>{{$data->subject_name}}</td>
							<td>
								<form action="{{route('branch_admin_assignments.destroy',$data->id)}}" method="post">
									@if($data->file)
										<li class="dropdown" style="display: inline;">
											<a href="#" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown">
												<i class="fa fa-download"></i>
											</a>
											<ul class="dropdown-menu">
												@if($data->file)
													<li><a href="{{ asset('public/uploads/files/assignments/'.$data->file) }}">{{ ('File 1') }}</a></li>
												@endif
												@if($data->file_2)
													<li><a href="{{ asset('public/uploads/files/assignments/'.$data->file_2) }}">{{ ('File 2') }}</a></li>
												@endif
												@if($data->file_3)
													<li><a href="{{ asset('public/uploads/files/assignments/'.$data->file_3) }}">{{ ('File 3') }}</a></li>
												@endif
												@if($data->file_4)
													<li><a href="{{ asset('public/uploads/files/assignments/'.$data->file_4) }}">{{ ('File 4') }}</a></li>
												@endif
											</ul>
										</li>
									@endif
									<a href="{{route('branch_admin_assignments.show',$data->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>
									<a href="{{route('branch_admin_assignments.edit',$data->id)}}" class="btn btn-warning btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
									{{ method_field('DELETE') }}
									@csrf
									<button type="submit" class="btn btn-danger btn-xs btn-remove"><i class="fa fa-eraser" aria-hidden="true"></i></button>
								</form>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>
@endsection

@section('js-script')
	<script>
		function showClass(elem) {
			if ($(elem).val() == "") {
				return;
			}
			window.location = "<?php echo url('branch_admin_assignments/class') ?>/" + $(elem).val();
		}
	</script>
@stop

