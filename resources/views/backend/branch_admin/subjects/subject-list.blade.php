@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-heading">
				<span class="panel-title" >
					{{('Subjects List')}}
				</span>
				<select id="class" class="select_class pull-right" onchange="showClass(this);">
				   <option value="">Select Class</option>
				   {{ create_admin_branch_class_option('classes','id','class_name',$class) }}
				</select>

				<a class="btn btn-primary btn-sm pull-right" data-title="Add Grade" href="{{ url('branch_admin_subjects/create') }}">{{ ('Add New Subject') }}</a>
			</div>
			<div class="panel-body no-export">
				<table class="table table-bordered data-table">
					<thead>
						<th>{{ ('Subject Name') }}</th>
						<th>{{ ('Subject Code') }}</th>
						<th>{{ ('Class') }}</th>
						<th>{{ ('Full mark') }}</th>
						<th>{{ ('Pass mark') }}</th>
						<th>{{ ('Action') }}</th>
					</thead>
					<tbody>
						@foreach($subjects AS $data)
						<tr>
							<td>{{ $data->subject_name }}</td>
							<td>{{ $data->subject_code }}</td>
							<td>{{ $data->class_name }}</td>
							<td>{{ $data->full_mark }}</td>
							<td>{{ $data->pass_mark }}</td>
							<td>
								<form action="{{route('branch_admin_subjects.destroy',$data->id)}}" method="post">
									<a href="{{route('branch_admin_subjects.edit',$data->id)}}" class="btn btn-warning btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
									{{ method_field('DELETE') }}
									@csrf
									<button type="submit" class="btn btn-danger btn-xs btn-remove"><i class="fa fa-eraser" aria-hidden="true"></i></button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js-script')
<script>
function showClass(elem){
	if($(elem).val() == ""){
		return;
	}
	window.location = "<?php echo url('branch_admin_subjects/class') ?>/"+$(elem).val();
}
</script>
@stop
