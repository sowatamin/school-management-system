@extends('layouts.backend')
@section('content')
    <div class="row">
        <strong class="panel-title" style="display: none;">{{ ('Teacher Details') }}</strong>
        <div class="col-md-4">
            <div class="card card-user">
                <div class="image">
                    <img src="{{ asset('public/uploads/images/background/background.jpg') }}" alt="...">
                </div>
                <div class="content">
                    <div class="author">
                        <img class="avatar border-white" src="{{ asset('public/uploads/images/'.$teacher->image) }}"
                             alt="{{ $teacher->name }}">
                        <h4 class="title">{{ $teacher->name }}</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <ul class="nav nav-tabs setting-tab">
                <li class="active">
                    <a data-toggle="tab" href="#profile" aria-expanded="true">{{ ('Profile') }}</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#national_id" aria-expanded="false">{{ ('National ID') }}</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#student_certificate" aria-expanded="false">{{ ('Certificate') }}</a>
                </li>
                <li class="pull-right">
                    <div style="padding: 7px;">
                        <a href="{{ url('branch_admin_teachers/certificate',$teacher->id) }}"
                           class="btn btn-primary rect-btn btn-sm"><i
                                    class="fa fa-upload" aria-hidden="true"></i>{{ ('Upload Certificate') }}</a>
                    </div>
                </li>
                <li class="pull-right">
                    <div style="padding: 7px;">
                        <a href="{{ route('branch_admin_teachers.edit',$teacher->id) }}"
                           class="btn btn-primary rect-btn btn-sm"><i
                                    class="fa fa-pencil" aria-hidden="true"></i>{{ ('Edit') }}</a>
                    </div>
                </li>
            </ul>
            <div class="tab-content">
                <div id="profile" class="tab-pane fade in active">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table table-custom" width="100%">
                                <tbody>
                                <tr>
                                    <td>{{ ('Name') }}</td>
                                    <td>{{ $teacher->name }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Designation') }}</td>
                                    <td>{{ ($teacher->designation) }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Date Of Birth') }}</td>
                                    <td>{{ $teacher->birthday  }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Gender') }}</td>
                                    <td>{{ $teacher->gender }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Religion') }}</td>
                                    <td>{{ $teacher->religion }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Joining Date') }}</td>
                                    <td>{{ $teacher->joining_date }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Address') }}</td>
                                    <td>{{ $teacher->address }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Phone') }}</td>
                                    <td>{{ $teacher->phone }}</td>
                                </tr>
                                <tr>
                                    <td>{{ ('Email') }}</td>
                                    <td>{{ $teacher->email }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="national_id" class="tab-pane fade">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @if(($teacher['nid_copy']) != NULL)
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <table class="table table-custom" width="100%">
                                            <tbody>
                                            <tr>
                                                <td>National ID Card Number</td>
                                                <td>{{ $teacher->nid_number }}</td>
                                            </tr>
                                            <tr>
                                                <td>National ID Copy</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    @php
                                        $strExt = explode('.', $teacher->nid_copy);
                                    @endphp
                                    <div class="col-md-4">
                                        <div class="well well-sm">
                                            <div class="">

                                                @if($strExt[1] === 'pdf')
                                                    <img style="height: 175px; width: 100%"
                                                         class="thumbnail img-responsive"
                                                         src="{{ asset('public/uploads/images/pdf.png') }}"/>
                                                @else
                                                    <img style="height: 175px; width: 100%"
                                                         class="thumbnail img-responsive"
                                                         src="{{ asset('public/uploads/images/nid/'.$teacher->nid_copy) }}"/>
                                                @endif
                                                <a download class="btn btn-primary btn-xl"
                                                   href="{{ asset('public/uploads/images/nid/'.$teacher->nid_copy) }}">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                                <a class="btn btn-info btn-xl" data-fancybox="filter"
                                                   data-caption="student_certificate" target="_blank"
                                                   href="{{ asset('public/uploads/images/nid/'.$teacher->nid_copy) }}">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-left: 10px;">
                                    <div style="padding: 7px;">
                                        <a href="{{ url('branch_admin_teachers/nid',$teacher->id) }}"
                                           class="btn btn-primary rect-btn btn-sm"><i
                                                    class="fa fa-upload" aria-hidden="true"></i>{{ ('Update NID') }}</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div id="student_certificate" class="tab-pane fade">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @foreach($certificates as $certificate)
                                @if(($certificate['certificate']) != NULL)
                                    @php
                                        $strExt = explode('.', $certificate->certificate);
                                    @endphp
                                    <div class="col-md-4">
                                        <div class="well well-sm">
                                            <div class="">
                                                @if($strExt[1] === 'pdf')
                                                    <img style="height: 175px; width: 100%"
                                                         class="thumbnail img-responsive"
                                                         src="{{ asset('public/uploads/images/pdf.png') }}"/>
                                                @else
                                                    <img style="height: 175px; width: 100%"
                                                         class="thumbnail img-responsive"
                                                         src="{{ asset('public/uploads/images/teacher_certificate/'.$certificate->certificate) }}"/>
                                                @endif
                                                <a download class="btn btn-primary btn-xl"
                                                   href="{{ asset('public/uploads/images/teacher_certificate/'.$certificate->certificate) }}">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                                <a class="btn btn-info btn-xl" data-fancybox="filter"
                                                   data-caption="student_certificate" target="_blank"
                                                   href="{{ asset('public/uploads/images/teacher_certificate/'.$certificate->certificate) }}">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                                <a href="{{ action('Branch_Admin\TeacherController@certificate_view',$certificate['id']) }}"
                                                   class="btn btn-primary btn-xl">
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>

                                                <a href="{{ action('Branch_Admin\TeacherController@certificate_remark',$certificate['id']) }}"
                                                   data-title="Certificate Remark"
                                                   class="btn btn-info btn-xl ajax-modal">
                                                    <i class="fa fa-comments-o" aria-hidden="true"></i>
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-fancybox="filter"]').fancybox({
                buttons: [
                    'slideShow',
                    'fullScreen',
                    'thumbs',
                    'download',
                    'zoom',
                    'close'
                ]
            });
        });
    </script>
@stop
