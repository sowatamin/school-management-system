<form method="post" class="validate"  autocomplete="off" action="{{action('Branch_Admin\TeacherController@certificate_remark',$certificate_remarks['id'])}}" enctype="multipart/form-data">
    {{ csrf_field()}}

    {{--<input type="hidden" value="{{ $certificate_remarks->id }}" name="id">--}}

    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Certificate Remarks</label>
            <textarea rows="4" class="form-control" name="certificate_remark" required>{{ $certificate_remarks->certificate_remark }}</textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">{{ ('Save') }}</button>
        </div>
    </div>
</form>
