@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title" >
					<i class="entypo-plus-circled"></i>{{ ('Add New Parent')}}
				</div>
			</div>
			<div class="panel-body">
			  <div class="col-md-8">
				<form action="{{route('branch_admin_parents.store')}}" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Parent Name')}}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="parent_name" value="{{ old('parent_name') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ("Father's Name")}}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="f_name" value="{{ old('f_name') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ("Mother's Name")}}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="m_name" value="{{ old('m_name') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ("Father's Profession")}}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="f_profession" value="{{ old('f_profession') }}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ("Mother's Profession")}}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="m_profession" value="{{ old('m_profession') }}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Phone')}}</label>
						<div class="col-sm-9">
							<input type="number" class="form-control" name="phone" value="{{ old('phone') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Address')}}</label>
						<div class="col-sm-9">
							<textarea class="form-control" name="address">{{ old('address') }}</textarea>
						</div>
					</div>

					<hr>
					<div class="page-header">
						<h4>National ID</h4>
					</div>
					<div class="form-group">
						<label for="nid_number" class="col-sm-3 control-label">
							Parent NID Number
						</label>
						<div class="col-md-9">
							<input id="nid_number" type="number" class="form-control"
								   name="nid_number"  value="{{ old('nid_number') }}" style="height: auto;" required>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<label for="nid_copy" class="col-sm-3 control-label">
							Parent NID
						</label>
						<div class="col-md-9">
							<input id="nid_copy" type="file" class="form-control nid_copy"
								   name="nid_copy" value="" style="height: auto;" required>
							<p class="help-block oError" style="color: red;margin-bottom: 0"></p>
						</div>
						<div class="clearfix"></div>
					</div>

					<hr>
					<div class="page-header">
					  <h4>Login Details</h4>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Email')}}</label>
						<div class="col-sm-9">
							<input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Password')}}</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" name="password" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Confirm Password')}}</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" name="password_confirmation" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Profile Picture')}}</label>
						<div class="col-sm-9">
							<input type="file" class="form-control dropify" name="image" data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info">Add Parent</button>
						</div>
					</div>
				</form>
			   </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js-script')
	<script type="text/javascript">

		$(document).ready(function () {
			var maxSize = '1024';
			var _validFileExtensions = [];
			var imgExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG"];
			var fileExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG", ".pdf", ".PDF"];

			function validateSingleInput(oInput) {
				if (oInput.type == "file") {
					var sFileName = oInput.value;
					if (sFileName.length > 0) {
						if (oInput.className.match(/\bonlyImg\b/)) {
							_validFileExtensions = imgExtensions;
						} else {
							_validFileExtensions = fileExtensions;
						}
						var blnValid = false;
						for (var j = 0; j < _validFileExtensions.length; j++) {
							var sCurExtension = _validFileExtensions[j];
							if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
								blnValid = true;
								break;
							}
						}
						if (!blnValid) {
							oInput.value = "";
							return false;
						}
					}
				}
				return true;
			}

			function fileSizeValidate(fdata) {
				if (fdata.files && fdata.files[0]) {
					var fsize = fdata.files[0].size / 1024;
					if (fsize > maxSize) {
						fdata.value = "";
						return false;
					} else {
						return true;
					}
				}
			}


			//                For teacher certificate

			$(document).on("change", ".nid_copy", function () {
				var noExtentionError = validateSingleInput(this);
				var noSizeError = fileSizeValidate(this);
				if (noExtentionError === false) {
					$(this).siblings(".oError").show().text("The nid copy must be JPG, JPEG, PNG, PDF format.");
					return false;
				} else {
					$(this).siblings(".oError").hide();
					if (noSizeError === false) {
						$(this).siblings(".oError").show().text("The nid copy will be less than 1 MB. ");
						return false;
					} else {
						$(this).siblings(".oError").hide();
					}
				}
			});

		});

	</script>
@stop
