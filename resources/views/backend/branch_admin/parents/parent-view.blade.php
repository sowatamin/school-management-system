@extends('layouts.backend')
@section('content')
	<div class="row">
		<strong class="panel-title" style="display: none;">{{ ('Teacher Details') }}</strong>
		<div class="col-md-4">
			<div class="card card-user">
				<div class="image">
					<img src="{{ asset('public/uploads/images/background/background.jpg') }}" alt="...">
				</div>
				<div class="content">
					<div class="author">
						<img class="avatar border-white" src="{{ asset('public/uploads/images/'.$parent->image) }}"
							 alt="{{ $parent->name }}">
						<h4 class="title">{{ $parent->name }}</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<ul class="nav nav-tabs setting-tab">
				<li class="active">
					<a data-toggle="tab" href="#profile" aria-expanded="true">{{ ('Profile') }}</a>
				</li>
				<li>
					<a data-toggle="tab" href="#national_id" aria-expanded="false">{{ ('National ID') }}</a>
				</li>
				<li class="pull-right">
					<div style="padding: 7px;">
						<a href="{{ route('branch_admin_parents.edit',$parent->id) }}" class="btn btn-primary rect-btn btn-sm"><i
									class="fa fa-pencil" aria-hidden="true"></i>{{ ('Edit') }}</a>
					</div>
				</li>
			</ul>
			<div class="tab-content">
				<div id="profile" class="tab-pane fade in active">
					<div class="panel panel-default">
						<div class="panel-body">
							<table class="table table-custom" width="100%">
								<tbody>
								<tr>
									<td>{{ ("Guardian's Name ") }}</td>
									<td>{{ $parent->parent_name }}</td>
								</tr>
								@if($student['id'] != NULL)
									<tr>
										<td>{{ ("Student's Name ") }}</td>
										<td><a href="{{ route('branch_admin_students.show',$student->id) }}">{{ $student->first_name .' '.$student->last_name }}</a></td>
									</tr>
								@endif
								<tr>
									<td>{{ ("Father's Name ") }}</td>
									<td>{{ $parent->f_name  }}</td>
								</tr>
								<tr>
									<td>{{ ("Mother's Name ") }}</td>
									<td>{{ $parent->m_name }}</td>
								</tr>
								<tr>
									<td>{{ ("Father's Profession ") }}</td>
									<td>{{ $parent->f_profession }}</td>
								</tr>
								<tr>
									<td>{{ ("Mother's Profession ") }}</td>
									<td>{{ $parent->m_profession }}</td>
								</tr>
								<tr>
									<td>{{ ('Phone') }}</td>
									<td>{{ $parent->phone }}</td>
								</tr>
								<tr>
									<td>{{ ('Email') }}</td>
									<td>{{ $parent->email }}</td>
								</tr>
								<tr>
									<td>{{ ('Address') }}</td>
									<td>{{ $parent->address }}</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="national_id" class="tab-pane fade">
					<div class="panel panel-default">
						<div class="panel-body">
							@if(($parent['nid_copy']) != NULL)
								<div class="col-md-12">
									<div class="col-md-6">
										<table class="table table-custom" width="100%">
											<tbody>
											<tr>
												<td>National ID Card Number</td>
												<td>{{ $parent->nid_number }}</td>
											</tr>
											<tr>
												<td>National ID Copy</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-md-12">
									@php
										$strExt = explode('.', $parent->nid_copy);
									@endphp
									<div class="col-md-4">
										<div class="well well-sm">
											<div class="">

												@if($strExt[1] === 'pdf')
													<img style="height: 175px; width: 100%"
														 class="thumbnail img-responsive"
														 src="{{ asset('public/uploads/images/pdf.png') }}"/>
												@else
													<img style="height: 175px; width: 100%"
														 class="thumbnail img-responsive"
														 src="{{ asset('public/uploads/images/nid/'.$parent->nid_copy) }}"/>
												@endif
												<a download class="btn btn-primary btn-xl"
												   href="{{ asset('public/uploads/images/nid/'.$parent->nid_copy) }}">
													<i class="fa fa-download"></i>
												</a>
												<a class="btn btn-info btn-xl" data-fancybox="filter"
												   data-caption="student_certificate" target="_blank"
												   href="{{ asset('public/uploads/images/nid/'.$parent->nid_copy) }}">
													<i class="fa fa-eye"></i>
												</a>

											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12" style="margin-left: 10px;">
									<div style="padding: 7px;">
										<a href="{{ url('branch_admin_parents/nid',$parent->id) }}"
										   class="btn btn-primary rect-btn btn-sm"><i
													class="fa fa-upload" aria-hidden="true"></i>{{ ('Update NID') }}</a>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js-script')
	<script type="text/javascript">
		$(document).ready(function () {
			$('[data-fancybox="filter"]').fancybox({
				buttons: [
					'slideShow',
					'fullScreen',
					'thumbs',
					'download',
					'zoom',
					'close'
				]
			});
		});
	</script>
@stop
