@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title" >
					<i class="entypo-plus-circled"></i>{{('Edit Transport')}}
				</div>
			</div>
			<div class="panel-body">
			  <div class="col-md-8">
				<form action="{{route('branch_admin_transports.update',$transport->id)}}" class="form-horizontal validate" method="POST">
					@csrf
					{{ method_field('PATCH') }}
					<div class="form-group">
						<label class="col-sm-3 control-label">{{('Road Name')}}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="road_name" value="{{ $transport->road_name }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{('Vehicle Serial No')}}</label>
						<div class="col-sm-9">
							<select class="form-control select2" name="vehicle_id" required>
								<option value="">{{('Select One') }}</option>
								@foreach($vehicles AS $vehicle)
									<option  value="{{$vehicle->id}}" @if($transport->vehicle_id==$vehicle->id) selected="selected"  @endif>{{ $vehicle->serial_number }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{!! ('Road Fare')." <b>".get_option('currency_symbol')."</b>" !!}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="road_fare" value="{{ $transport->road_fare }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Note') }}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="note" value="{{ $transport->note }}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info">{{('Update Transport')}}</button>
						</div>
					</div>
				</form>
			   </div>
			</div>
		</div>
	</div>
</div>
@endsection
