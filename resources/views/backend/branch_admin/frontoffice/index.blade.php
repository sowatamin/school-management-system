@extends('layouts.backend')

@section('content')
<div class="row">
	<strong class="panel-title" style="display: none;">{{ ('Front Office') }}</strong>
	<div class="col-md-12">
		<ul class="nav nav-tabs setting-tab">
			<li @if((Request::is('branch_admin_frontoffice'))OR(Request::is('branch_admin_frontoffice/admission_enquiries'))) class="active" @endif><a data-toggle="tab" href="#admission_enquiries" aria-expanded="true">{{ ('Admission Enquiry') }}</a></li>
			<li @if(Request::is('branch_admin_frontoffice/visitor_informations')) class="active" @endif><a data-toggle="tab" href="#visitor_informations" aria-expanded="false">{{ ('Visitor Information') }}</a></li>
			<li @if(Request::is('branch_admin_frontoffice/phone_call_logs')) class="active" @endif><a data-toggle="tab" href="#sms" aria-expanded="false">{{ ('Phone Call Logs') }}</a></li>
			<li @if(Request::is('branch_admin_frontoffice/complains')) class="active" @endif><a data-toggle="tab" href="#complains" aria-expanded="false">{{ ('Complain') }}</a></li>
		</ul>
		<div class="tab-content">
			<div id="admission_enquiries" class="tab-pane fade @if((Request::is('branch_admin_frontoffice'))OR(Request::is('branch_admin_frontoffice/admission_enquiries')))in active @endif">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong class="panel-title">{{ ('List') }}</strong>
						<a href="{{ route('branch_admin_admission_enquiries.create') }}" class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ ('Add') }}">
							<i class="fa fa-plus"></i> {{ ('Add New') }}
						</a>
					</div>
					<div class="panel-body no-export">
						<table class="table table-bordered data-table admission_enquiries">
							<thead>
								<th>{{ ('Name') }}</th>
								<th>{{ ('Phone') }}</th>
								<th>{{ ('Source') }}</th>
								<th>{{ ('Enquiry Date') }}</th>
								<th>{{ ('Next Follow Up Date') }}</th>
								<th>{{ ('Action') }}</th>
							</thead>
							<tbody>
								@foreach($admission_enquiries AS $data)
								<tr>
									<td>{{ $data->first_name .' '. $data->last_name }}</td>
									<td>{{ $data->phone }}</td>
									<td>{{ $data->source }}</td>
									<td>{{ date('d M, Y',strtotime($data->date)) }}</td>
									<td>{{ date('d M, Y',strtotime($data->next_follow_up_date)) }}</td>
									<td>
										<form action="{{route('branch_admin_admission_enquiries.destroy',$data->id)}}" method="post">
											<a href="{{route('branch_admin_admission_enquiries.show',$data->id)}}" class="btn btn-info btn-xs ajax-modal" data-title="Details"><i class="fa fa-eye" aria-hidden="true"></i></a>
											<a href="{{route('branch_admin_admission_enquiries.edit',$data->id)}}" class="btn btn-warning btn-xs ajax-modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											{{ method_field('DELETE') }}
											@csrf
											<button type="submit" class="btn btn-danger btn-xs btn-remove"><i class="fa fa-eraser" aria-hidden="true"></i></button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div id="visitor_informations" class="tab-pane fade @if(Request::is('branch_admin_frontoffice/visitor_informations'))in active @endif">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong class="panel-title">{{ ('List') }}</strong>
						<a href="{{ route('branch_admin_visitor_informations.create') }}" class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ ('Add') }}">
							<i class="fa fa-plus"></i> {{ ('Add New') }}
						</a>
					</div>
					<div class="panel-body no-export">
						<table class="table table-bordered data-table">
							<thead>
								<th>{{ ('Purpose') }}</th>
								<th>{{ ('Name') }}</th>
								<th>{{ ('Phone') }}</th>
								<th>{{ ('Date') }}</th>
								<th>{{ ('Action') }}</th>
							</thead>
							<tbody>
								@foreach($visitor_informations AS $data)
								<tr>
									<td>{{ $data->purpose }}</td>
									<td>{{ $data->name }}</td>
									<td>{{ $data->phone }}</td>
									<td>{{ date('d M, Y',strtotime($data->date)) }}</td>
									<td>
										<form action="{{route('branch_admin_visitor_informations.destroy',$data->id)}}" method="post">
											<a href="{{route('branch_admin_visitor_informations.show',$data->id)}}" class="btn btn-info btn-xs ajax-modal" data-title="Details"><i class="fa fa-eye" aria-hidden="true"></i></a>
											<a href="{{route('branch_admin_visitor_informations.edit',$data->id)}}" class="btn btn-warning btn-xs ajax-modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											{{ method_field('DELETE') }}
											@csrf
											<button type="submit" class="btn btn-danger btn-xs btn-remove"><i class="fa fa-eraser" aria-hidden="true"></i></button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div id="sms" class="tab-pane fade @if(Request::is('branch_admin_frontoffice/phone_call_logs'))in active @endif">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong class="panel-title">{{ ('List') }}</strong>
						<a href="{{ route('branch_admin_phone_call_logs.create') }}" class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ ('Add') }}">
							<i class="fa fa-plus"></i> {{ ('Add New') }}
						</a>
					</div>
					<div class="panel-body no-export">
						<table class="table table-bordered data-table">
							<thead>
								<th>{{ ('Name') }}</th>
								<th>{{ ('Phone') }}</th>
								<th>{{ ('Call Type') }}</th>
								<th>{{ ('Date') }}</th>
								<th>{{ ('Start Time') }}</th>
								<th>{{ ('End Time') }}</th>
								<th>{{ ('Action') }}</th>
							</thead>
							<tbody>
								@foreach($phone_call_logs AS $data)
								<tr>
									<td>{{ $data->name }}</td>
									<td>{{ $data->phone }}</td>
									<td>{{ $data->call_type }}</td>
									<td>{{ date('d M, Y',strtotime($data->date)) }}</td>
									<td>{{ $data->start_time }}</td>
									<td>{{ $data->end_time }}</td>
									<td>
										<form action="{{ route('branch_admin_phone_call_logs.destroy',$data->id) }}" method="post">
											<a href="{{ route('branch_admin_phone_call_logs.show',$data->id) }}" class="btn btn-info btn-xs ajax-modal" data-title="Details"><i class="fa fa-eye" aria-hidden="true"></i></a>
											<a href="{{ route('branch_admin_phone_call_logs.edit',$data->id) }}" class="btn btn-warning btn-xs ajax-modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											{{ method_field('DELETE') }}
											@csrf
											<button type="submit" class="btn btn-danger btn-xs btn-remove"><i class="fa fa-eraser" aria-hidden="true"></i></button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div id="complains" class="tab-pane fade @if(Request::is('branch_admin_frontoffice/complains'))in active @endif">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong class="panel-title">{{ ('List') }}</strong>
						<a href="{{ route('branch_admin_complains.create') }}" class="btn btn-primary btn-sm pull-right ajax-modal" data-title="Add">
							<i class="fa fa-plus"></i> {{ ('Add New') }}
						</a>
					</div>
					<div class="panel-body no-export">
						<table class="table table-bordered data-table">
							<thead>
								<th>{{ ('Complain Type') }}</th>
								<th>{{ ('Source') }}</th>
								<th>{{ ('Complain By') }}</th>
								<th>{{ ('Phone') }}</th>
								<th>{{ ('Date') }}</th>
								<th>{{ ('Action') }}</th>
							</thead>
							<tbody>
								@foreach($complains AS $data)
								<tr>
									<td>{{ $data->complain_type }}</td>
									<td>{{ $data->source }}</td>
									<td>{{ $data->complain_by }}</td>
									<td>{{ $data->phone }}</td>
									<td>{{ date('d M, Y',strtotime($data->date)) }}</td>
									<td>
										<form action="{{route('branch_admin_complains.destroy',$data->id)}}" method="post">
											@if($data->attach_document != '')
											<a href="{{ asset('public/uploads/files/complains/'.$data->attach_document) }}" class="btn btn-primary btn-xs"><i class="fa fa-download" aria-hidden="true"></i></a>
											@endif
											<a href="{{route('branch_admin_complains.show',$data->id)}}" class="btn btn-info btn-xs ajax-modal" data-title="Details"><i class="fa fa-eye" aria-hidden="true"></i></a>
											<a href="{{route('branch_admin_complains.edit',$data->id)}}" class="btn btn-warning btn-xs ajax-modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											{{ method_field('DELETE') }}
											@csrf
											<button type="submit" class="btn btn-danger btn-xs btn-remove"><i class="fa fa-eraser" aria-hidden="true"></i></button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
