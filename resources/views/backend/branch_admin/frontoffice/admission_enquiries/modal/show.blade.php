<table class="table table-custom" width="100%">
	<tbody class="text-center">
		<tr>
			<td colspan="2"><img class="profile" src="{{ asset('public/uploads/images/profile.png') }}" style="width: 100px;height: 100px;"></td>
		</tr>
		<tr>
			<td>{{ ('Name') }}</td>
			<td>{{ $admission_enquiry->first_name .' '. $admission_enquiry->last_name}}</td>
		</tr>
		<tr>
			<td>{{ ('Phone') }}</td>
			<td>{{ $admission_enquiry->phone }}</td>
		</tr>
		<tr>
			<td>{{ ('Email') }}</td>
			<td>{{ $admission_enquiry->email }}</td>
		</tr>
		<tr>
			<td>{{ ('Address') }}</td>
			<td>{{ $admission_enquiry->address }}</td>
		</tr>
		<tr>
			<td>{{ ('Description') }}</td>
			<td>{{ $admission_enquiry->description }}</td>
		</tr>
		<tr>
			<td>{{ ('Date') }}</td>
			<td>{{ $admission_enquiry->date }}</td>
		</tr>
		<tr>
			<td>{{ ('Next Follow Up Date') }}</td>
			<td>{{ $admission_enquiry->next_follow_up_date }}</td>
		</tr>
		<tr>
			<td>{{ ('Reference') }}</td>
			<td>{{ $admission_enquiry->reference }}</td>
		</tr>
		<tr>
			<td>{{ ('Source') }}</td>
			<td>{{ $admission_enquiry->source }}</td>
		</tr>
		<tr>
			<td>{{ ('Class') }}</td>
			<td>{{ $admission_enquiry->class_name }}</td>
		</tr>
	</tbody>
</table>
