@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title" >
					{{ ('Admission Enquiry') }}
				</div>
			</div>
			<div class="panel-body">
				<form action="{{route('branch_admin_admission_enquiries.store')}}" class="validate" autocomplete="off" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('First Name') }}</label>
							<input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Last Name') }}</label>
							<input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Phone') }}</label>
							<input type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Email') }}</label>
							<input type="email" class="form-control" name="email" value="{{ old('email') }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Address') }}</label>
							<textarea class="form-control" name="address">{{ old('address') }}</textarea>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Description') }}</label>
							<textarea class="form-control" name="description">{{ old('description') }}</textarea>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Date') }}</label>
							<input type="text" class="form-control datepicker" name="date" value="{{ old('date') }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Next Follow Up Date') }}</label>
							<input type="text" class="form-control datepicker" name="next_follow_up_date" value="{{ old('next_follow_up_date') }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Reference') }} <a href="{{ url('branch_admin_picklists/create') }}" class="btn-link ajax-modal">{{ ('Add New') }}</a></label>
							<select class="form-control select2" name="reference">
								<option value="">{{ ('Select One') }}</option>
								{{ create_option("picklists","value","value",old('reference'),array("type="=>"Reference")) }}
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Source') }} <a href="{{ url('branch_admin_picklists/create') }}" class="btn-link ajax-modal">{{ ('Add New') }}</a></label>
							<select class="form-control select2" name="source" required>
								<option value="">{{ ('Select One') }}</option>
								{{ create_option("picklists","value","value",old('source'),array("type="=>"Source")) }}
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Class</label>
							<select name="class_id" class="form-control select2" required>
								<option value="">Select One</option>
								{{ create_admin_branch_class_option('classes','id','class_name',old('class_id')) }}
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<button type="submit" class="btn btn-info">{{ ('Save') }}</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
