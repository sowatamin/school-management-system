<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-bordered">
            <tr>
                <td>{{ ('Type') }}</td>
                <td>{{ $picklist->type }}</td>
            </tr>
            <tr>
                <td>{{ ('Value') }}</td>
                <td>{{ $picklist->value }}</td>
            </tr>

        </table>
    </div>
</div>
