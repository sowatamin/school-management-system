@extends('layouts.backend')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title">List Invoice</span>

                    <select id="class" class="select_class pull-right" onchange="showClass(this);">
                        <option value="">Select Class</option>
                        {{ create_admin_branch_class_option('classes','id','class_name',$class) }}
                    </select>

                    <a class="btn btn-primary btn-sm pull-right" data-title="Add New Invoice"
                       href="{{route('branch_admin_invoices.create')}}">Add New</a>

                    {{--<select name="invoice_month" id="invoice_month" class="select_class pull-right">--}}
                    {{--<option value="">{{ ('Select Month') }}</option>--}}
                    {{--{{ get_month_list(old('invoice_month')) }}--}}
                    {{--</select>--}}
                </div>

                <div class="panel-body">
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Student</th>
                            <th>Class / Section</th>
                            <th>Roll</th>
                            <th>Due Date</th>
                            <th>Title</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($invoices as $invoice)
                            <tr id="row_{{ $invoice->id }}">
                                <td>{{ $invoice->id }}</td>
                                <td>{{ $invoice->first_name." ".$invoice->last_name }}</td>
                                <td>{{ $invoice->class_name }} / {{ $invoice->section_name }}</td>
                                <td>{{ $invoice->roll }}</td>
                                <td>{{ date('d-M-Y', strtotime($invoice->due_date)) }}</td>
                                <td style='max-width:250px;'>{{ $invoice->title }}</td>
                                <td>{{ $invoice->total }}</td>
                                <td>{!! $invoice->status=="Paid" ? '<i class="fa fa-circle paid"></i>'.$invoice->status : '<i class="fa fa-circle unpaid"></i>'.$invoice->status !!}</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-primary btn-sm dropdown-toggle" type="button"
                                                data-toggle="dropdown">Action
                                            <span class="caret"></span></button>

                                        <form action="{{action('Branch_Admin\InvoiceController@destroy', $invoice['id'])}}"
                                              method="post">
                                            {{ csrf_field() }}
                                            <input name="_method" type="hidden" value="DELETE">
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ action('Branch_Admin\InvoiceController@edit', $invoice['id']) }}">Edit</a>
                                                </li>
                                                <li><a href="{{ action('Branch_Admin\InvoiceController@show', $invoice['id']) }}"
                                                       data-title="View Invoice" data-fullscreen="true"
                                                       class="ajax-modal">View Invoice</a></li>
                                                <li><a href="{{ url('branch_admin_student_payments/create/'.$invoice['id']) }}"
                                                       data-title="Add Payment"
                                                       class="ajax-modal">Take Payment</a></li>
                                                <li>
                                                    <button class="btn-remove link-btn"
                                                            type="submit">Delete
                                                    </button>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-script')
    <script>
        function showClass(elem) {
            if ($(elem).val() == "") {
                return;
            }
            window.location = "<?php echo url('branch_admin_invoices/class') ?>/" + $(elem).val();
        }
    </script>
@stop


