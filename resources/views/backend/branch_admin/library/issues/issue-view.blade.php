@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-sm-8">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title" >
					{{ ('Book Issue')}}
				</div>
			</div>
			<table class="table table-striped table-bordered" width="100%">
				<tbody style="text-align: center;">
					<tr>
						<td colspan="4">
							<img src="{{ asset('public/uploads/images/books/'.$issue->photo) }}" style="border-radius: 5px;" width="160px" alt="">
						</td>
					</tr>
					<tr>
						<td>{{ ('Mamber Name')}}</td>
						<td>{{$issue->member_name}}</td>
						<td>{{ ('Library Id')}}</td>
						<td>{{$issue->library_id}}</td>
					</tr>
					<tr>
						<td>{{ ('Book Name')}}</td>
						<td>{{$issue->name}}</td>
						<td>{{ ('Category')}}</td>
						<td>{{$issue->category_name}}</td>
					</tr>
					<tr>
						<td>{{ ('Issue Date')}}</td>
						<td>{{$issue->issue_date}}</td>
						<td>{{ ('Due Date')}}</td>
						<td>{{$issue->due_date}}</td>
					</tr>
					<tr>
						<td>{{ ('Return Date')}}</td>
						<td>{{$issue->return_date}}</td>
						<td>{{ ('Status')}}</td>
						<td>@if($issue->status == '1') <span class="badge badge-danger">{{ ('Not Return')}}</span> @else <span class="badge badge-success">{{'Returned'}}</span> @endif</td>
					</tr>
					<tr>
						<td colspan="2">{{ ('Note')}}</td>
						<td colspan="2">{{$issue->note}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
