@extends('layouts.backend')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">{{('Add New Student')}}</div>
                </div>
                <div class="panel-body">
                    <div class="col-md-10">
                        <form id="studentApplicationInf" action="{{route('branch_admin_students.store')}}" autocomplete="off"
                              class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data"
                              method="post" accept-charset="utf-8">
                            @csrf

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('First Name')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="first_name"
                                           value="{{ old('first_name') }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Last Name')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="last_name"
                                           value="{{ old('last_name') }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Guardian')}}</label>
                                <div class="col-sm-9">
                                    <select name="guardian" id="guardian" class="form-control" required>
                                        <option value="">{{ ('Select One') }}</option>
                                        {{ create_admin_branch_parent_option('parents','id','parent_name',old('guardian')) }}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Birthday')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control datepicker_2" name="birthday"
                                           value="{{ old('birthday') }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Gender')}}</label>
                                <div class="col-sm-9">
                                    <select name="gender" class="form-control niceselect wide" required>
                                        <option value="">{{ ('Select One') }}</option>
                                        <option value="Male">{{ ('Male') }}</option>
                                        <option value="Female">{{ ('Female') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Blood Group')}}</label>
                                <div class="col-sm-9">
                                    <select name="blood_group" class="form-control select2">
                                        <option value="">{{ ('Select One') }}</option>
                                        <option value="A+">N/A</option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Religion') }}</label>
                                <div class="col-sm-9">
                                    <select name="religion" class="form-control niceselect wide" required>
                                        <option value="">{{ ('Select One') }}</option>
                                        {{ create_option("picklists","value","value",old('religion'),array("type="=>"Religion")) }}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Phone') }}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Address')}}</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="address">{{ old('address') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('State')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="state" value="{{ old('state') }}"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Country')}}</label>
                                <div class="col-sm-9">
                                    <select name="country" class="form-control select2" required>
                                        {{ get_country_list(old('country')) }}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Class')}}</label>
                                <div class="col-sm-9">
                                    <select name="class" class="form-control select2" id="class" required>
                                        <option value="">{{ ('Select One') }}</option>
                                        {{ create_admin_branch_class_option('classes','id','class_name',old('class')) }}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Section</label>
                                <div class="col-sm-9">
                                    <select name="section" class="form-control niceselect wide" id="section" required>
                                        <option value="">{{ ('Select One') }}</option>
                                        @foreach($sections AS $data)
                                            <option data-class="{{$data->class_id}}"
                                                    value="{{$data->id}}">{{$data->section_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Group')}}</label>
                                <div class="col-sm-9">
                                    <select name="group" class="form-control select2">
                                        <option value="">{{ ('Select One') }}</option>
                                        {{ create_option('student_groups','id','group_name',old('group')) }}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Optional Subject')}}</label>
                                <div class="col-sm-9">
                                    <select name="optional_subject" id="optional_subject" class="form-control select2">
                                        <option value="">{{ ('Select One') }}</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Register NO')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="register_no"
                                           value="{{ old('register_no') }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Roll')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="roll" value="{{ old('roll') }}"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Extra Curricular Activities ')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="activities"
                                           value="{{ old('activities') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Remarks')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="remarks" value="{{ old('remarks') }}">
                                </div>
                            </div>
                            <hr>

                            <div class="page-header">
                                <h4>Academic Certificate or Document</h4>
                            </div>
                            <div class="form-group">
                                <label for="student_certificate" class="col-sm-3 control-label">
                                    Student Academic Certificate
                                </label>
                                <div class="col-md-5">
                                    <input id="student_certificate" type="file" class="form-control student_certificate"
                                           name="student_certificate[]" value="" style="height: auto;">
                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                </div>
                                <div class="col-md-2 col-xs-6 text-center addButtonWrap">
                                            <span type="button"
                                                  class="label label-success pictureAttachment certificateAttachmentElement"
                                                  style="background: black !important;cursor: pointer">
                                                <i class="fa fa-plus"></i> Add More
                                            </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="studentCertificateElement hide">
                                <div class="col-md-5 col-md-offset-3 appendElement" style="margin-top: 15px">
                                    <input id="" type="file" class="form-control student_certificate"
                                           name="student_certificate[]" value="" style="height: auto;">
                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                </div>
                                <div class="col-md-2 col-xs-6 text-center deleteButtonWrap" style="margin-top: 15px">
                                    <span type="button" class="label label-danger deleteAttachment"
                                          style="cursor: pointer">
                                        <i class="fa fa-trash"></i> Delete
                                    </span>
                                </div>
                            </div>

                            <hr>
                            <div class="page-header">
                                <h4>Login Details</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Email')}}</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Password')}}</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Confirm Password')}}</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{('Profile Picture')}}</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control dropify" name="image"
                                           data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-info">Add Student</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".pictureAttachment").on("click", function () {
                var attachInput = $(this).parents(".addButtonWrap").siblings("div").find("input");
                if ($(this).hasClass("certificateAttachmentElement")) {
                    var appendHtml = $(".studentCertificateElement").html();
                } else {
                    alert("something went wrong !!!");
                }
                if (attachInput.val() == '') {
                    attachInput.addClass("errorInput");
                } else {
                    var appendElement = $(this).parents(".addButtonWrap").siblings(".appendElement:last");
                    if (appendElement.length === 0) {
                        attachInput.removeClass('errorInput');
                        $(this).parents(".addButtonWrap").before(appendHtml);
                        $(this).parents(".addButtonWrap").css("margin-top", "15px");
                    } else {
                        if (appendElement.find("input").val() == '') {
                            appendElement.find("input").addClass("errorInput");
                        } else {
                            appendElement.find("input").removeClass("errorInput");
                            appendElement.siblings(".addButtonWrap").before(appendHtml);
                            appendElement.siblings(".addButtonWrap").css("margin-top", "15px");
                        }
                    }

                }
            });
            $("#studentApplicationInf").on("click", ".deleteAttachment", function () {
                var appendElement = $(this).parents(".deleteButtonWrap").siblings(".appendElement").length;
                $(this).parents(".deleteButtonWrap").prev(".appendElement").remove();
                $(this).parents(".deleteButtonWrap").remove();
                if (appendElement === 1) {
                    $(".pictureAttachment").parents(".addButtonWrap").css("margin-top", "0px");
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            var maxSize = '1024';
            var _validFileExtensions = [];
            var imgExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG"];
            var fileExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG", ".pdf", ".PDF"];

            function validateSingleInput(oInput) {
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        if (oInput.className.match(/\bonlyImg\b/)) {
                            _validFileExtensions = imgExtensions;
                        } else {
                            _validFileExtensions = fileExtensions;
                        }
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }

            function fileSizeValidate(fdata) {
                if (fdata.files && fdata.files[0]) {
                    var fsize = fdata.files[0].size / 1024;
                    if (fsize > maxSize) {
                        fdata.value = "";
                        return false;
                    } else {
                        return true;
                    }
                }
            }

//                For student certificate

            $(document).on("change", ".student_certificate", function () {
                var noExtentionError = validateSingleInput(this);
                var noSizeError = fileSizeValidate(this);
                if (noExtentionError === false) {
                    $(this).siblings(".oError").show().text("The certified copy must be JPG, JPEG, PNG, PDF format.");
                    return false;
                } else {
                    $(this).siblings(".oError").hide();
                    if (noSizeError === false) {
                        $(this).siblings(".oError").show().text("The certified copy will be less than 1 MB. ");
                        return false;
                    } else {
                        $(this).siblings(".oError").hide();
                    }
                }
            });

        });
    </script>

    <script>
        $(window).on('load', function () {
            $("#section").next().find("ul li").css("display", "none");
            $(document).on("change", "#class", function () {
                $("#section").next().find("ul li").css("display", "none");
                var class_id = $(this).val();
                $('#section option[data-class="' + class_id + '"]').each(function () {
                    var section_id = $(this).val();
                    $("#section").next().find("ul li[data-value='" + section_id + "']").css("display", "block");
                });
            });


            $(document).on('change', '#class', function () {
                load_option_subject();
            });

            function load_option_subject() {
                var class_id = $("#class").val();
                var link = "{{ url('branch_admin_students/get_subjects/') }}";
                $.ajax({
                    url: link + "/" + class_id,
                    success: function (data) {
                        $('#optional_subject').html(data);
                    }
                });
            }


            $('#guardian').select2({
                placeholder: "{{ ('Select One') }}",

                ajax: {
                    dataType: "json",
                    url: "{{ url('branch_admin_parents/get_parents') }}",
                    delay: 400,
                    data: function (params) {
                        return {
                            term: params.term
                        }
                    },
                    processResults: function (data, page) {
                        return {
                            results: data
                        };
                    },
                }
            });

        });


    </script>
@stop

