@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="panel-title">Syllabus List</span>
				<select id="class" class="select_class pull-right" onchange="showClass(this);">
					<option value="">Select Class</option>
					{{ create_admin_branch_class_option('classes','id','class_name',$class) }}
				</select>
				<a href="{{route('branch_admin_syllabus.create')}}"
				   class="btn btn-primary btn-sm pull-right">{{('Add New Syllabus')}}</a>
			</div>

			<div class="panel-body content no-export">
				<table class="table table-bordered data-table">
					<thead>
					<th>{{ ('Title')}}</th>
					<th>{{ ('Description')}}</th>
					<th>{{ ('Class')}}</th>
					<th>{{ ('File')}}</th>
					<th style="width: 155px;">{{ ('Action')}}</th>
					</thead>
					<tbody>
					@foreach($syllabus AS $data)
						<tr>
							<td>{{$data->title}}</td>
							<td>{{substr(strip_tags($data->description),0,100)}}...</td>
							<td>{{$data->class_name}}</td>
							<td>{{$data->file}}</td>
							<td>
								<form action="{{route('branch_admin_syllabus.destroy',$data->id)}}" method="post">
									<a href="{{ asset('public/uploads/files/syllabus/'.$data->file) }}" class="btn btn-info btn-xs"><i class="fa fa-download" aria-hidden="true"></i></a>
									<a href="{{route('branch_admin_syllabus.show',$data->id)}}" class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>
									<a href="{{route('branch_admin_syllabus.edit',$data->id)}}" class="btn btn-warning btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
									{{ method_field('DELETE') }}
									@csrf
									<button type="submit" class="btn btn-danger btn-xs btn-remove"><i class="fa fa-eraser" aria-hidden="true"></i></button>
								</form>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js-script')
	<script>
		function showClass(elem) {
			if ($(elem).val() == "") {
				return;
			}
			window.location = "<?php echo url('branch_admin_syllabus/class') ?>/" + $(elem).val();
		}
	</script>
@stop
