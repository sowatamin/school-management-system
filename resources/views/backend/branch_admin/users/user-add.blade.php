@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title" >
					{{ ('Add New User') }}
				</div>
			</div>
			<div class="panel-body">
			  <div class="col-md-8">
				<form action="{{route('branch_admin_users.store')}}" class="form-horizontal form-groups-bordered validate" autocomplete="off" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Name') }}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Email') }}</label>
						<div class="col-sm-9">
							<input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Phone') }}</label>
						<div class="col-sm-9">
							<input type="phone" class="form-control" name="phone" value="{{ old('phone') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Password') }}</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" name="password" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Confirm Password') }}</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" name="password_confirmation" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('User Type') }}</label>
						<div class="col-sm-9">
							<select name="user_type" id="user_type" class="form-control select2" required>
								<option value="">{{ ('Select One') }}</option>
								<option value="Accountant">{{ ('Accountant') }}</option>
								<option value="Librarian">{{ ('Librarian') }}</option>
								<option value="Employee">{{ ('Employee') }}</option>
								<option value="Teacher" disabled>{{ ('Teacher') }}</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Permission Role') }}</label>
						<div class="col-sm-9">
							<select name="role_id" id="role_id" class="form-control select2" required>
								<option value="">{{ ('Select One') }}</option>
								<option value="0">{{ ('Default') }}</option>
								{{ create_option("permission_roles","id","role_name") }}
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Profile') }}</label>
						<div class="col-sm-9">
							<input type="file" class="form-control dropify" name="image" data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Facebook Link') }}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="facebook" value="{{ old('facebook') }}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Twitter Link') }}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="twitter" value="{{ old('twitter') }}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Linkedin Link') }}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="linkedin" value="{{ old('linkedin') }}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">{{ ('Google Plus Link') }}</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="google_plus" value="{{ old('google_plus') }}">
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info">{{ ('Add User') }}</button>
						</div>
					</div>
				</form>
			  </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js-script')
<script>
	$("#user_type").val("{{ old('user_type') }}");
	$("#role_id").val("{{ old('role_id') }}");
</script>
@endsection
