@extends('layouts.backend')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Teacher Certificate Update</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            @php
                                $strExt = explode('.', $certificate->certificate);
                            @endphp
                            @if($strExt[1] === 'pdf')
                                <img style="height: 250px; width: 100%;margin-bottom: 6px"
                                     class="thumbnail img-responsive"
                                     src="{{ asset('public/uploads/images/pdf.png') }}"/>
                            @else
                                <img style="height: 250px; width: 100%;margin-bottom: 6px"
                                     class="thumbnail img-responsive"
                                     src="{{ asset('public/uploads/images/teacher_certificate/'.$certificate->certificate) }}"/>
                            @endif
                            <a class="btn btn-info btn-sm" data-fancybox="filter"
                               data-caption="student_certificate" target="_blank"
                               href="{{ asset('public/uploads/images/teacher_certificate/'.$certificate->certificate) }}">
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                    </div>
                    <form action="{{ url('teachers/certificate_update', $certificate->id )}}" autocomplete="off"
                          class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data"
                          method="post" accept-charset="utf-8">
                        @csrf

                        <div class="col-md-12 mt-5" style="margin-top: 20px">
                            <div class="form-group">
                                <label for="teacher_certificate" class="col-md-2 control-label">
                                    Teacher Certificate
                                </label>
                                <div class="col-md-6">
                                    <input id="teacher_certificate" type="file" class="form-control teacher_certificate"
                                           name="teacher_certificate" value="" style="height: auto;" required>
                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-5">
                                    <button type="submit" class="btn btn-info">Update Certificate</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            var maxSize = '1024';
            var _validFileExtensions = [];
            var imgExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG"];
            var fileExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG", ".pdf", ".PDF"];

            function validateSingleInput(oInput) {
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        if (oInput.className.match(/\bonlyImg\b/)) {
                            _validFileExtensions = imgExtensions;
                        } else {
                            _validFileExtensions = fileExtensions;
                        }
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }

            function fileSizeValidate(fdata) {
                if (fdata.files && fdata.files[0]) {
                    var fsize = fdata.files[0].size / 1024;
                    if (fsize > maxSize) {
                        fdata.value = "";
                        return false;
                    } else {
                        return true;
                    }
                }
            }

//                For student certificate

            $(document).on("change", ".teacher_certificate", function () {
                var noExtentionError = validateSingleInput(this);
                var noSizeError = fileSizeValidate(this);
                if (noExtentionError === false) {
                    $(this).siblings(".oError").show().text("The certified copy must be JPG, JPEG, PNG, PDF format.");
                    return false;
                } else {
                    $(this).siblings(".oError").hide();
                    if (noSizeError === false) {
                        $(this).siblings(".oError").show().text("The certified copy will be less than 1 MB. ");
                        return false;
                    } else {
                        $(this).siblings(".oError").hide();
                    }
                }
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-fancybox="filter"]').fancybox({
                buttons: [
                    'slideShow',
                    'fullScreen',
                    'thumbs',
                    'download',
                    'zoom',
                    'close'
                ]
            });
        });
    </script>
@stop


