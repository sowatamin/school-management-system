@extends('layouts.backend')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title">Teachers List</span>

                    <a href="{{route('teachers.create')}}" class="btn btn-info btn-sm pull-right"
                       style="margin-left: 10px;">{{ ('Add New Teacher')}}</a>
                    <a href="{{url('teachers/excel_import')}}" class="btn btn-info btn-sm pull-right ajax-modal"
                       data-title="Import Excel">Import Excel</a>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered data-table">
                        <thead>
                        <th>Profile</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Designation</th>
                        <th>Active</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @foreach($teachers AS $data)
                            <tr>
                                <td><img class="img-circle" src="{{ asset('public/uploads/images/'.$data->image) }}" width="50px" alt="">
                                </td>
                                <td>{{$data->name}}</td>
                                <td>{{$data->email}}</td>
                                <td>{{$data->designation}}</td>
                                <td>
                                    @if ($data->status == 'Active')
                                        <form action="{{ route('teachers.deactivate',$data->id) }}" method="post">
                                            {{ method_field('POST') }}
                                            @csrf
                                            <button type="submit" class="btn btn-primary btn-xs btn-inactive"><i
                                                        class="fa fa-check" aria-hidden="true"></i></button>
                                        </form>
                                    @else
                                        <form action="{{ route('teachers.active',$data->id) }}" method="post">
                                            {{ method_field('POST') }}
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-xs btn-active"><i
                                                        class="fa fa-minus-circle" aria-hidden="true"></i></button>
                                        </form>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('teachers.show',$data->id)}}"
                                       class="btn btn-info btn-xs">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{route('teachers.edit',$data->id)}}" class="btn btn-warning btn-xs">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
