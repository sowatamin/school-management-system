@extends('layouts.backend')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">{{ ('Add New Teacher')}}</div>
                    @if (empty(check_branch('branches')))
                        <div class="alert alert-warning float-right" style="margin-top: 10px;">
                            <strong>Warning!</strong>
                            <span style="color: #1d2124">Before Add Teacher.
                                Must <a href="{{route('branches.index')}}">Add Branch</a></span>
                        </div>
                    @endif
                </div>
                <div class="panel-body">
                    <div class="col-md-8">
                        <form id="teacherApplicationInf" action="{{route('teachers.store')}}" method="post"
                              autocomplete="off" class="form-horizontal validate" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Name')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Designation')}}</label>
                                <div class="col-sm-9">
                                    <select class="form-control select2" name="designation" required>
                                        <option value="">{{  ('Select One') }}</option>
                                        {{ create_option("picklists","value","value",old('designation'),array("type="=>"Designation")) }}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Birthday')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control datepicker_2" name="birthday"
                                           value="{{ old('birthday') }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Gender')}}</label>
                                <div class="col-sm-9">
                                    <select name="gender" class="form-control select2" required>
                                        <option value="">Select One</option>
                                        <option @if(old('gender')=='Male') selected @endif value="Male">Male</option>
                                        <option @if(old('gender')=='Female') selected @endif value="Female">Female
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Religion')}}</label>
                                <div class="col-sm-9">
                                    <select name="religion" class="form-control niceselect wide" required>
                                        <option value="">{{  ('Select One') }}</option>
                                        {{ create_option("picklists","value","value",old('religion'),array("type="=>"Religion")) }}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Phone')}}</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" name="phone" value="{{ old('phone') }}"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Address')}}</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="address"
                                              required>{{ old('address') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Joining Date')}}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control datepicker_2" name="joining_date"
                                           value="{{ old('joining_date') }}" required>
                                </div>
                            </div>

                            <hr>
                            <div class="page-header">
                                <h4>Academic Certificate and NID</h4>
                            </div>

                            <div class="form-group">
                                <label for="nid_number" class="col-sm-3 control-label">
                                    Teacher NID Number
                                </label>
                                <div class="col-md-9">
                                    <input id="nid_number" type="number" class="form-control"
                                           name="nid_number"  value="{{ old('nid_number') }}" style="height: auto;" required>
                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label for="nid_copy" class="col-sm-3 control-label">
                                    Teacher NID
                                </label>
                                <div class="col-md-9">
                                    <input id="nid_copy" type="file" class="form-control nid_copy"
                                           name="nid_copy" value="" style="height: auto;" required>
                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label for="teacher_certificate" class="col-sm-3 control-label">
                                    Teacher Certificate
                                </label>
                                <div class="col-md-5">
                                    <input id="teacher_certificate" type="file" class="form-control teacher_certificate"
                                           name="teacher_certificate[]" value="" style="height: auto;" required>
                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                </div>
                                <div class="col-md-2 col-xs-6 text-center addButtonWrap">
                                            <span type="button"
                                                  class="label label-success pictureAttachment certificateAttachmentElement"
                                                  style="background: black !important;cursor: pointer">
                                                <i class="fa fa-plus"></i> Add More
                                            </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="teacherCertificateElement hide">
                                <div class="col-md-5 col-md-offset-3 appendElement" style="margin-top: 15px">
                                    <input id="" type="file" class="form-control teacher_certificate"
                                           name="teacher_certificate[]" value="" style="height: auto;" required>
                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                </div>
                                <div class="col-md-2 col-xs-6 text-center deleteButtonWrap" style="margin-top: 15px">
                                    <span type="button" class="label label-danger deleteAttachment"
                                          style="cursor: pointer">
                                        <i class="fa fa-trash"></i> Delete
                                    </span>
                                </div>
                            </div>


                            <hr>
                            <div class="page-header">
                                <h4>Login Details</h4>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Email')}}</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Password')}}</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Confirm Password')}}</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{ ('Profile Picture')}}</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control dropify" name="image"
                                           data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-info">Add Teacher</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".pictureAttachment").on("click", function () {
                var attachInput = $(this).parents(".addButtonWrap").siblings("div").find("input");
                if ($(this).hasClass("certificateAttachmentElement")) {
                    var appendHtml = $(".teacherCertificateElement").html();
                } else {
                    alert("something went wrong !!!");
                }
                if (attachInput.val() == '') {
                    attachInput.addClass("errorInput");
                } else {
                    var appendElement = $(this).parents(".addButtonWrap").siblings(".appendElement:last");
                    if (appendElement.length === 0) {
                        attachInput.removeClass('errorInput');
                        $(this).parents(".addButtonWrap").before(appendHtml);
                        $(this).parents(".addButtonWrap").css("margin-top", "15px");
                    } else {
                        if (appendElement.find("input").val() == '') {
                            appendElement.find("input").addClass("errorInput");
                        } else {
                            appendElement.find("input").removeClass("errorInput");
                            appendElement.siblings(".addButtonWrap").before(appendHtml);
                            appendElement.siblings(".addButtonWrap").css("margin-top", "15px");
                        }
                    }

                }
            });
            $("#teacherApplicationInf").on("click", ".deleteAttachment", function () {
                var appendElement = $(this).parents(".deleteButtonWrap").siblings(".appendElement").length;
                $(this).parents(".deleteButtonWrap").prev(".appendElement").remove();
                $(this).parents(".deleteButtonWrap").remove();
                if (appendElement === 1) {
                    $(".pictureAttachment").parents(".addButtonWrap").css("margin-top", "0px");
                }
            });
        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            var maxSize = '1024';
            var _validFileExtensions = [];
            var imgExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG"];
            var fileExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG", ".pdf", ".PDF"];

            function validateSingleInput(oInput) {
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        if (oInput.className.match(/\bonlyImg\b/)) {
                            _validFileExtensions = imgExtensions;
                        } else {
                            _validFileExtensions = fileExtensions;
                        }
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }

            function fileSizeValidate(fdata) {
                if (fdata.files && fdata.files[0]) {
                    var fsize = fdata.files[0].size / 1024;
                    if (fsize > maxSize) {
                        fdata.value = "";
                        return false;
                    } else {
                        return true;
                    }
                }
            }

//                For teacher certificate

            $(document).on("change", ".teacher_certificate", function () {
                var noExtentionError = validateSingleInput(this);
                var noSizeError = fileSizeValidate(this);
                if (noExtentionError === false) {
                    $(this).siblings(".oError").show().text("The certified copy must be JPG, JPEG, PNG, PDF format.");
                    return false;
                } else {
                    $(this).siblings(".oError").hide();
                    if (noSizeError === false) {
                        $(this).siblings(".oError").show().text("The certified copy will be less than 1 MB. ");
                        return false;
                    } else {
                        $(this).siblings(".oError").hide();
                    }
                }
            });


            //                For teacher certificate

            $(document).on("change", ".nid_copy", function () {
                var noExtentionError = validateSingleInput(this);
                var noSizeError = fileSizeValidate(this);
                if (noExtentionError === false) {
                    $(this).siblings(".oError").show().text("The nid copy must be JPG, JPEG, PNG, PDF format.");
                    return false;
                } else {
                    $(this).siblings(".oError").hide();
                    if (noSizeError === false) {
                        $(this).siblings(".oError").show().text("The nid copy will be less than 1 MB. ");
                        return false;
                    } else {
                        $(this).siblings(".oError").hide();
                    }
                }
            });

        });

    </script>

@stop
