<table class="table table-striped table-bordered" width="100%">
	<tbody>
		<tr>
			<td style="text-align: center;" colspan="4"><img width="200px" style="border-radius: 7px;" src="{{ asset('public/uploads/images/'.$teacher->image) }}"></td>
		</tr>
		<tr>
			<td>{{ ('Name')}}</td>
			<td>{{$teacher->name}}</td>
		</tr>
		<tr>
			<td>{{ ('Designation')}}</td>
			<td>{{$teacher->designation}}</td>
		</tr>
		<tr>
			<td>{{ ('Date Of Birth')}}</td>
			<td>{{ date('d M, Y',strtotime($teacher->birthday)) }}</td>
		</tr>
		<tr>
			<td>{{ ('Gender')}}</td>
			<td>{{$teacher->gender}}</td>
		</tr>
		<tr>
			<td>{{ ('Religion')}}</td>
			<td>{{$teacher->religion}}</td>
		</tr>
		<tr>
			<td>{{  ('Joining Date') }}</td>
			<td>{{ date('d M, Y',strtotime($teacher->joining_date)) }}</td>
		</tr>
		<tr>
			<td>{{  ('Address') }}</td>
			<td>{{$teacher->address}}</td>
		</tr>
		<tr>
			<td>{{ ('Phone')}}</td>
			<td>{{$teacher->phone}}</td>
		</tr>
		<tr>
			<td>{{  ('Email') }}</td>
			<td>{{ $teacher->email }}</td>
		</tr>
	</tbody>
</table>
