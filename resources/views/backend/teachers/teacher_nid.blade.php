@extends('layouts.backend')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">{{ $nid->name }}`s Update NID</div>
                </div>
                <div class="panel-body">
                    <div class="col-md-10">
                        <form id="teacherApplicationInf" action="{{url('teachers/nid_store')}}"  autocomplete="off"
                              class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data"
                              method="post" accept-charset="utf-8">
                            @csrf

                            <input type="hidden" name="teacher_id" value="{{ $nid->id }}">
                            <input type="hidden" name="user_id" value="{{ $nid->user_id }}">

                            <div class="form-group">
                                <label for="nid_number" class="col-sm-3 control-label">
                                    Teacher NID Number
                                </label>
                                <div class="col-md-9">
                                    <input id="nid_number" type="text" class="form-control" value="{{ $nid->nid_number }}"  required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="nid_copy" class="col-sm-3 control-label">
                                    Teacher NID
                                </label>
                                <div class="col-md-9">
                                    <input id="nid_copy" type="file" class="form-control nid_copy"
                                           name="nid_copy" value="" style="height: auto;" required>
                                    <p class="help-block oError" style="color: red;margin-bottom: 0"></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-info">Update NID</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">

        $(document).ready(function () {
            var maxSize = '1024';
            var _validFileExtensions = [];
            var imgExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG"];
            var fileExtensions = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", "PNG", ".pdf", ".PDF"];

            function validateSingleInput(oInput) {
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        if (oInput.className.match(/\bonlyImg\b/)) {
                            _validFileExtensions = imgExtensions;
                        } else {
                            _validFileExtensions = fileExtensions;
                        }
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }

            function fileSizeValidate(fdata) {
                if (fdata.files && fdata.files[0]) {
                    var fsize = fdata.files[0].size / 1024;
                    if (fsize > maxSize) {
                        fdata.value = "";
                        return false;
                    } else {
                        return true;
                    }
                }
            }

//                For student certificate

            $(document).on("change", ".nid_copy", function () {
                var noExtentionError = validateSingleInput(this);
                var noSizeError = fileSizeValidate(this);
                if (noExtentionError === false) {
                    $(this).siblings(".oError").show().text("The nid copy must be JPG, JPEG, PNG, PDF format.");
                    return false;
                } else {
                    $(this).siblings(".oError").hide();
                    if (noSizeError === false) {
                        $(this).siblings(".oError").show().text("The nid copy will be less than 1 MB. ");
                        return false;
                    } else {
                        $(this).siblings(".oError").hide();
                    }
                }
            });

        });
    </script>

@stop

