<table class="table table-custom" width="100%">
	<tbody class="text-center">
		<tr>
			<td colspan="2"><img class="profile" src="{{ asset('public/uploads/images/profile.png') }}" style="width: 100px;height: 100px;"></td>
		</tr>
		<tr>
			<td>{{ ('Purpose') }}</td>
			<td>{{ $visitor_information->purpose }}</td>
		</tr>
		<tr>
			<td>{{ ('Name') }}</td>
			<td>{{ $visitor_information->name }}</td>
		</tr>
		<tr>
			<td>{{ ('Phone') }}</td>
			<td>{{ $visitor_information->phone }}</td>
		</tr>
		<tr>
			<td>{{ ('Date') }}</td>
			<td>{{ $visitor_information->date }}</td>
		</tr>
		<tr>
			<td>{{ ('In Time') }}</td>
			<td>{{ $visitor_information->in_time }}</td>
		</tr>
		<tr>
			<td>{{ ('Out Time') }}</td>
			<td>{{ $visitor_information->out_time }}</td>
		</tr>
		<tr>
			<td>{{ ('Number Of Person') }}</td>
			<td>{{ $visitor_information->number_of_person }}</td>
		</tr>
		<tr>
			<td>{{ ('Id Card') }}</td>
			<td>{{ $visitor_information->id_card }}</td>
		</tr>
		<tr>
			<td>{{ ('Note') }}</td>
			<td>{{ $visitor_information->note }}</td>
		</tr>
	</tbody>
</table>
