@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title" >
					{{('Profile')}}
				</div>
			</div>
			<div class="panel-body">
				<table class="table table-bordered" width="100%">
					<tbody style="text-align: center;">
						<tr class="text-center">
							<td colspan="2"><img src="{{ asset('public/uploads/images/profile.png') }}" style="width: 100px; border-radius: 5px"></td>
						</tr>
						<tr class="text-center">
							<td>{{ ('Complain Type') }}</td>
							<td>{{ $complain->complain_type }}</td>
						</tr>
						<tr class="text-center">
							<td>{{ ('Source') }}</td>
							<td>{{ $complain->source }}</td>
						</tr>
						<tr class="text-center">
							<td>{{ ('Complain By') }}</td>
							<td>{{ $complain->complain_by }}</td>
						</tr>
						<tr class="text-center">
							<td>{{ ('Phone') }}</td>
							<td>{{ $complain->phone }}</td>
						</tr>
						<tr class="text-center">
							<td>{{ ('Email') }}</td>
							<td>{{ $complain->email }}</td>
						</tr>
						<tr class="text-center">
							<td>{{ ('Date') }}</td>
							<td>{{ $complain->date }}</td>
						</tr>
						<tr class="text-center">
							<td>{{ ('Taken Action') }}</td>
							<td>{{ $complain->taken_action }}</td>
						</tr>
						<tr class="text-center">
							<td>{{ ('Note') }}</td>
							<td>{{ $complain->note }}</td>
						</tr>
						@if($complain->attach_document != '')
						<tr class="text-center">
							<td>{{ ('Attach Document') }}</td>
							<td><a href="{{ asset('public/uploads/files/complains/'.$complain->attach_document) }}"><i class="fa fa-download"></i></a></td>
						</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
