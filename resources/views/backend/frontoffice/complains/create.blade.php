@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title" >
					{{ ('Complain') }}
				</div>
			</div>
			<div class="panel-body">
				<form action="{{route('complains.store')}}" class="validate" autocomplete="off" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Complain Type') }} <a href="{{ url('picklists/create') }}" class="btn-link ajax-modal">{{ ('Add New') }}</a></label>
							<select class="form-control select2" name="complain_type" required>
								<option value="">{{ ('Select One') }}</option>
								{{ create_option("picklists","value","value",old('complain_type'),array("type="=>"Complain")) }}
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Source') }} <a href="{{ url('picklists/create') }}" class="btn-link ajax-modal">{{ ('Add New') }}</a></label>
							<select class="form-control select2" name="source" required>
								<option value="">{{ ('Select One') }}</option>
								{{ create_option("picklists","value","value",old('source'),array("type="=>"Source")) }}
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Complain By') }}</label>
							<input type="text" class="form-control" name="complain_by" value="{{ old('complain_by') }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Phone') }}</label>
							<input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Email') }}</label>
							<input type="email" class="form-control" name="email" value="{{ old('email') }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">{{ ('Date') }}</label>
							<input type="text" class="form-control datepicker" name="date" value="{{ (old('date')) ? old('date') : Carbon\Carbon::now()->toDateString() }}" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">{{ ('Branch') }}</label>
							<select name="branch" id="branch" class="form-control branch_select2" required>
								<option value="">Select One</option>
								{{ get_branch_list('branches','id','branch_name',old('branch')) }}
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">{{ ('Taken Action') }}</label>
							<textarea class="form-control" name="taken_action">{{ old('taken_action') }}</textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">{{ ('Note') }}</label>
							<textarea class="form-control" name="note">{{ old('note') }}</textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">{{('Attach Document')}}</label>
							<input type="file" class="form-control" name="attach_document">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<button type="submit" class="btn btn-info">{{ ('Save') }}</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
