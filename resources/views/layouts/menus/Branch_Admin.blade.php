<ul class="nav metismenu" id="menu">
    <li @if(Request::is('dashboard')) class="active" @endif>
        <a href="{{route('dashboard')}}">
            <i class="fa fa-desktop"></i>
            {{ ('Dashboard') }}
        </a>
    </li>
    <li @if((Request::is('branch_admin_frontoffice'))OR(Request::is('branch_admin_frontoffice/*'))) class="active" @endif>
        <a href="{{url('branch_admin_frontoffice')}}">
            <i class="fa fa-building-o"></i>
            {{ ('Front Office') }}
        </a>
    </li>
    <li>
        <a href="#"><i class="fa fa-user-o"></i>{{ ('Profile') }}</a>
        <ul>
            <li @if(Request::is('profile/my_profile')) class="active" @endif>
                <a href="{{ url('profile/my_profile')}}">
                    {{ ('Profile') }}
                </a>
            </li>
            <li @if(Request::is('profile/edit')) class="active" @endif>
                <a href="{{ url('profile/edit')}}">
                    {{ ('Update Profile') }}
                </a>
            </li>
            <li @if(Request::is('profile/changepassword')) class="active" @endif>
                <a href="{{ url('profile/changepassword') }}">
                    {{ ('Change Password') }}
                </a>
            </li>
            <li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
					document.getElementById('logout-form2').submit();">
                    {{ __('Logout') }}
                </a>
            </li>
            <form id="logout-form2" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-address-card"></i>{{ ('Students') }}</a>
        <ul>
            <li @if((Request::is('branch_admin_students'))OR(Request::is('branch_admin_students/create'))OR(Request::is('branch_admin_students/edit'))) class="active" @endif>
                <a href="{{ route('branch_admin_students.index') }}">Students</a>
            </li>
            <li @if(Request::is('branch_admin_students/promote')) class="active" @endif>
                <a href="{{ url('branch_admin_students/promote') }}">Promote Students</a>
            </li>
        </ul>
    </li>
    <li @if((Request::is('branch_admin_parents'))OR(Request::is('branch_admin_parents/*'))) class="active" @endif>
        <a href="{{route('branch_admin_parents.index')}}">
            <i class="fa fa-user-circle-o"></i>Parents
        </a>
    </li>
    <li @if((Request::is('branch_admin_teachers'))OR(Request::is('branch_admin_teachers/*'))) class="active" @endif>
        <a href="{{route('branch_admin_teachers.index')}}">
            <i class="fa fa-address-book"></i>Teachers
        </a>
    </li>
    <li>
        <a href="#"><i class="fa fa-building-o"></i>Academic</a>
        <ul>
            <li @if((Request::is('branch_admin_class'))OR(Request::is('branch_admin_class/*'))) class="active" @endif>
                <a href="{{route('branch_admin_class.index')}}">Class</a>
            </li>
            <li @if((Request::is('branch_admin_sections'))OR(Request::is('branch_admin_sections/*'))) class="active" @endif>
                <a href="{{route('branch_admin_sections.index')}}">Sections</a>
            </li>
            <li @if((Request::is('branch_admin_subjects'))OR(Request::is('branch_admin_subjects/*'))) class="active" @endif>
                <a href="{{route('branch_admin_subjects.index')}}">Subjects</a>
            </li>
            <li @if((Request::is('branch_admin_assignsubjects'))OR(Request::is('branch_admin_assignsubjects/*'))) class="active" @endif>
                <a href="{{route('branch_admin_assignsubjects.index')}}">Assign Subjects</a>
            </li>
            <li @if((Request::is('branch_admin_syllabus'))OR(Request::is('branch_admin_syllabus/*'))) class="active" @endif>
                <a href="{{route('branch_admin_syllabus.index')}}">Syllabus</a>
            </li>
            <li @if((Request::is('branch_admin_assignments'))OR(Request::is('branch_admin_assignments/*'))) class="active" @endif>
                <a href="{{route('branch_admin_assignments.index')}}">Assignments</a>
            </li>
            <li @if((Request::is('branch_admin_class_routines'))) class="active" @endif>
                <a href="{{url('branch_admin_class_routines')}}">Class Routine</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-calendar-check-o"></i>Attendance</a>
        <ul>
            <li @if((Request::is('branch_admin_student'))OR(Request::is('branch_admin_student/*'))) class="active" @endif>
                <a href="{{url('branch_admin_student/attendance')}}">
                    {{ ('Student Attendance') }}
                </a>
            </li>
            <li @if((Request::is('branch_admin_staff'))OR(Request::is('branch_admin_staff/*'))) class="active" @endif>
                <a href="{{url('branch_admin_staff/attendance')}}">
                    {{ ('Staff Attendance') }}
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-university"></i>Bank / Cash Account</a>
        <ul>
            <li @if(Request::is('branch_admin_accounts')) class="active" @endif>
                <a href="{{url('branch_admin_accounts')}}">Accounts</a>
            </li>
            <li @if(Request::is('branch_admin_accounts/create')) class="active" @endif>
                <a href="{{url('branch_admin_accounts/create')}}">Add New</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-money"></i>Transaction</a>
        <ul>
            <li @if(Request::is('branch_admin_transactions/income')) class="active" @endif>
                <a href="{{ url('branch_admin_transactions/income') }}">Income</a>
            </li>
            <li @if(Request::is('branch_admin_transactions/expense')) class="active" @endif>
                <a href="{{ url('branch_admin_transactions/expense') }}">Expense</a>
            </li>
            <li @if(Request::is('branch_admin_chart_of_accounts')) class="active" @endif>
                <a href="{{url('branch_admin_chart_of_accounts')}}">Chart Of Accounts</a>
            </li>
            <li @if(Request::is('branch_admin_payment_methods')) class="active" @endif>
                <a href="{{url('branch_admin_payment_methods')}}">Payment Methods</a>
            </li>
            <li @if(Request::is('branch_admin_payee_payers')) class="active" @endif>
                <a href="{{url('branch_admin_payee_payers')}}">Payee & Payers</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-credit-card"></i>{{ ('Student Fees') }}</a>
        <ul>
            <li @if(Request::is('branch_admin_fee_types')) class="active" @endif>
                <a href="{{url('branch_admin_fee_types')}}">Fees Type</a>
            </li>

            <li @if(Request::is('branch_admin_invoices/create')) class="active" @endif>
                <a href="{{url('branch_admin_invoices/create')}}">Generate Invoice</a>
            </li>

            <li @if(Request::is('branch_admin_invoices')) class="active" @endif>
                <a href="{{url('branch_admin_invoices')}}">Student Invoices</a>
            </li>
            <li @if(Request::is('branch_admin_student_payments')) class="active" @endif>
                <a href="{{url('branch_admin_student_payments')}}">Payment History</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-book"></i>Library</a>
        <ul>
            <li @if((Request::is('branch_admin_librarymembers'))OR(Request::is('branch_admin_librarymembers/*'))) class="active" @endif>
                <a href="{{url('branch_admin_librarymembers')}}">Members</a>
            </li>
            <li @if((Request::is('branch_admin_books'))OR(Request::is('branch_admin_books/*'))) class="active" @endif>
                <a href="{{url('branch_admin_books')}}">Books</a>
            </li>
            <li @if((Request::is('branch_admin_bookcategories'))OR(Request::is('branch_admin_bookcategories/*'))) class="active" @endif>
                <a href="{{url('branch_admin_bookcategories')}}">Book Categories</a>
            </li>
            <li @if((Request::is('branch_admin_bookissues'))OR(Request::is('branch_admin_bookissues/list'))OR(Request::is('branch_admin_bookissues/list/*'))OR(Request::is('branch_admin_bookissues/*/edit'))) class="active" @endif>
                <a href="{{url('branch_admin_bookissues')}}">Issues</a>
            </li>
            <li @if((Request::is('branch_admin_bookissues/create'))) class="active" @endif>
                <a href="{{url('branch_admin_bookissues/create')}}">Add Issues</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-car"></i>{{ ('Transport') }}</a>
        <ul>
            <li @if((Request::is('branch_admin_transportvehicles'))OR(Request::is('branch_admin_transportvehicles/*'))) class="active" @endif>
                <a href="{{url('branch_admin_transportvehicles')}}">
                    {{ ('Vehicles') }}
                </a>
            </li>
            <li @if((Request::is('branch_admin_transports'))OR(Request::is('branch_admin_transports/*'))) class="active" @endif>
                <a href="{{url('branch_admin_transports')}}">
                    {{ ('Transports') }}
                </a>
            </li>
            <li @if((Request::is('branch_admin_transportmembers'))OR(Request::is('branch_admin_transportmembers/*'))) class="active" @endif>
                <a href="{{url('branch_admin_transportmembers')}}">
                    {{ ('Members') }}
                </a>
            </li>
        </ul>
    </li>
    {{--<li>--}}
        {{--<a href="#"><i class="fa fa-building-o"></i>{{ ('Hostel') }}</a>--}}
        {{--<ul>--}}
            {{--<li @if((Request::is('hostels'))OR(Request::is('hostels/*'))) class="active" @endif>--}}
                {{--<a href="{{url('hostels')}}">--}}
                    {{--{{ ('Hostel') }}--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li @if((Request::is('hostelcategories'))OR(Request::is('hostelcategories/*'))) class="active" @endif>--}}
                {{--<a href="{{url('hostelcategories')}}">--}}
                    {{--{{ ('Categories') }}--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li @if((Request::is('hostelmembers'))OR(Request::is('hostelmembers/*'))) class="active" @endif>--}}
                {{--<a href="{{url('hostelmembers')}}">--}}
                    {{--{{ ('Members') }}--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</li>--}}
    <li>
        <a href="#"><i class="fa fa-newspaper-o"></i>Examinations</a>
        <ul>
            <li @if(Request::is('branch_admin_exams')) class="active" @endif>
                <a href="{{url('branch_admin_exams')}}">Exam List</a>
            </li>

            <li @if(Request::is('branch_admin_exams/schedule/create')) class="active" @endif>
                <a href="{{url('branch_admin_exams/schedule/create')}}">Exam Schedule</a>
            </li>

            <li @if(Request::is('branch_admin_exams/schedule')) class="active" @endif>
                <a href="{{url('branch_admin_exams/schedule')}}">Exam Routine</a>
            </li>

            <li @if(Request::is('branch_admin_exams/attendance')) class="active" @endif>
                <a href="{{url('branch_admin_exams/attendance')}}">Exam Attendance </a>
            </li>

        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-balance-scale"></i>Marks</a>
        <ul>
            <li @if(Request::is('branch_admin_marks')) class="active" @endif>
                <a href="{{ url('branch_admin_marks') }}">Marks</a>
            </li>

            <li @if(Request::is('branch_admin_marks/rank')) class="active" @endif>
                <a href="{{ url('branch_admin_marks/rank') }}">Student Rank</a>
            </li>

            <li @if(Request::is('branch_admin_marks/create')) class="active" @endif>
                <a href="{{ url('branch_admin_marks/create') }}">Mark Register</a>
            </li>

            <li @if(Request::is('branch_admin_grades')) class="active" @endif>
                <a href="{{ url('branch_admin_grades') }}">Grade Setup</a>
            </li>

            <li @if(Request::is('branch_admin_mark_distributions')) class="active" @endif>
                <a href="{{ url('branch_admin_mark_distributions') }}">Mark Distribution</a>
            </li>

        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-envelope-open"></i>Message
            {!! count_inbox() > 0 ? '<span class="label label-danger inbox-count">'.count_inbox().'</span>' : '' !!}
        </a>
        <ul>
            <li @if(Request::is('branch_admin_message/compose')) class="active" @endif>
                <a href="{{ url('branch_admin_message/compose') }}">New Message </a>
            </li>
            <li @if(Request::is('branch_admin_message/inbox')) class="active" @endif>
                <a href="{{ url('branch_admin_message/inbox') }}">Inbox Items</a>
            </li>
            <li @if(Request::is('branch_admin_message/outbox')) class="active" @endif>
                <a href="{{ url('branch_admin_message/outbox') }}">Send Items </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-newspaper-o"></i>Notice</a>
        <ul>
            <li @if(Request::is('branch_admin_notices')) class="active" @endif>
                <a href="{{ route('branch_admin_notices.index') }}">All Notice </a>
            </li>
            <li @if(Request::is('branch_admin_notices/create')) class="active" @endif>
                <a href="{{ route('branch_admin_notices.create') }}">New Notice</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-calendar"></i>{{ ('Events') }}</a>
        <ul>
            <li @if(Request::is('branch_admin_events')) class="active" @endif>
                <a href="{{ route('branch_admin_events.index') }}">All Events</a>
            </li>
            <li @if(Request::is('branch_admin_events/create')) class="active" @endif>
                <a href="{{ route('branch_admin_events.create') }}">Add New Event</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-envelope-o"></i>{{ ('Email & SMS') }}</a>
        <ul>
            <li @if(Request::is('branch_admin_email/compose')) class="active" @endif>
                <a href="{{ url('branch_admin_email/compose') }}">Send Email </a>
            </li>
            <li @if(Request::is('branch_admin_email/logs')) class="active" @endif>
                <a href="{{ url('branch_admin_email/logs') }}">Email Log</a>
            </li>
            <li @if(Request::is('branch_admin_sms/compose')) class="active" @endif>
                <a href="{{ url('branch_admin_sms/compose') }}">Send SMS</a>
            </li>
            <li @if(Request::is('branch_admin_sms/logs')) class="active" @endif>
                <a href="{{ url('branch_admin_sms/logs') }}">SMS Log</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-bar-chart"></i>{{ ('Reports') }}</a>
        <ul>
            <li @if(Request::is('branch_admin_reports/student_attendance_report') || Request::is('branch_admin_reports/student_attendance_report/view')) class="active" @endif>
                <a href="{{ url('branch_admin_reports/student_attendance_report') }}">Student Attendance</a>
            </li>
            <li @if(Request::is('branch_admin_reports/staff_attendance_report') || Request::is('branch_admin_reports/staff_attendance_report/view')) class="active" @endif>
                <a href="{{ url('branch_admin_reports/staff_attendance_report') }}">Staff Attendance</a>
            </li>
            <li @if(Request::is('branch_admin_reports/student_id_card') || Request::is('branch_admin_reports/student_id_card/view')) class="active" @endif>
                <a href="{{ url('branch_admin_reports/student_id_card') }}">Student ID Card</a>
            </li>
            <li @if(Request::is('branch_admin_reports/exam_report') || Request::is('branch_admin_reports/exam_report/view')) class="active" @endif>
                <a href="{{ url('branch_admin_reports/exam_report') }}">Exam Report</a>
            </li>
            <li @if(Request::is('branch_admin_reports/progress_card') || Request::is('branch_admin_reports/progress_card/view')) class="active" @endif>
                <a href="{{ url('branch_admin_reports/progress_card') }}">Progress Card</a>
            </li>
            <li @if(Request::is('branch_admin_reports/class_routine') || Request::is('branch_admin_reports/class_routine/view')) class="active" @endif>
                <a href="{{ url('branch_admin_reports/class_routine') }}">Class Routine</a>
            </li>
            <li @if(Request::is('branch_admin_reports/exam_routine') || Request::is('branch_admin_reports/exam_routine/view')) class="active" @endif>
                <a href="{{ url('branch_admin_reports/exam_routine') }}">Exam Routine</a>
            </li>
            <li @if(Request::is('branch_admin_reports/income_report') || Request::is('branch_admin_reports/income_report/view')) class="active" @endif>
                <a href="{{ url('branch_admin_reports/income_report') }}">Income Report</a>
            </li>
            <li @if(Request::is('branch_admin_reports/expense_report') || Request::is('branch_admin_reports/expense_report/view')) class="active" @endif>
                <a href="{{ url('branch_admin_reports/expense_report') }}">Expense Report</a>
            </li>
            <li @if(Request::is('branch_admin_reports/account_balance')) class="active" @endif>
                <a href="{{ url('branch_admin_reports/account_balance') }}">Financial Account Balance</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fa fa-cogs"></i>{{ ('Administration') }}</a>
        <ul>
            <li @if((Request::is('branch_admin_picklists'))) class="active" @endif>
                <a href="{{route('branch_admin_picklists.index')}}">Picklist Editor </a>
            </li>
        </ul>
    </li>
</ul>
