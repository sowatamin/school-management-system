<ul class="nav metismenu" id="menu">
    <li @if(Request::is('dashboard')) class="active" @endif>
        <a href="{{route('dashboard')}}">
            <i class="fa fa-desktop"></i>
            {{ ('Dashboard') }}
        </a>
    </li>
    @include('layouts.menus.menus')

    <li>
        <a href="#"><i class="fa fa-book"></i>{{ ('Library') }}</a>
        <ul>

            <li @if((Request::is('librarymembers'))OR(Request::is('librarymembers/*'))) class="active" @endif>
                <a href="{{url('librarymembers')}}">
                    {{ ('Members') }}
                </a>
            </li>

            <li @if((Request::is('books'))OR(Request::is('books/*'))) class="active" @endif>
                <a href="{{url('books')}}">
                    {{ ('Books') }}
                </a>
            </li>

            <li @if((Request::is('bookcategories'))OR(Request::is('bookcategories/*'))) class="active" @endif>
                <a href="{{url('bookcategories')}}">
                    {{ ('Book Categories') }}
                </a>
            </li>

            <li @if((Request::is('bookissues'))OR(Request::is('bookissues/list'))OR(Request::is('bookissues/list/*'))OR(Request::is('bookissues/*/edit'))) class="active" @endif>
                <a href="{{url('bookissues')}}">
                    {{ ('Issues') }}
                </a>
            </li>

            <li @if((Request::is('bookissues/create'))) class="active" @endif>
                <a href="{{url('bookissues/create')}}">
                    {{ ('Add Issues') }}
                </a>
            </li>
        </ul>
    </li>
</ul>
