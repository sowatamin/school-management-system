<ul class="nav metismenu" id="menu">

	<li @if(Request::is('dashboard')) class="active" @endif>
		<a href="{{ route('dashboard') }}">
			<i class="fa fa-desktop"></i>
			{{ ('Dashboard') }}
		</a>
	</li>

	<li @if(Request::is('student/my_assignment')) class="active" @endif>
		<a href="{{ url('student/my_assignment') }}">
			<i class="fa fa-hourglass-half"></i>
			{{ ('Academic Assignment') }}
		</a>
	</li>

	<li @if(Request::is('student/my_syllabus')) class="active" @endif>
		<a href="{{ url('student/my_syllabus') }}">
			<i class="fa fa-file"></i>
			{{ ('Academic Syllabus') }}
		</a>
	</li>

	<li @if(Request::is('student/my_profile')) class="active" @endif>
		<a href="{{ url('student/my_profile') }}">
			<i class="fa fa-user-circle-o"></i>
			{{ ('My Profile') }}
		</a>
	</li>

	<li @if(Request::is('student/my_subjects')) class="active" @endif>
		<a href="{{ url('student/my_subjects') }}">
			<i class="fa fa-briefcase"></i>
			{{ ('My Subjects') }}
		</a>
	</li>

	<li @if(Request::is('student/class_routine')) class="active" @endif>
		<a href="{{ url('student/class_routine') }}">
			<i class="fa fa-calendar"></i>
			{{ ('Class Routine') }}
		</a>
	</li>

	<li @if(Request::is('student/exam_routine')) class="active" @endif>
		<a href="{{ url('student/exam_routine') }}">
			<i class="fa fa-calendar"></i>
			{{ ('Exam Routine') }}
		</a>
	</li>

	<li @if(Request::is('student/progress_card')) class="active" @endif>
		<a href="{{ url('student/progress_card') }}">
			<i class="fa fa-bar-chart"></i>
			{{ ('Progress Card') }}
		</a>
	</li>

	<li>
		<a href="#"><i class="fa fa-line-chart"></i>Chart</a>
		<ul>
			<li @if(Request::is('student/mark_chart')) class="active" @endif>
				<a href="{{ url('student/mark_chart') }}">Mark Chart</a>
			</li>
		</ul>
	</li>

	 <li>
		<a href="#"><i class="fa fa-file-text"></i>{{ ('My Invoice') }}</a>
		<ul>
		   <li @if(Request::is('student/my_invoice')) class="active" @endif>
				<a href="{{ url('student/my_invoice') }}">
					{{ ('Unpaid Invoice') }}
				</a>
			</li>

			<li @if(Request::is('student/my_invoice/paid')) class="active" @endif>
				<a href="{{ url('student/my_invoice/paid') }}">
					{{ ('Paid Invoice') }}
				</a>
			</li>
		</ul>
	 </li>

	<li @if(Request::is('student/payment_history')) class="active" @endif>
		<a href="{{ url('student/payment_history') }}">
			<i class="fa fa-cc"></i>
			{{ ('Payment History') }}
		</a>
	</li>

	<li @if(Request::is('student/library_history')) class="active" @endif>
		<a href="{{ url('student/library_history') }}">
			<i class="fa fa-book"></i>
			{{ ('Library History') }}
		</a>
	</li>

	@include('layouts.menus.menus')
</ul>
