@extends('install.layout')

@section('content')
<div class="panel panel-default">
  <div class="panel-heading text-center">Branch Details</div>
	<div class="panel-body">

		<form action="{{ url('install/finish') }}" method="post" autocomplete="off">
			{{ csrf_field() }}

			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">Branch Name</label>
					<input type="text" class="form-control" name="branch_name" required>
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">Branch Address</label>
					<textarea class="form-control" name="branch_address" required
							  rows="4">{{ old('branch_address') }}</textarea>
				</div>
			</div>

			<div class="col-md-12">
			  <div class="form-group">
				<label class="control-label">Branch Phone</label>
				<input type="number" class="form-control" name="branch_phone" required>
			  </div>
			</div>


			<div class="col-md-12">
				<button type="submit" id="next-button" class="btn btn-install">Finish</button>
		    </div>
		</form>

	</div>
</div>
@endsection

